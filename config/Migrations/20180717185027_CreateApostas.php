<?php
use Migrations\AbstractMigration;

class CreateApostas extends AbstractMigration
{
	/**
	 * Change Method.
	 *
	 * More information on this method is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-change-method
	 * @return void
	 */
	public function change()
	{
		$table = $this->table('apostas', [
			'collation' => 'utf8_general_ci'
		]);
		$table->addColumn('bilhete_id', 'integer', [
			'default' => null,
			'limit' => 11,
			'null' => true
		]);
		$table->addColumn('partida_id', 'integer', [
			'default' => null,
			'limit' => 11,
			'null' => true
		]);
		$table->addColumn('partida_time', 'string', [
			'default' => null,
			'null' => false
		]);
		$table->addColumn('league_name', 'string', [
			'default' => null,
			'limit' => 150,
			'null' => true
		]);
		$table->addColumn('home_name', 'string', [
			'default' => null,
			'limit' => 150,
			'null' => true
		]);
		$table->addColumn('away_name', 'string', [
			'default' => null,
			'limit' => 150,
			'null' => true
		]);
		$table->addColumn('palpite_id', 'string', [
			'default' => null,
			'limit' => 50,
			'null' => true
		]);
		$table->addColumn('palpite_odd', 'string', [
			'default' => null,
			'limit' => 10,
			'null' => true
		]);
		$table->addColumn('won', 'boolean', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('score', 'string', [
			'limit' => 10,
			'default' => null,
			'null' => true
		]);
		$table->addColumn('is_active', 'boolean', [
			'default' => false,
			'null' => true
		]);
		$table->addColumn('created', 'datetime', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('modified', 'datetime', [
			'default' => null,
			'null' => true
		]);
		$table->create();
	}
}
