<?php
use Migrations\AbstractMigration;

class CreateBilhetes extends AbstractMigration
{
	/**
	 * Change Method.
	 *
	 * More information on this method is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-change-method
	 * @return void
	 */
	public function change()
	{
		$table = $this->table('bilhetes', [
			'collation' => 'utf8_general_ci'
		]);
		$table->addColumn('apostador', 'string', [
			'default' => null,
			'limit' => 150,
			'null' => true
		]);
		$table->addColumn('revendedor', 'string', [
			'default' => null,
			'limit' => 150,
			'null' => true
		]);
		$table->addColumn('valor_apostado', 'float', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('soma_odds', 'float', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('valor_premio', 'float', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('valor_revendedor', 'float', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('valor_total', 'float', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('expiration_datetime', 'datetime', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('is_approved', 'boolean', [
			'default' => false,
			'null' => true
		]);
		$table->addColumn('approval_datetime', 'datetime', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('approval_user', 'integer', [
			'default' => null,
			'limit' => 11,
			'null' => true
		]);
		$table->addColumn('won', 'boolean', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('pgto_premio', 'boolean', [
			'default' => false,
			'null' => true
		]);
		$table->addColumn('pgto_premio_date', 'timestamp', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('pgto_premio_user', 'integer', [
			'default' => null,
			'limit' => 11,
			'null' => true
		]);
		$table->addColumn('pgto_revendedor', 'boolean', [
			'default' => false,
			'null' => true
		]);
		$table->addColumn('pgto_revendedor_date', 'timestamp', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('pgto_revendedor_user', 'integer', [
			'default' => null,
			'limit' => 11,
			'null' => true
		]);
		$table->addColumn('is_active', 'boolean', [
			'default' => false,
			'null' => true
		]);
		$table->addColumn('created', 'datetime', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('modified', 'datetime', [
			'default' => null,
			'null' => true
		]);
		$table->create();
	}
}
