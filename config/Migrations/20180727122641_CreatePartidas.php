<?php
use Migrations\AbstractMigration;

class CreatePartidas extends AbstractMigration
{
	/**
	 * Change Method.
	 *
	 * More information on this method is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-change-method
	 * @return void
	 */
	public function change()
	{
		$table = $this->table('partidas', [
			'collation' => 'utf8_general_ci'
		]);
		$table->addColumn('time', 'string', [
			'default' => null,
			'null' => false
		]);

		// League
		$table->addColumn('league_id', 'integer', [
			'default' => null,
			'limit' => 11,
			'null' => true
		]);
		$table->addColumn('league_name', 'string', [
			'default' => null,
			'limit' => 150,
			'null' => true
		]);
		$table->addColumn('league_cc', 'string', [
			'default' => null,
			'limit' => 25,
			'null' => true
		]);

		// Home
		$table->addColumn('home_id', 'integer', [
			'default' => null,
			'limit' => 11,
			'null' => true
		]);
		$table->addColumn('home_name', 'string', [
			'default' => null,
			'limit' => 150,
			'null' => true
		]);
		$table->addColumn('home_image_id', 'string', [
			'default' => null,
			'limit' => 25,
			'null' => true
		]);
		$table->addColumn('home_cc', 'string', [
			'default' => null,
			'limit' => 25,
			'null' => true
		]);

		// Away
		$table->addColumn('away_id', 'integer', [
			'default' => null,
			'limit' => 11,
			'null' => true
		]);
		$table->addColumn('away_name', 'string', [
			'default' => null,
			'limit' => 150,
			'null' => true
		]);
		$table->addColumn('away_image_id', 'string', [
			'default' => null,
			'limit' => 25,
			'null' => true
		]);
		$table->addColumn('away_cc', 'string', [
			'default' => null,
			'limit' => 25,
			'null' => true
		]);

		// Odds
		$table->addColumn('odds', 'string', [
			'default' => null,
			'limit' => 10000,
			'null' => true
		]);

		$table->addColumn('is_active', 'boolean', [
			'default' => true,
			'null' => false
		]);
		$table->addColumn('created', 'datetime', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('modified', 'datetime', [
			'default' => null,
			'null' => true
		]);
		$table->create();
	}
}
