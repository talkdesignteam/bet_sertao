<?php
use Migrations\AbstractMigration;

class CreatePalpites extends AbstractMigration
{
	/**
	 * Change Method.
	 *
	 * More information on this method is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-change-method
	 * @return void
	 */
	public function change()
	{
		$table = $this->table('palpites', [
			'collation' => 'utf8_general_ci'
		]);
		$table->addColumn('nome', 'string', [
			'default' => null,
			'limit' => 100,
			'null' => true
		]);
		$table->addColumn('descricao', 'text', [
			'default' => null,
			'limit' => 250,
			'null' => true
		]);
		$table->addColumn('calculo', 'text', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('is_active', 'boolean', [
			'default' => true,
			'null' => false
		]);
		$table->addColumn('created', 'datetime', [
			'default' => null,
			'null' => true
		]);
		$table->addColumn('modified', 'datetime', [
			'default' => null,
			'null' => true
		]);
		$table->create();
	}
}
