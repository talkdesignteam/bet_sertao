-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: bet_mysql
-- Generation Time: 14-Set-2018 às 03:48
-- Versão do servidor: 5.7.21
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `betsertao`
--

--
-- Extraindo dados da tabela `palpites`
--

INSERT INTO `palpites` (`id`, `nome`, `descricao`, `calculo`, `is_active`, `created`, `modified`) VALUES
(1, 'home_od', 'Casa', 'return (($home1+$home2) > ($away1+$away2))? true:false;', 1, NULL, NULL),
(2, 'draw_od', 'Empate', 'return (($home1+$home2) == ($away1+$away2))? true:false;', 1, NULL, NULL),
(3, 'away_od', 'Fora', 'return (($home1+$home2) < ($away1+$away2))? true:false;', 1, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
