<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PalpitesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PalpitesTable Test Case
 */
class PalpitesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PalpitesTable
     */
    public $Palpites;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.palpites',
        'app.apostas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Palpites') ? [] : ['className' => PalpitesTable::class];
        $this->Palpites = TableRegistry::get('Palpites', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Palpites);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
