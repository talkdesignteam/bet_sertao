<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BilhetesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BilhetesTable Test Case
 */
class BilhetesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BilhetesTable
     */
    public $Bilhetes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bilhetes',
        'app.apostas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Bilhetes') ? [] : ['className' => BilhetesTable::class];
        $this->Bilhetes = TableRegistry::get('Bilhetes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bilhetes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
