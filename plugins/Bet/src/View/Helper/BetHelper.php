<?php
namespace Bet\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Bet helper
 */
class BetHelper extends Helper
{

	public $helpers = ['Html'];

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

	/**
	 * Get image URL for team
	 *
	 * @param string $team_id (home->image_id)
	 * @param string $type 's' default, 'b'
	 * @return  string|false  url path from image team
	 */
	public function getTeamImage($team_id=null, $type='small') {

		$image_types = [
			'small'=>'s',
			'medium'=>'m',
			'big'=>'b'
		];
		$type = $image_types[$type];

		$img_url = "https://assets.b365api.com/images/team/$type/$team_id.png";

		return $img_url ?: false;
	}

	/**
	 * Get image URL for team
	 *
	 * @param string $team_id
	 * @return  string|false  url path from image team
	 */
	public function image($team_id=null, $options=array()) {

		$img_url = $this->getTeamImage($team_id);

		return $this->Html->image($img_url, $options);
	}

	/**
	 * Get time status
	 *
	 * @param integer $time_status
	 * @return  string|desconhecido  resposta em string do time_status passado
	 */
	public function getStringTimeStatus($time_status=null) {
		
		$str_time_status = [
			'0'=>'Not Started',
			'1'=>'InPlay',
			'2'=>'TO BE FIXED',
			'3'=>'Ended',
			'4'=>'Postponed',
			'5'=>'Cancelled',
			'6'=>'Walkover',
			'7'=>'Interrupted',
			'8'=>'Abandoned',
			'9'=>'Retired',
			'99'=>'Removed'
		];

		if ( isset($str_time_status[$time_status]) ) {

			return $str_time_status[$time_status];
		}
		else{

			return 'Unknown';
		}
	}
}
