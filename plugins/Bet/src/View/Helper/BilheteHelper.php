<?php
namespace Bet\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Bilhete helper
 */
class BilheteHelper extends Helper
{

	public $helpers = ['Html', 'Number'];

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

	/**
	 * Status de bilhetes .
	 *
	 * @var array
	 */
	protected $_bilheteStatus = [
		null=>'aguardando resultado',
		0=>'perdeu',
		1=>'ganhou'
	];

	/**
	 * Palpites de apostas .
	 *
	 * @var array
	 */
	protected $_palpites = [
		null => 'desconhecido',
		'home_od' => 'Casa',
		'draw_od' => 'Empate',
		'away_od' => 'Fora'
	];

	/**
	 * @return string to palpite_id
	 */
	public function palpiteAposta($palpite_id=null) {

		return $this->_palpites[$palpite_id];
	}

	/**
	 * Retorna o status do bilhete por extenso
	 *
	 * @param integer $status
	 * @return  string|desconhecido  resposta em string do status passado
	 */
	public function bilheteStatus($status=null) {
		
		return $this->_bilheteStatus[$status];
	}

	/**
	 * Retorna o palpite no formato padrão estabelecido ex.: 1,00
	 *
	 * @param numeric $palpite_odd
	 * @return  numeric formated $palpite_odd  
	 */
	public function formatPalpiteOdd($value=null) {

		if ( is_numeric($value) ) {

			$value = $this->Number->format(round($value, 2), 
				['places' => 2]
			);

			return $value;
		}

		return null;
	}
}
