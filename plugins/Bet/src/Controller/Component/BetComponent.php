<?php
namespace Bet\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Exception\Exception;

/**
 * Componente simples para uso da API betsapi.com
 * Betsapi.com API v2: https://betsapi.com/docs
 *
 * @version 1.0
 */
class BetComponent extends Component
{

	/**
	 * Default configuration.
	 *
	 * @var array
	 */
	protected $_defaultConfig = [
		'api_token' => null,
		'api_endpoint' => 'https://api.betsapi.com'
	];

	/**
	 * Last error
	 *
	 * @var array
	 */
	private $last_error = '';
	
	/**
	 * Time Status
	 *
	 * @var array
	 */
	private $time_status = [
		0 => 'Not Started',
		1 => 'InPlay',
		2 => 'TO BE FIXED',
		3 => 'Ended',
		4 => 'Postponed',
		5 => 'Cancelled',
		6 => 'Walkover',
		7 => 'Interrupted',
		8 => 'Abandoned',
		9 => 'Retired',
		99 => 'Removed'
	];

	/**
	 * Initialize properties.
	 *
	 * @param array $config The config data.
	 * @return void
	 */
	public function initialize(array $config) {

		if (!function_exists('curl_init') || !function_exists('curl_setopt')) {
			throw new \Exception(__('cURL support is required, but can\'t be found.'));
		}

		if ($this->_config['api_token']==null) {
			throw new Exception(__('Informe o token de acesso (API token)'));
		}

		if ($this->_config['api_endpoint']==null) {
			throw new Exception(__('Informe um Endpoint da API.'));
		}
	}

	/**
	 * Get Upcoming Events
	 * ref.: https://betsapi.com/docs/events/upcoming.html
	 *
	 * @param array $params[
	 * 		'sport_id'=>1, 			// default is 1 (soccer)
	 * 		'league_id'=>null, 		// useful when you want only one league
	 * 		'team_id'=>null, 		// useful when you want only one team
	 * 		'cc'=>null,				// Eg: 'co' for Colombia (R-Countries)
	 * 		'day'=>null, 			// format YYYYMMDD, eg: 20161201
	 * 		'page'=>null, 			// (R-Pager) 
	 * ]
	 *
	 * @return array or json
	 */
	public function getUpcomingEvents($params=null) {

		$method = 'v2/events/upcoming';
		
		if ( $data=$this->_makeRequest($method, $params) ) {

			return $data->results;
		}

		return false;
	}

	/**
	 * Get Event View
	 * ref.: https://betsapi.com/docs/events/view.html
	 *
	 * @param array $params[
	 * 		'event_id'=>int $id,	//Event ID you get from events/*
	 * ]
	 * @return array or json
	 */
	public function getEventView($params=null) {

		$method = 'v1/event/view';

		if ( $data=$this->_makeRequest($method, $params) ) {

			return $data->results[0];
		}

		return false;
	}

	/**
	 * Get Event Odds
	 * ref.: https://betsapi.com/docs/events/odds.html
	 *
	 * @param array $params[
	 * 		'event_id'=>null,		// Event ID you get from events/* (required)
	 * 		'source'=>null, 		// bet365, 10bet...defaults to bet365.
	 * 		'since_time'=>null,		// integer. add_time will be >= $since_time in results. Faster to get only updates.
	 * 		'odds_market'=>null		// String. &odds_market=1 or &odds_market=2,3 etc.
	 * ]
	 *
	 * @return array or json
	 */
	public function getEventOdds($params=null) {

		$method = 'v1/event/odds';
		
		if ( $data=$this->_makeRequest($method, $params) ) {

			return $data->results;
		}

		return false;
	}

	/**
	 * Get Event Odds Summary
	 * ref.: https://betsapi.com/docs/events/odds.html
	 *
	 * @param array $params[
	 * 		'event_id'=>null		// Event ID you get from events/* (required)
	 * ]
	 *
	 * @return array or json
	 */
	public function getEventOddsSummary($params=null) {

		$method = 'v1/event/odds/summary';
		
		if ( $data=$this->_makeRequest($method, $params) ) {

			return $data->results;
		}

		return false;
	}

	/**
	 * @return string Endpoint da API
	 */
	public function getApiEndpoint() {

		return $this->_config['api_endpoint'];
	}

	/**
	 * Set the Endpoint API
	 *
	 * @return bool true or false
	 */
	public function setApiEndpoint($endpoint=false) {

		if($endpoint) {

			$this->_config['api_endpoint'] = $endpoint;
			return true;
		}

		return false;
	}

	/**
	 * Get the last error returned by the API.
	 * If something didn't work, this should contain the string describing the problem.
	 *
	 * @return  string|false  describing the error
	 */
	public function getLastError() {

		return $this->last_error ?: false;
	}

	/**
	 * Performs the underlying HTTP request. 
	 *
	 * @param  string $method    The API method to be called
	 * @param  array  $args      Assoc array of parameters to be passed
	 * @param int     $timeout
	 *
	 * @return array|false Assoc array of decoded result
	 */
	private function _makeRequest($method, $params=array(), $timeout=30) {

		$params = $this->_makeParams($params);

		$url = $this->_config['api_endpoint'].'/'.$method.'?'.$params;

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$responseContent = json_decode(curl_exec($ch));

		if(is_object($responseContent)) {

			if ($responseContent === false) {

				$info = curl_getinfo($ch);
				curl_close($ch);

				$this->last_error = __('Erro durante o curl exec. '.var_export($info));
				return false;
			}
			elseif ($responseContent->success == 0) {

				$this->last_error = __($responseContent->error);
				return false;
			}
		}

		curl_close($ch);

		return $responseContent;
	}

	/**
	 * Filtra e retorna os parâmetros em string
	 *
	 * @param  array $params
	 *
	 * @return string|false 
	 */
	private function _makeParams($params=array()) {

		$params = array_filter($params);

		$new_params[] = 'token='.$this->_config['api_token'];
		foreach ($params as $key=>$value) {
			
			if(!empty($key) and !empty($value))
				$new_params[] = "$key=$value";
		}
		$str_params = implode('&', array_filter($new_params));

		return $str_params ?: false;	
	}
}
