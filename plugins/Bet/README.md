# Plugin de apostas para CakePHP

## Instalação

Você pode instalar este plugin no seu aplicativo CakePHP usando [composer](http://getcomposer.org).

Instalação via composer:

```
composer require andersoncorso/cakephp-plugin-bet
```

Ativando o plugin via console:
```
bin/cake plugin load Bet
```

Ou você pode ativar manualmente no arquivo `config\bootstrap.php`, inserindo o comando abaixo:
```php
Plugin::load('Bet', ['bootstrap' => false, 'routes' => true]);
```