<?php
namespace Bet\Test\TestCase\View\Helper;

use Bet\View\Helper\BetHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * Bet\View\Helper\BetHelper Test Case
 */
class BetHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Bet\View\Helper\BetHelper
     */
    public $Bet;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Bet = new BetHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bet);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
