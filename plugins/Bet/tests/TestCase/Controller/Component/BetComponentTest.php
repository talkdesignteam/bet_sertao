<?php
namespace Bet\Test\TestCase\Controller\Component;

use Bet\Controller\Component\BetComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * Bet\Controller\Component\BetComponent Test Case
 */
class BetComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Bet\Controller\Component\BetComponent
     */
    public $Bet;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Bet = new BetComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bet);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
