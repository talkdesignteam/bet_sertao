# Bet Sertão

Projeto CakePHP do site de apostas Bet Sertão


## Instalação

- Baixe o repositório:

```
git clone https://andersoncorso@bitbucket.org/talkdesignteam/bet_sertao.git
```

## Configuração:

- Copie o arquivo 'config/schema/app.default.php' para 'config/app.php' e configure o `'Datasources'` alem de qualquer outra configuração relevante.

- Instale as dependencias:
```
composer install
```

- Estruturas de bando de dados principal
```
bin/cake migrations migrate
```

- Plugin [AclManager](https://github.com/ivanamat/cakephp3-aclmanager):
```
bin/cake migrations migrate -p Acl
```

- Plugin [AccessManager](https://github.com/andersoncorso/cakephp-plugin-access_manager):
```
bin/cake migrations migrate -p AccessManager
```
- Descomente a seguinte linha no 'src/Controller/AppController.php':
```
$this->Auth->allow();
```
- crie registros de 'Groups', 'Roles' e 'Users' acessando:
    '.../access-manager/groups';
    '.../access-manager/roles';
    '.../access-manager/users';

- acesse a url '.../AclManager' e clique no link 'Update ACOs and AROs and set default values';

- comente ou exclua a seguinte linha no 'src/Controller/AppController.php':
```
// $this->Auth->allow();
```


### Plugin [Places](https://github.com/andersoncorso/cakephp-plugin_places):

- importe o arquivo de banco de dados com a estrutura e dados de Regiões, Estados e Municípios
```
vendor/andersoncorso/cakephp-plugin_places/config/schema/cakephp_plugin-places.sql
```


## Layout

* Favicon
- Acesse o site [Favicon Generator](https://www.favicon-generator.org/), crie seu favicon e cole dentros do diretório:
```
webroot/img/favicon/
```


* Ps: Tema [AdminLTE](https://github.com/maiconpinto/cakephp-adminlte-theme);
 Por padrão o App utiliza o tema AdminLTE, visite a página oficial para mais detalhes. 

* Ps: Recomenda-se  o uso do Docker com o ambiente: [Docker cakephp lemp](https://github.com/andersoncorso/docker-cakephp-lemp);
