<?php 
	use Cake\I18n\Time;
	use Cake\Core\Configure;
?>
<footer class="main-footer text-muted" style="background: #e2e2e2">
	
	<div class="container">
		
		<!-- Desktop -->
		<section class="hidden-xs">
			<div class="col-md-5">
				<?php 
					$now = Time::now();

					echo '&copy; ';
					echo ($now->year=='2018')? $now->year : '2018-'.$now->year;
					echo ' '.mb_strtoupper( Configure::read('Theme.title') );
					echo ' - '.Configure::read('Theme.description');
				?>		
			</div>
			<div class="col-md-6 col-md-offset-1 text-right text-muted">
			</div>
		</section>

		<!-- Mobile -->
		<section class="visible-xs">
			<div class="col-xs-12 text-center text-muted">
				<?php 
					$now = Time::now();

					echo '&copy; ';
					echo ($now->year=='2018')? $now->year : '2018-'.$now->year;
					echo ' '.mb_strtoupper( Configure::read('Theme.title') );
					echo ' - '.Configure::read('Theme.description');
				?>		
			</div>
		</section>

	</div>
</footer>