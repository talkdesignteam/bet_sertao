<nav class="navbar navbar-static-top">

	<!-- Sidebar toggle button-->
	<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="hidden-sm hidden-md hidden-lg">MENU</span>
	</a>

	<div class="navbar-custom-menu">
		<ul class="nav navbar-nav">

			<!-- Messages: style can be found in dropdown.less-->
			<?php //echo $this->element('messages'); ?>

			<!-- Notifications: style can be found in dropdown.less -->
			<?php //echo $this->element('notifications'); ?>

			<!-- Tasks: style can be found in dropdown.less -->
			<?php //echo $this->element('tasks'); ?>

			<!-- User Account: style can be found in dropdown.less -->
			<?php 
				if ($this->request->session()->read('Auth.User')) {

					echo $this->element('menu_user-account',
						['User'=>$this->request->session()->read('Auth.User')]
					);
				}
				else{
			?>
				<li class="dropdown user user-menu">
				<?php 
					echo $this->Html->link('<i class="fa fa-lock"></i> LOGIN', 
						['controller'=>'Users', 'action'=>'login', 'plugin'=>'AccessManager'],
						['class'=>'', 'escape'=>false, '_full'=>false]
					);
				?>
				</li>
			<?php } ?>

			<!-- Control Sidebar Toggle Button -->
<!-- 			<li>
				<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
			</li>
 -->			
		</ul>
	</div>
</nav>