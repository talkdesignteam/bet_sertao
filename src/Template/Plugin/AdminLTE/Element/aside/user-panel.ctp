<div class="user-panel">
	<div class="pull-left image">
		<?php 
			if (isset($User['profile']['image'])) {

				echo $this->Html->image($User['profile']['image'],
					['class'=>'img-circle', 'alt'=>'User Image']
				);
			}
		?>
	</div>
	<div class="pull-left info">
		<p>
			<?php echo (isset($User['full_name']))? $User['full_name'] : __('Sem nome'); ?>
		</p>
		<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	</div>
</div>