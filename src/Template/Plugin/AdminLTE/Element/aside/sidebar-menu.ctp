<ul class="sidebar-menu">
	<?php 

		// inicio link
		$home_title = '<i class="fa fa-home"></i><span> Início</span>';
		$home_url = ['controller'=>'Bilhetes', 'action'=>'home', 'plugin'=>false, '_full'=>true];
		echo '<li>'.$this->Html->link($home_title, $home_url, ['escape'=>false]).'</li>';
		
		// painel link
		$home_title = '<i class="fa fa-tachometer"></i><span> Painel</span>';
		$home_url = ['controller'=>'Dashboards', 'action'=>'index', 'plugin'=>false];
		echo '<li>'.$this->Html->link($home_title, $home_url, ['escape'=>false]).'</li>';

		if (!empty($menu)) {

			foreach ($menu as $k => $m) {

				# Dropdown menu
				if(isset($m['title']) and isset($m['links'])){
					
					$li_title = '<span>'.__($m['title']).'</span>'; // Link title
					$li_arrow = '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
					$li_icon = '<span class="fa fa-angle-right"></span>';
					echo '<li class="treeview">';
						echo $this->Html->link($m['icon'].' '.$li_title.$li_arrow, '#', ['escape'=>false]);
						echo '<ul class="treeview-menu">';
						foreach ($m['links'] as $title=>$url) {
							
							echo '<li>';
							echo $this->Html->link($li_icon.__($title), $url, ['escape'=>false]);
							echo '</li>';
						}
						echo '</ul>';
					echo '</li>';
						
				}
				# Separetor menu
				elseif(isset($m['separetor'])){

					echo '<li class="header">'.__(mb_strtoupper(current($m))).'</li>' ;
				}
				# Link menu
				else{

					$li_key = '<span>'.__(key($m)).'</span>';
					$li_url = current($m);
					echo '<li>'.$this->Html->link($m['icon'].' '.$li_key, $li_url, ['escape'=>false]).'</li>';
				}
			}
		}

	?>
</ul>