<li class="dropdown user user-menu">
	
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<?php
			$profileImage = (isset($User['profile']['image']))? isset($User['profile']['image']) : 'user-icon.png';
			echo $this->Html->image($profileImage,
				['class'=>'user-image', 'alt'=>'User Image']
			);
		?>
		<span class="hidden-xs">
			<?php echo (isset($User['full_name']))? $User['full_name'] : __('Minha conta'); ?>
		</span>
	</a>

	<ul class="dropdown-menu">
		<?php if(isset($User['profile'])): ?>
		<!-- User image -->
		<li class="user-header">
			<?php echo $this->Html->image('user-icon.png', array('class' => 'img-circle', 'alt' => 'User Image')); ?>

			<p>
				<?= $User['full_name'].' - '.ucfirst($User['role']['name']) ?>
				<!-- <small>Member since Nov. 2012</small> -->
			</p>
		</li>
		<?php endif; ?>
		<!-- Menu Body -->
		<li class="user-body">
			<div class="row">
				<div class="text-center">
					<strong><?= $User['email'] ?></strong><br>
					<small><?= 'Cadastrado em '.$User['created'] ?></small>
				</div>
			</div>
		</li>
		<!-- Menu Footer-->
		<li class="user-footer">
			<div class="pull-left">
				<?php						
					echo $this->Html->link('Editar',
						['controller'=>'Users', 'action'=>'userChangeEmail', $this->request->session()->read('Auth.User.id'), 'plugin'=>'AccessManager'],
						['class'=>'btn btn-default btn-flat', 'escape'=>false, '_full'=>false]
					);
				?>
			</div>
			<div class="pull-right">
				<?php
					echo $this->Html->link('<i class="fa fa-sign-out"></i> Sair',
						['controller'=>'Users', 'action'=>'logout', 'plugin'=>'AccessManager'],
						['class'=>'btn btn-default btn-flat', 'escape'=>false, '_full'=>false]
					);
				?>
			</div>
		</li>
	</ul>

</li>