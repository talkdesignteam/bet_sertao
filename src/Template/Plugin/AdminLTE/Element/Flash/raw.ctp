<?php
if ($this->request->getQuery('escape') !== 'false') {
	$message = h($message);
}
?>
<section class="content-header">
	<div class="alert alert-raw alert-dismissible">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<?= $message ?>
	</div>
</section>