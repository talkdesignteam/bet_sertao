<?php
if ($this->request->getQuery('escape') !== 'false') {
	$message = h($message);
}
?>
<section class="content-header">
    <div class="alert alert-danger alert-dismissible">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-ban"></i> <?= __('Atenção') ?>!</h4>
        <?= $message ?>
    </div>
</section>
