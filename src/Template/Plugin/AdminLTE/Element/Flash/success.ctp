<?php
if ($this->request->getQuery('escape') !== 'false') {
	$message = h($message);
}
?>
<section class="content-header">
    <div class="alert alert-success alert-dismissible">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-check"></i> <?= __('Sucesso') ?>!</h4>
        <?= $message ?>
    </div>
</section>