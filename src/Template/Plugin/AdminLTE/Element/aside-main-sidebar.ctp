<?php use Cake\Core\Configure; ?>
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- Sidebar user panel -->
		<?php //echo $this->element('aside/user-panel', ['User'=>$this->request->session()->read('Auth.User')]); ?>

		<!-- search form -->
		<?php //echo $this->element('aside/form'); ?>

		<!-- sidebar menu: : style can be found in sidebar.less -->
		<?php 
			if( $this->request->session()->read('Auth.User') ) {

				$role_id = $this->request->session()->read('Auth.User.role_id');
				echo $this->element('aside/sidebar-menu', 
					['menu'=>Configure::read('Menu.aside.'.$role_id)]
				);
			}
		?>

	</section>
</aside>