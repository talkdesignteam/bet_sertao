<?php use Cake\Core\Configure; ?>
<!DOCTYPE html>
<html>
<head>

	<?= $this->element('Layout/back-meta_tags') ?>

	<title><?= $title ?></title>

	<!-- Bootstrap 3.3.5 -->
	<?php echo $this->Html->css('AdminLTE./bootstrap/css/bootstrap.min'); ?>

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Theme style and skin -->
	<?php echo $this->Html->css('AdminLTE.AdminLTE.min'); ?>
	<?php //echo $this->Html->css('AdminLTE/skins/skin-'.Configure::read('Theme.skin') .'.min'); ?>
	<?php echo $this->Html->css('AdminLTE/skins/skin-'.Configure::read('Theme.skin')); ?>

	
	<!-- Theme for project -->
	<?php //echo $this->Html->css('AdminLTE/AdminLTE-theme_irrigo'); ?>

	<!-- Default style -->
	<?php //echo $this->Html->css('default'); ?>

	<?php echo $this->fetch('css'); ?>

	<!-- Favicon -->
	<?php
		$path = $this->Url->build('/img/favicon');
		echo $this->element('favicon', ['path'=>$path]);
	?>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- ATENÇÃO - INSERIR NO ARQUIVO CSS -->
	<style>
/*		.actions .fa{font-size: 18px;}
		.actions .btn{font-size: 14px;}
*/	</style>
</head>
<body class="hold-transition skin-<?php echo Configure::read('Theme.skin'); ?> sidebar-mini">

	<!-- Site wrapper -->
	<div class="wrapper">
		<header class="main-header">
			<!-- Logo -->
			<a href="<?php echo $this->Url->build('/'); ?>" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><?= Configure::read('Theme.logo.mini'); ?></span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><?= Configure::read('Theme.logo.large'); ?></span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<?php echo $this->element('nav-top'); ?>
		</header>

		<!-- Left side column. contains the sidebar -->
		<?php echo $this->element('aside-main-sidebar'); ?>

		<!-- =============================================== -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<?php echo $this->Flash->render(); ?>
			<?php echo $this->Flash->render('auth'); ?>
			<?php echo $this->fetch('content'); ?>

		</div>
		<!-- /.content-wrapper -->

		<?php //echo $this->element('footer'); ?>

		<!-- Control Sidebar -->
		<?php //echo $this->element('aside-control-sidebar'); ?>

		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<?php echo $this->Html->script('AdminLTE./plugins/jQuery/jquery-2.2.3.min'); ?>

<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('AdminLTE./bootstrap/js/bootstrap.min'); ?>

<!-- SlimScroll -->
<?php echo $this->Html->script('AdminLTE./plugins/slimScroll/jquery.slimscroll.min'); ?>

<!-- FastClick -->
<?php echo $this->Html->script('AdminLTE./plugins/fastclick/fastclick'); ?>

<!-- AdminLTE App -->
<?php echo $this->Html->script('AdminLTE./js/app.min'); ?>

<!-- AdminLTE for demo purposes -->
<?php echo $this->fetch('script'); ?>
<?php echo $this->fetch('scriptBottom'); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".navbar .menu").slimscroll({
			height: "200px",
			alwaysVisible: false,
			size: "3px"
		}).css("width", "100%");

		var a = $('a[href="<?php echo $this->request->webroot . $this->request->url ?>"]');
		if (!a.parent().hasClass('treeview') && !a.parent().parent().hasClass('pagination')) {
			a.parent().addClass('active').parents('.treeview').addClass('active');
		}
		
		// Loading Button
		$('form').on('submit', function(e) {
			var btn = $('#loadButton').button('loading')
		});

	});
</script>

</body>
</html>
