<?php use Cake\Core\Configure; ?>
<!DOCTYPE html>
<html>
<head>
	
	<?= $this->element('Layout/front-meta_tags') ?>
	
	<title><?= $title ?>Teste</title>
	
	<!-- CSS styles -->
    <?= $this->Html->css('app.css') ?>
    <?= $this->Html->css('plugins.css') ?>
	<?= $this->fetch('css'); ?>

	<!-- Favicon -->
	<?php
		$path = $this->Url->build('/img/favicon');
		echo $this->element('favicon', ['path'=>$path]);
	?>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<!-- Scripts - top -->
	<?= $this->fetch('script'); ?>

</head>
<body>

	<header class="header-site">
		<?= $this->element('Layout/front-header', ['cache'=>false]) ?>
	</header>

	<main class="conteudo-site">
		
		<div class="container">
			<?php echo $this->Flash->render(); ?>
			<?php echo $this->Flash->render('auth'); ?>
		</div>
		<?php echo $this->fetch('content'); ?>

	</main>
	<section class="banners-footer">
		<div class="container">
			<div class="cols">
				<div class="col -seiscolunas">
					<?= $this->Html->image('layout/banner-melhores-cotacoes.jpg', ['class'=>'img-responsive']) ?>
				</div>
				<div class="col -seiscolunas">
					<?= $this->Html->image('layout/banner-faca-sua-aposta.jpg', ['class'=>'img-responsive']) ?>
				</div>
			</div>
		</div>
	</section>

	<footer class="rodape">
		<?= $this->element('Layout/front-rodape') ?>
	</footer>

	<!-- Scripts -->
	<?php 
		echo $this->Html->script([
			'/bower_components/jquery/dist/jquery.min.js',
			'/bower_components/bootstrap-sass/assets/javascripts/bootstrap.min',
			'/bower_components/wow/dist/wow.min',
			'scripts',
			'https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js'
		]);
	?>

	<?= $this->fetch('scriptBottom'); ?>
	<script type="text/javascript">
		$(document).ready(function(){

			// Chat Tawk.to
			Tawk.to
			var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
			(function(){
				var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
				s1.async=true;
				s1.src='https://embed.tawk.to/5b82bc03afc2c34e96e7ea59/default';
				s1.charset='UTF-8';
				s1.setAttribute('crossorigin','*');
				s0.parentNode.insertBefore(s1,s0);
			})();

		});			
	</script>

</body>
</html>