<?php use Cake\Core\Configure; ?>
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?= $title ?></title>

	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<!-- Bootstrap 3.3.5 -->
	<?php echo $this->Html->css('AdminLTE./bootstrap/css/bootstrap.min'); ?>

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Theme style and skin -->
	<?php echo $this->Html->css('AdminLTE.AdminLTE.min'); ?>

	<?php echo $this->fetch('css'); ?>

</head>
<body onload="window.print();">
	
	<div class="container hidden-print">
		<div class="col-xs-12 text-center">
			<br>
			<?php
				echo $this->Html->link('<i class="fa fa-print"></i>&nbsp'.__('imprimir'), 
					'javascript: window.print();',
					['class'=>'btn btn-success btn-lg btn-flat', 'escape'=>false]
				);
				// echo '&nbsp;&nbsp;';
				// echo $this->Html->link('<i class="fa fa-arrow-left"></i> '.__('voltar'),
				// 	'javascript:window.history.back()',
				// 	['escape' => false, 'class'=>'btn btn-default btn-lg btn-flat']
				// );
			?>
			<br><br><br>
		</div>
	</div>
	
	<!-- Print content -->
	<?php echo $this->fetch('content'); ?>

	<!-- jQuery 2.2.3 -->
	<?php echo $this->Html->script('AdminLTE./plugins/jQuery/jquery-2.2.3.min'); ?>

</body>
</html>