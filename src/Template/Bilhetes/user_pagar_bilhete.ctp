<section class="content-header">
	<h1>
		<?= __('Pagar bilhete') ?>&nbsp;
		<div class="pull-right"></div>
	</h1>
	<ol class="breadcrumb">
		<li>
			<?php 
				echo $this->Html->link('<i class="fa fa-angle-double-left"></i> '.__('Voltar'),
					'javascript:window.history.back()',
					['escape' => false]
				);
			?>
		</li>
	</ol>
</section>

<section class="content">
	
	<!-- pesquisa -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-solid">
				<div class="box-body" style="padding:15px 0px">
					<?= $this->element('Forms/bilhetes-user_pagar_bilhete-filter') ?>
				</div>
			</div>
		</div>
	</div>

	<?php if(!empty($bilhete)): ?>
		
		<?php if($bilhete->won===true): ?>

			<div class="row">
				<div class="col-xs-12">
					<div class="box box-solid">
						<div class="box-body">
							<?= $this->element('Table/bilhetes-user_pagar_bilhete') ?>
						</div>
					</div>
				</div>
			</div>

		<?php elseif(!is_null($bilhete->won)): ?>
			
			<div class="alert alert-warning">
				<?= __('Este bilhete não foi premiado e por isso não pode ser pago!') ?>
			</div>

		<?php else: ?>
			
			<div class="alert alert-info">
				<?= __('Bilhete aguardando resultados!') ?>
			</div>

		<?php endif; ?>

	<?php elseif(isset($bilhete)): ?>

		<div class="alert alert-info"><?= __('Bilhete inexistente.') ?></div>

	<?php endif; ?>

</section>

<?php
$this->Html->css([
	// ''
],
['block' => 'css']);

$this->Html->script([
	// ''
],
['block' => 'script']);

	// Loading GIF
	$loading = $this->Html->image('loading.gif', ['style'=>'height:20px; margin:0 auto;']);
?>
<?php $this->start('scriptBottom'); ?>
<script>
  $(function () {
	
	// Loading clicks
	$('.loading').on('click', function () {
		$(this).html('confirmando &nbsp;&nbsp;<?= $loading ?>');
	});

  });
</script>
<?php $this->end(); ?>