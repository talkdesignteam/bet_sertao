<?php $this->layout = 'login'; ?>

<!-- page header --> 
<section class="content-header">
	<h1>
		<?= __('Atualização de Apostas') ?>&nbsp;
	</h1>
</section>

<!-- page content -->
<section class="content">
	<div class="row">

		<div class="col-xs-12">
		<?php
			if(isset($count_saved))
				echo "Bilhetes atualizados: $count_saved <br>";
			
			if(isset($count_not_saved))
				echo "Bilhetes não atualizados: $count_not_saved <br>";

			if(isset($bilhetes))
				echo 'Total: '.count($bilhetes).'<br>';
		?>
		</div>

	</div>
</section>