<?php
	// Incluindo arquivos css no bloco para o layout
	$this->Html->css([
		// 'print',
	],
	['block' => 'css']);

	$opt_dinheiro = [
		'places' => 2,
		'before' => 'R$ '
	];
?>

<div class="container">
	<div class="col-xs-12">
		
		<h1>BetSertao365.com</h1>

		<!-- bilhete -->
		<?php if(!empty($bilhete)): ?>
			
			<h3><?= __('Bilhete'). " #$bilhete->id" ?></h3>
			<?php if($bilhete->is_active and $bilhete->is_approved and is_null($bilhete->won)): ?>
				<div class="alert alert-info"><?= __('Aguardando resultado!') ?></div>
			<?php endif; ?> 
			<?php if( $this->request->is('mobile') ): ?>
			<div class="table-responsive">
			<?php endif;?>
			<table class="table table-bordered table-striped text-center">
				<thead>
					<th><?= __('Apostador') ?></th>
					<th><?= __('Revendedor') ?></th>
					<th><?= __('Valor apostado') ?></th>
					<th><?= __('Prêmio') ?></th>
					<th><?= __('Lucro Revendedor') ?></th>
					<th><?= __('Total') ?></th>
					<th><?= __('Premiado?') ?></th>
				</thead>
				<tbody>
					<tr>
						<td><?= h($bilhete->apostador) ?></td>
						<td><?= h($bilhete->revendedor) ?></td>
						<td>
						<?php 
							echo $this->Number->format($bilhete->valor_apostado, [
								'places' => 2,
								'before' => 'R$ '
							]); 
						?>
						</td>
						<td title="Prêmio atual (inicial)">
						<?php 
							echo $this->Number->format($bilhete->valor_premio_real, $opt_dinheiro);
							echo ' <small class="text-muted">('. $this->Number->format($bilhete->valor_premio, $opt_dinheiro).')</small>';
						?>	
						</td>
						<td title="Lucro atual (inicial)">
						<?php 
							echo $this->Number->format($bilhete->valor_revendedor_real, $opt_dinheiro);
							echo ' <small class="text-muted">('. $this->Number->format($bilhete->valor_revendedor, $opt_dinheiro).')</small>';
						?>	
						</td>
						<td title="Total atual (inicial)">
						<?php 
							echo $this->Number->format($bilhete->valor_total_real, $opt_dinheiro);
							echo ' <small class="text-muted">('. $this->Number->format($bilhete->valor_total, $opt_dinheiro).')</small>';
						?>	
						</td>
						<td>
						<?php
							if ($bilhete->won!==null and $bilhete->is_approved) {

								echo ($bilhete->won)?
									'<span class="badge bg-default">Sim</span>'
									: '<span class="badge bg-default">Não</span>';
							}
							else{

								echo '-';
							}
						?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php if( $this->request->is('mobile') ): ?>
			</div>
			<?php endif;?>

		<?php endif; ?>

		<!-- apostas -->
		<?php if(isset($bilhete->apostas)): ?>

			<h3><?= __('Apostas') ?></h3>
			<?php if( $this->request->is('mobile') ): ?>
			<div class="table-responsive">
			<?php endif;?>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th><?= __('Partida') ?></th>
						<th class="text-center"><?= __('Palpite') ?></th>
						<th class="text-center"><?= __('Situação') ?></th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$premio = 0;
					$total_premios = 0;
					foreach ($bilhete->apostas as $k=>$p) {

						$partida_date = (!empty($p->partida_time))? date('d/m/Y H:i:s', $p->partida_time) : '-';
						$palpite = $this->Bilhete->palpiteAposta($p->palpite_id);
						$palpite_odd = $this->Bilhete->formatPalpiteOdd($p->palpite_odd);
						$status = '-';
						if ($p->won!==null) {

							$status = ($p->won)?
								'<span class="badge bg-default">Acertou</span>'
								: '<span class="badge bg-default">Errou</span>';
						}

						echo "<tr>";
						echo "<td class='text-center'>$p->id</td>";
						echo "<td><strong>$p->league_name</strong><br>$p->home_name x $p->away_name<br><small>$partida_date</small></td>";
						echo "<td class='text-center'><strong>$palpite</strong><br>$palpite_odd</td>";
						echo "<td class='text-center'><strong>$status</strong><br>$p->score</td>";
						echo "</tr>";
					}
				?>	
				</tbody>
			</table>
			<?php if( $this->request->is('mobile') ): ?>
			</div>
			<?php endif;?>

		<?php endif; ?>
	</div>
</div>