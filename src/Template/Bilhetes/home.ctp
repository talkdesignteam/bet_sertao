<?php
	// Definindo o layout da página
	$this->layout = 'AdminLTE.front';
	
	// Incluindo arquivos css no bloco para o layout
	$this->Html->css([
		'home',
		'https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css'
	],
	['block' => 'css']);

	// Incluindo arquivos de javascript no bloco para o layout
	$this->Html->script([
		// 'https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js'
	],
	['block' => 'scriptBottom']);
?>
<div class="slide-home">
	<div class="box-chamada">
		<div class="container">
			<h1 class="titulo">O melhor site de apostas da região</h1>
			<p>Venha fazer parte desse time de campeões, faça agora mesmo sua aposta e ganhe com o seu time!</p>
		</div>
	</div>
</div>
<div class="container ajax-form">
	
	<!-- Banner -->
	<div class="content">
		<div class="banner">
			<?= $this->Html->image('layout/banner-aposte-ja.jpg', ['class'=>'img-responsive']) ?>
		</div>
	</div>
	
	<div id="responseMessage"></div>

	<!-- Form -->
	<?= $this->Form->create($bilhete, ['id'=>'formApostas', 'url'=>['controller'=>'Bilhetes', 'action'=>'home', '_ext'=>'json', 'plugin'=>false, 'admin'=>false]]) ?>
	<div class="cols">
		
		<!-- left side (partidas disponíveis) -->
		<div class="col -oitocolunas content-center">
			<section class="jogos-disponiveis">  


				<!-- Novo layout de partidas-->						
				<div class="content-partidas">
					<div class="partidas">
					<?php foreach($partidas as $league_name=>$data): ?>
						<div class="campeonato">
							<?= $league_name ?>
						</div>
						<div class="infopartida">
							<div class="dados-partida">
								<div class="btn-group btn-group-sm pull-right" role="group" aria-label="...">
									<button type="button" class="btn btn-default">Casa</button>
									<button type="button" class="btn btn-default">Empate</button>
									<button type="button" class="btn btn-default">Fora</button>
									<button type="button" class="btn btn-default">+</button>
								</div>
							</div>
							<?php foreach($data as $k=>$p): ?>
							<?php

								// prepara variáveis
								$odds = trim($p->odds, '"');
								$odds = stripcslashes($odds);
								$odds = json_decode($odds);
								$opt = [
									'home_od'=>$this->Number->precision($odds->home_od, 2),
									'draw_od'=>$this->Number->precision($odds->draw_od, 2),
									'away_od'=>$this->Number->precision($odds->away_od, 2)
								];
							?>
							<div class="bloco-partida">
								<div class="jogo pull-left">
									<?= $p->home_name.' x '.$p->away_name ?>
									<span class="hora"><?= date('d/m/Y', $p->time).' '.date('H', $p->time).'h'.date('i', $p->time) ?></span>
								</div>
								<div class="opcoes pull-right">
									<?php 
										// palpite
										echo $this->Form->input('apostas.'.$p->id.'.palpite_id', [
											'type' => 'radio',												
											'templates' => [
												'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}}>{{text}}</label>',
												'radioWrapper' => '<span class="cotacao">{{label}}</span>&nbsp;'
											],
											//'type' => 'radio',
											'options' => $opt,
											'label' => false,
											'required'=>false,
											'hiddenField' => false,
											'class'=>'waschecked'
										]);

										// outros
										echo $this->Form->hidden('apostas.'.$p->id.'.partida_id', ['value'=>$p->id]);
										echo $this->Form->hidden('apostas.'.$p->id.'.partida_time', ['value'=>$p->time]);
										echo $this->Form->hidden('apostas.'.$p->id.'.league_name', ['value'=>$p->league_name]);
										echo $this->Form->hidden('apostas.'.$p->id.'.home_name', ['value'=>$p->home_name]);
										echo $this->Form->hidden('apostas.'.$p->id.'.away_name', ['value'=>$p->away_name]);
										echo $this->Form->hidden('apostas.'.$p->id.'.odds', ['value'=>$p->odds]);
									?>
									<a class="botao -mais" data-toggle="modal" data-target="#MaisOpcoes" style="cursor:pointer;">+00</a>
								</div>		
							</div>
							<?php endforeach; ?>
						</div>
					<?php endforeach; ?>
					</div>
				</div><!-- fim do novo layout-->

				<!-- Partidas e odds -->
				<?php if(1==2): ?>
				<div class="table-content">
					<div  class="table-responive">
						<table id="TabelaPartidas" class="toggle-radio-buttons resultado-filtro table table-hover table-striped" style="width:100%">
							<thead class="header-partidas">
								<tr>
									<th><?= __('Partidas disponíveis') ?></th>
									<th><?= __('Casa') ?></th>
									<th><?= __('Empate') ?></th>
									<th><?= __('Fora') ?></th>
									<th><?= __('Mais') ?></th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($partidas)): ?>

								<?php foreach($partidas as $k=>$p): ?>
									<?php
										$odds = trim($p->odds, '"');
										$odds = stripcslashes($odds);
										$odds = json_decode($odds);

										$opt = [
											'home_od'=>$this->Number->precision($odds->home_od, 2),
											'draw_od'=>$this->Number->precision($odds->draw_od, 2),
											'away_od'=>$this->Number->precision($odds->away_od, 2)
										];
									?>
									<tr>
										<td>
											<span class="partida"><?= $p->home_name.' x '.$p->away_name ?></span>
											<span class="hora">
											<?php 
												echo date('d/m/Y', $p->time).' '.date('H', $p->time).'h'.date('i', $p->time).' - '.__($p->league_name);
											?>												
											</span>
										</td>										
										<?php 
											// palpite
											echo $this->Form->input('apostas.'.$p->id.'.palpite_id', [
												'type' => 'radio',												
												'templates' => [
													'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}}>{{text}}</label>',
													'radioWrapper' => '<td class="cotacao">{{label}}</td>'
												],
												//'type' => 'radio',
												'options' => $opt,
												'label' => false,
												'required'=>false,
												'hiddenField' => false,
												'class'=>'waschecked'
											]);

											// outros
											echo $this->Form->hidden('apostas.'.$p->id.'.partida_id', ['value'=>$p->id]);
											echo $this->Form->hidden('apostas.'.$p->id.'.partida_time', ['value'=>$p->time]);
											echo $this->Form->hidden('apostas.'.$p->id.'.league_name', ['value'=>$p->league_name]);
											echo $this->Form->hidden('apostas.'.$p->id.'.home_name', ['value'=>$p->home_name]);
											echo $this->Form->hidden('apostas.'.$p->id.'.away_name', ['value'=>$p->away_name]);
											echo $this->Form->hidden('apostas.'.$p->id.'.odds', ['value'=>$p->odds]);
										?>
										<td>
											<a class="botao -mais" data-toggle="modal" data-target="#MaisOpcoes" style="cursor:pointer;"><i class="fa fa-eye"></i> Mais</a>
										</td>
									</tr> 
								<?php endforeach; ?>
							<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
				<?php endif; ?>

			</section>
		</div>
		
		<!-- Modal Mais opções-->
		<div class="modal fade" id="MaisOpcoes" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php if(1==1): ?>
						<div class="modal-header">
							<h1 class="text-danger">Em desenvolvimento!<br><small>logo logo estará disponível ;)</small></h1>
						</div>
					<?php else: ?>
						<div class="modal-header">
							<h5 class="modal-title float-left" id="ModalLabel">Flamengo X Ponte Preta - 09/09/2018 17h</h5>
							<!--
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
							-->
						</div>
						<div class="modal-body">
							<!--Linha com as opções-->
							<div class="row">
								<div class="box-opcao">
									<header class="header-opcao">
										<h3>Ambas marcam</h3>
									</header>
									<div class="content-opcao">
										<div class="itemopcao">
											<div class="texto">
												Ambas
											</div>
											<div class="acao cotacao">										
												<input type="radio" name="AmbasMarcam" value="AmbasMarcam"/>
												<label>2,29</label>
											</div>
										</div>
										<div class="itemopcao">
											<div class="texto">
												Apenas uma marca
											</div>
											<div class="acao cotacao">										
												<input type="radio" name="ApenasUma" value="ApenasUma"/>
												<label>1,29</label>
											</div>
										</div>	
									</div>
								</div>								
							</div><!--.Fima da linha com opções-->					
						</div>
					<?php endif; ?>			
				</div>
			</div>
		</div>

		<!-- right side (apostas) -->
		<div class="col -quatrocolunas content-right responseContent">
			<section class="box-sidebar">
				<div class="painel-aposta wow slideInRight" data-wow-duration="1s" data-wow-delay="1s">
					<div class="header">
						<h3><?= __('Cupom de aposta') ?></h3>
					</div>
					<div class="formulario-aposta">
						<?= $this->element('Forms/bilhetes-home-form_bilhete') ?>
					</div>
				</div>
			</section>
		</div>

	</div>
	<?php $this->Form->end(); ?>

</div>

<?php $this->start('scriptBottom'); ?>
<script>
$(document).ready(function(){

	// on blue valor apostado
	$(document).on('blur', '.atualizaPainelAposta', function(event) {
		atualizaPainelAposta();
	});

	// check Palpite e atualizao painel de aposta
	$(document).on('click', '.waschecked', function(event) {
		wasChecked($(this));
		atualizaPainelAposta();
	});

	// atualiza o painel do bilhete
	function atualizaPainelAposta() {

		var form = $('.ajax-form form');
		$.ajax({

			beforeSend: function(xhr) {

				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			},
			type: "POST",
			url: "<?= $this->Url->build(['controller'=>'Bilhetes', 'action'=>'atualizaPainelAposta', '_ext'=>'json'], ['fullBase'=>true]) ?>",
			data: form.serialize()
			
		}).done(function(response) {
			
			// alert(JSON.stringify(response));
			$('.responseContent').html(response.content);

		}).fail(function(response) {

			// alert(JSON.stringify(response));
			var errorMessage = '<div class="alert alert-danger">Erro interno. Por favor, avise-nos!</div>';
			$('.responseContent').html(errorMessage);
			console.log(response.error);
		});
		
		// loading 
		$('.responseContent .loading').html('<div class="alert alert-info">Aguarde!</div>');
	}

	// toggle radio checked
	function wasChecked(radio) {
		
		if (radio.data('waschecked') == true) {

			radio.prop('checked', false);
			radio.data('waschecked', false);
		}
		else{

			radio.data('waschecked', true);
		}
	}
	  
	// Form when submit
	$(document).on('submit', '.ajax-form form', function(event) {

		var form = $(this);
		$.ajax({

			beforeSend: function(xhr) {

				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			},
			type: form.attr('method'),
			url: form.attr('action'),
			data: form.serialize()
			
		}).done(function(response) {
			
			// alert(JSON.stringify(response));
			$('.responseContent').html(response.content);

		}).fail(function(response) {

			var errorMessage = '<div class="alert alert-danger">Erro interno. Por favor, avise-nos!</div>';
			$('.responseContent').html(errorMessage);
			console.log(response.error);
		});

		event.preventDefault(); // Prevent the form from submitting via the browser.
	});

	$('#TabelaPartidas').DataTable({
		"stateSave": true, 		// Salva o estado do filtro mesmo após reload de página e etc.
		"paging": true,			// Exibe paginação
		"lengthChange": false, 	// 
		"pageLength": 25,		// Num de registros por página
		"searching": true,		// Exibir filtro de pesquisa
		"ordering": true,		// Ordenar colunas
		"info": true,			// Informações da tabela
		"autoWidth": true,		// Auto redimensionamento da tabela
		"responsive": true,
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
		}
	});

	function AddLoading(element) {
		if ($(element).hasClass('-ativa') == false) {
			$(element).html("<i class=\"fa fa-refresh fa-spin fa-fw\"></i>");
		}
	}

	$('.botao').click(function(){
		$('.botao').toggleClass('-ativa');
		$('.-cotacao').toggleClass('-ativa');
		$(this).toggleClass('-ativa');
	});

});	
</script>
<?php $this->end(); ?>