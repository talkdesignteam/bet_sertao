<?php if($success): ?>

	<div class="alert alert-success alert-dismissible">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<?= $message ?>
	</div>

	<!-- Modal pré-bilheite -->
	<div class="modal fade" id="GerarBilhete" tabindex="-1" role="dialog" aria-labelledby="GerarBilheteLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Aposta realizada com sucesso</h4>
				</div>
				<div class="modal-body">
					<div class="numero-pin">
						<h1>pin: <?= $bilhete->id ?></h1>
					</div>
					<div class="alert alert-success">
						Anote o codigo acima e entre em contato com um de nossos representantes para validar seu bilhete. 
						Esta aposta está disponível para validação até <?= $bilhete->expiration_datetime ?>
					</div>
				</div>
				<div class="modal-footer">
					<div class="representante pull-left"><a href="">Falar com representante</a></div>
					<button type="button" class="botao -verde" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('#GerarBilhete').modal('toggle');
	</script>

	<?php 
		echo $this->Html->link('Nova aposta',
			['controller'=>'Bilhetes', 'action'=>'home'],
			['class'=>'btn btn-primary']
		);
	?>

<?php else: ?>
	
	<div class="alert alert-danger alert-dismissible">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<h4><i class="icon fa fa-ban"></i> <?= __('Atenção') ?>!</h4>
		<?= $message ?>
	</div>

	<section class="box-sidebar">
		<div class="painel-aposta">
			<div class="header">
				<h3><?= __('Cupom de aposta') ?></h3>
			</div>
			<div class="formulario-aposta">
				<?= $this->element('Forms/bilhetes-home-form_bilhete') ?>
			</div>
		</div>
	</section>

<?php endif; ?>
