<section class="content-header">
	<h1>
		<?= __('Meu caixa') ?>&nbsp;
		<div class="pull-right"></div>
	</h1>
	<ol class="breadcrumb">
		<li>
			<?php 
				echo $this->Html->link('<i class="fa fa-angle-double-left"></i> '.__('Voltar'),
					'javascript:window.history.back()',
					['escape' => false]
				);
			?>
		</li>
	</ol>
</section>

<section class="content">
	
	<!-- pesquisa -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-solid">
				<div class="box-body" style="padding-bottom:0px; padding-top:15px">
					<?= $this->element('Forms/bilhetes-user_caixa-filter') ?>
				</div>
			</div>
		</div>
	</div>

	<?php if(!empty($bilhetes)): ?>

		<?php if(!empty($resumo)): ?>
		<!-- resumo -->
		<div class="row">
			<!-- total apostado -->
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="fa fa-money"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Total Apostado</span>
						<span class="info-box-number">
						<?php 
							echo $this->Number->format($resumo['total_apostas'], [
								'places' => 2,
								'before' => 'R$ '
							]); 
						?>
						</span>
					</div>
				</div>
			</div>
			<!-- total prêmios -->
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-trophy"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Total de prêmios</span>
						<span class="info-box-number">
						<?php 
							echo $this->Number->format($resumo['total_premios_real'], [
								'places' => 2,
								'before' => 'R$ '
							]);
							$premio_porcento = round(($resumo['total_premios_pago']/$resumo['total_premios_real'])*100, 0);
						?>
						</span>
						<div class="progress">
							<div class="progress-bar bg-green" style="width: <?= $premio_porcento ?>%"></div>
						</div>
						<span class="progress-description">
							Á pagar: 
							<?php 
								echo $this->Number->format($resumo['total_premios_receber'], [
									'places' => 2,
									'before' => 'R$ '
								]);
							?>
						</span>
					</div>
				</div>
			</div>
			<!-- total lucro -->
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-yellow"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Meu lucro</span>
						<span class="info-box-number">
						<?php 
							echo $this->Number->format($resumo['total_lucro_real'], [
								'places' => 2,
								'before' => 'R$ '
							]);
							$lucro_porcento = round(($resumo['total_lucro_pago']/$resumo['total_lucro_real'])*100, 0);
						?>
						</span>
						<div class="progress">
							<div class="progress-bar bg-yellow" style="width: <?= $lucro_porcento ?>%"></div>
						</div>
						<span class="progress-description">
							Á receber: 
							<?php 
								echo $this->Number->format($resumo['total_lucro_receber'], [
									'places' => 2,
									'before' => 'R$ '
								]);
							?>
						</span>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-muted">
					<div class="box-header with-border">
						<h3 class="box-title"><?= __('Bilhetes') ?></h3>
					</div>
					<div class="box-body">
						<?= $this->element('Table/bilhetes-user_caixa') ?>
					</div>
				</div>
			</div>
		</div>
		
	<?php elseif(isset($bilhetes)): ?>

		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-info">
					<?= __('Nenhum bilhete encontrado.') ?>
				</div>
			</div>
		</div>

	<?php endif; ?>

</section>

<?php
$this->Html->css([
	'AdminLTE./plugins/datatables/dataTables.bootstrap',
	'AdminLTE./plugins/datepicker/datepicker3'
],
['block' => 'css']);

$this->Html->script([
	'AdminLTE./plugins/datatables/jquery.dataTables.min',
	'AdminLTE./plugins/datatables/dataTables.bootstrap.min',
	'AdminLTE./plugins/input-mask/jquery.inputmask',
	'AdminLTE./plugins/input-mask/jquery.inputmask.date.extensions',
	'AdminLTE./plugins/datepicker/bootstrap-datepicker',
	'AdminLTE./plugins/datepicker/locales/bootstrap-datepicker.pt-BR'
],
['block' => 'script']);

	// URL for dataTables translate file
	$url_dataTable_translate_file = $this->Url->assetUrl('/datatables/Portuguese-Brasil.json', ['fullBase'=>true]);
?>
<?php $this->start('scriptBottom'); ?>
<script>
  $(function () {
	
	//Datemask mm/dd/yyyy
	$(".datepicker")
		.inputmask("dd-mm-yyyy", {"placeholder": "__-__-____"})
		.datepicker({
			language:'pt-BR',
			format: 'dd-mm-yyyy'
		});

	$('#tbList').DataTable({
		"stateSave": true, 		// Salva o estado do filtro mesmo após reload de página e etc.
		"paging": true,			// Exibe paginação
		"lengthChange": true, 	// 
		"pageLength": 25,		// Num de registros por página
		"searching": true,		// Exibir filtro de pesquisa
		"ordering": false,		// Ordenar colunas
		"info": true,			// Informações da tabela
		"autoWidth": true,		// Auto redimensionamento da tabela
		"language": {
			"url": "<?= $url_dataTable_translate_file ?>" // "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
		},
	});

  });
</script>
<?php $this->end(); ?>