<section class="content-header">
	<h1>
		<?= __('Consultar bilhete') ?>&nbsp;
		<div class="pull-right"></div>
	</h1>
	<ol class="breadcrumb">
		<li>
			<?php 
				echo $this->Html->link('<i class="fa fa-angle-double-left"></i> '.__('Voltar'),
					'javascript:window.history.back()',
					['escape' => false]
				);
			?>
		</li>
	</ol>
</section>

<section class="content">
	
	<!-- pesquisa -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-solid">
				<div class="box-body" style="padding:15px 0px">
					<?= $this->element('Forms/bilhetes-view-filter') ?>
				</div>
			</div>
		</div>
	</div>

	<?php if(!empty($bilhete)): ?>

		<div class="row">
			<div class="col-xs-12">
				<div class="box box-muted">
					<div class="box-header with-border">
						<h3 class="box-title"><?= __('Bilhete').'&nbsp;#'.$bilhete->id ?></h3>
					</div>
					<div class="box-body">
						<?= $this->element('Table/bilhetes-view') ?>
					</div>
					<div class="box-footer">
						<?php 

							// button - pagar bilhete
							if($bilhete->won===true and (!$bilhete->pgto_premio or !$bilhete->pgto_revendedor)) {

								echo '&nbsp;';
								echo $this->Html->link('<i class="fa fa-money"></i>&nbsp'.__('pagar bilhete'),
									['controller'=>'Bilhetes', 'action'=>'userPagarBilhete', '?'=>['id'=>$bilhete->id]],
									['escape'=>false, 'class'=>'btn btn-success', 'title'=>'Pagar bilhete']
								);
							}

							// button - imprimir
							if($bilhete->is_active and $bilhete->is_approved) {

								echo '&nbsp;&nbsp;';
								echo $this->Html->link('<i class="fa fa-print"></i>&nbsp'.__('imprimir'),
									['controller'=>'Bilhetes', 'action'=>'imprimir', $bilhete->id],
									['escape'=>false, 'class'=>'btn btn-default', 'title'=>'Imprimir', 'target'=>'_blank']
								);
							}

							// button - validar bilhete
							if($bilhete->is_active and !$bilhete->is_approved and !$bilhete->expiration_datetime->isPast()) {

								echo '&nbsp;';
								$confirmMessage = __("Confirma a validação do bilhete $bilhete->id?");
								echo $this->Form->postLink(
									'<i class="fa fa-check"></i>&nbsp;'.__('validar'),
									['action'=>'validar', $bilhete->id],
									['confirm'=>__("$confirmMessage"), 'escape'=>false, 'class'=>'btn btn-warning']
									
								);
							}

							// excluir
							echo '&nbsp;';
							$confirm = __('Atenção, este bilhete e suas apostas serão excluídos!');
							echo $this->Form->postLink(
								'<i class="fa fa-trash"></i> '.__('Excluir'),
								['action'=>'delete', $bilhete->id],
								['confirm'=>__("$confirm"), 'escape'=>false, 'class'=>'btn text-muted pull-right loading', 'title'=>__('Excluir bilhete')]
							);

							// buttons - ativar e desativar bilhete
							if ($bilhete->is_active) {

								echo '&nbsp;';
								$confirm = __('O bilhete e suas apostas serão canceladas.');
								echo $this->Form->postLink(
									'<i class="fa fa-ban"></i> '.__('Desativar'),
									['action'=>'desativar', $bilhete->id],
									['confirm'=>__("$confirm"), 'escape'=>false, 'class'=>'btn text-muted pull-right loading', 'title'=>__('Cancelar bilhete')]
								);
							}
							else{

								$confirm = __('O bilhete e suas apostas serão ativados.');
								echo $this->Form->postLink(
									'<i class="fa fa-circle-o"></i> '.__('Ativar'),
									['action'=>'ativar', $bilhete->id],
									['confirm'=>__("$confirm"), 'escape'=>false, 'class'=>'btn text-primary pull-right loading', 'title'=>__('Ativar bilhete')]
								);
							}
						?>
					</div>
				</div>
			</div>
		</div>

	<?php elseif(isset($bilhete)): ?>

		<div class="alert alert-info"><?= __('Bilhete inexistente.') ?></div>

	<?php endif; ?>

</section>

<?php
$this->Html->css([
	//code here
],
['block' => 'css']);

$this->Html->script([
	//code here
],
['block' => 'script']);

	// Loading GIF
	$loading = $this->Html->image('loading.gif', ['style'=>'height:20px; margin:0 auto;']);
?>
<?php $this->start('scriptBottom'); ?>
<script>
$(function () {

	// Loading clicks
	$('.loading').on('click', function () {
		$(this).html('<?= $loading ?>');
	});

});
</script>
<?php $this->end(); ?>