<?php
	// Definindo o layout da página
	$this->layout = 'AdminLTE.front';
	
	// Incluindo arquivos css no bloco para o layout
	$this->Html->css([
		'bilhetes',
		// 'AdminLTE./bootstrap/css/bootstrap.min',
		// 'AdminLTE.AdminLTE.min'
	],
	['block' => 'css']);

	// Incluindo arquivos de javascript no bloco para o layout
	$this->Html->script([
		// 'filename'
	],
	['block' => 'scriptBottom']);
?>

<div class="container">
	
	<!-- header -->
	<section class="page-header">
		<h1 class="titulo-pagina"><?= __('Consultar bilhete') ?></h1>
	</section>

	<section class="conteudo">
		
		<!-- pesquisa -->
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-solid">
					<div class="box-body" style="padding:15px 0px">
						<?= $this->element('Forms/bilhetes-consultar_bilhete-filter') ?>
					</div>
				</div>
			</div>
		</div>

		<?php if(!empty($bilhete)): ?>

			<div class="col-xs-12">
				<div class="box box-muted">
					<div class="box-header with-border">
						<h3 class="box-title"><?= __('Bilhete').'&nbsp;#'.$bilhete->id ?></h3>
					</div>
					<div class="box-body">
						<?= $this->element('Table/bilhetes-consultar_bilhete') ?>
					</div>
					<div class="box-footer">
						<?php 
							// button - imprimir
							if($bilhete->is_active and $bilhete->is_approved) {

								echo $this->Html->link('<i class="fa fa-print"></i>&nbsp'.__('imprimir'),
									['controller'=>'Bilhetes', 'action'=>'imprimirBilhete', $bilhete->id],
									['escape'=>false, 'class'=>'btn btn-default btn-lg', 'title'=>'Imprimir', 'target'=>'_blank']
								);
							}
						?>
					</div>
				</div>
			</div>

		<?php elseif(isset($bilhete)): ?>
			
			<div class="col-xs-12">
				<div class="alert alert-info"><?= __('Bilhete inexistente.') ?></div>
			</div>

		<?php endif; ?>

	</section>
	
</div>


<?php
$this->Html->css([
	'AdminLTE./plugins/datatables/dataTables.bootstrap',
	'AdminLTE./plugins/datepicker/datepicker3'
],
['block' => 'css']);

$this->Html->script([
	'AdminLTE./plugins/datatables/jquery.dataTables.min',
	'AdminLTE./plugins/datatables/dataTables.bootstrap.min',
	'AdminLTE./plugins/input-mask/jquery.inputmask',
	'AdminLTE./plugins/input-mask/jquery.inputmask.date.extensions',
	'AdminLTE./plugins/datepicker/bootstrap-datepicker',
	'AdminLTE./plugins/datepicker/locales/bootstrap-datepicker.pt-BR'
],
['block' => 'script']);

	// URL for dataTables translate file
	$url_dataTable_translate_file = $this->Url->assetUrl('/datatables/Portuguese-Brasil.json', ['fullBase'=>true]);

	// Loading GIF
	$loading = $this->Html->image('loading.gif', ['style'=>'height:20px; margin:0 auto;']);
?>
<?php $this->start('scriptBottom'); ?>
<script>
$(function () {

	//Datemask mm/dd/yyyy
	$(".datepicker")
		.inputmask("dd-mm-yyyy", {"placeholder": "__-__-____"})
		.datepicker({
			language:'pt-BR',
			format: 'dd-mm-yyyy'
		});

	// DataTables
	$('#tbList').DataTable({
		"stateSave": true, 		// Salva o estado do filtro mesmo após reload de página e etc.
		"paging": true,			// Exibe paginação
		"lengthChange": true, 	// 
		"pageLength": 25,		// Num de registros por página
		"searching": true,		// Exibir filtro de pesquisa
		"ordering": false,		// Ordenar colunas
		"info": true,			// Informações da tabela
		"autoWidth": true,		// Auto redimensionamento da tabela
		"language": {
			"url": "<?= $url_dataTable_translate_file ?>" // "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
		},
	});

	// Loading clicks
	$('.loading').on('click', function () {
		$(this).html('<?= $loading ?>');
	});

});
</script>
<?php $this->end(); ?>