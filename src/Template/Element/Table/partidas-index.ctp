<?php 
	$opt_dinheiro = [
		'places' => 2,
		'before' => 'R$ '
	];
?>

<?php if( $this->request->is('mobile') ): ?>
<div class="table-responsive">
<?php endif;?>
	<table id="tbList" class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th><?= __('Partida') ?></th>
				<th><?= __('Campeonato') ?></th>
				<th><?= __('Data') ?></th>
				<th><?= __('Casa') ?></th>
				<th><?= __('Empate') ?></th>
				<th><?= __('Fora') ?></th>
				<th><?= __('Status') ?></th>
				<th><?= __('Ações') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($partidas as $k=>$p): ?>
			<?php 
				// line style
				$status_tr = '';
				if($p->is_active===true) $status_tr = ' ';
				if($p->is_active===false) $status_tr = ' text-danger';
				if($p->is_active!==true) $status_tr = ' text-muted';
			?>
			<tr>
				<td>
				<?php 
					echo h($p->home_name);
					echo '&nbsp;<span class="fa fa-times"></span>&nbsp;';
					echo h($p->away_name);
				?>
				</td>
				<td><?= h($p->league_name) ?></td>
				<td><?= h($p->inicio) ?></td>
				<td><?= h($p->home_od) ?></td>
				<td><?= h($p->draw_od) ?></td>
				<td><?= h($p->away_od) ?></td>
				<td>
				<?php
					echo ($p->is_active)?
						'<span class="badge bg-green">Ativado</span>'
						: '<span class="badge bg-red">Desativado</span>';
				?>
				</td>
				<td class="actions" style="white-space:nowrap">
					<?= $this->element('Layout/partidas-index-actions', ['partida'=>$p]) ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php if( $this->request->is('mobile') ): ?>
</div>
<?php endif;?>