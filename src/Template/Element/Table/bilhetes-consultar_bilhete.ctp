<?php if ($bilhete->is_active): ?>

	<?php if(!$bilhete->is_approved and $bilhete->expiration_datetime->isPast()): ?> 
		
		<!-- Bilhete expirado -->
		<div class="alert alert-danger">
			<?= __('Bilhete expirado e não pode mais ser validado. Nenhuma aposta registrada nele é válida!') ?>
		</div>

	<?php else: ?>

		<?php if(!$bilhete->is_approved and !$bilhete->expiration_datetime->isPast()): ?>
			<!-- aguardando validação -->
			<div class="alert alert-warning"><?= __('Bilhete aguardando validação!') ?></div>
		<?php endif; ?> 

		<?php if($bilhete->is_approved and is_null($bilhete->won)): ?>
			<!-- aguardando resultados -->
			<div class="alert alert-info"><?= __('Bilhete aguardando resultado dos jogos!') ?></div>
		<?php endif; ?> 
		
		<?php if ($bilhete->is_approved): ?>
			<!-- data de validação -->
			<div class="well">
				Bilhete validado em: <strong><?= h($bilhete->approval_datetime) ?></strong>
			</div>
		<?php endif; ?>
		
		<!-- bilhete -->
		<?php if( $this->request->is('mobile') ): ?>
		<div class="table-responsive">
		<?php endif;?>
			<table class="table table-bordered text-center">
				<thead>
					<th class="text-center">#</th>
					<th class="text-center"><?= __('Apostador') ?></th>
					<th class="text-center"><?= __('Revendedor') ?></th>
					<th class="text-center"><?= __('Valor') ?></th>
					<th class="text-center"><?= __('Prêmio') ?></th>
					<th class="text-center"><?= __('Lucro Revendedor') ?></th>
					<th class="text-center"><?= __('Total') ?></th>
					<th class="text-center"><?= __('Premiado?') ?></th>
				</thead>
				<tbody>
					<tr>
						<td><?= h($bilhete->id) ?></td>
						<td><?= h($bilhete->apostador) ?></td>
						<td><?= h($bilhete->revendedor) ?></td>
						<td>
						<?php 
							echo $this->Number->format($bilhete->valor_apostado, [
								'places' => 2,
								'before' => 'R$ '
							]); 
						?>
						</td>
						<td>
						<?php 
							echo $this->Number->format($bilhete->valor_premio_real, [
								'places' => 2,
								'before' => 'R$ '
							]); 
						?>
						</td>
						<td>
						<?php 
							echo $this->Number->format($bilhete->valor_revendedor_real, [
								'places' => 2,
								'before' => 'R$ '
							]); 
						?>
						</td>
						<td>
						<?php 
							echo $this->Number->format($bilhete->valor_total_real, [
								'places' => 2,
								'before' => 'R$ '
							]); 
						?>
						</td>
						<td>
						<?php
							if ($bilhete->won!==null and $bilhete->is_approved) {

								echo ($bilhete->won)?
									'<span class="badge bg-green">Sim</span>'
									: '<span class="badge bg-red">Não</span>';
							}
							else{

								echo '-';
							}
						?>
						</td>
					</tr>
				</tbody>
			</table>
		<?php if( $this->request->is('mobile') ): ?>
		</div>
		<?php endif;?>

		<?php if (isset($bilhete->apostas)): ?>
			
			<!-- apostas -->
			<h4><small>Palpites:</small></h4>
			<?php if( $this->request->is('mobile') ): ?>
			<div class="table-responsive">
			<?php endif;?>
				<table class="table table-bordered table-striped">
					<tbody>
					<?php
						foreach ($bilhete->apostas as $k=>$p) {

							$partida_date = (!empty($p->partida_time))? date('d/m/Y H:i:s', $p->partida_time) : '-';
							$palpite = $this->Bilhete->stringApostaPalpite($p->palpite_id);
							$palpite_odd = $this->Bilhete->formatPalpiteOdd($p->palpite_odd);
							$status = '-';
							if ($p->won!==null) {

								$status = ($p->won)?
									'<span class="badge bg-green">Acertou</span>'
									: '<span class="badge bg-red">Errou</span>';
							}

							echo "<tr>";
							echo "<td><strong>$p->league_name</strong><br>$p->home_name x $p->away_name<br><small>$partida_date</small></td>";
							echo "<td class='text-center'><strong>$palpite</strong><br>$palpite_odd</td>";
							echo "<td class='text-center'><strong>$status</strong><br>$p->score</td>";
							echo "</tr>";
						}
					?>	
					</tbody>
				</table>
			<?php if( $this->request->is('mobile') ): ?>
			</div>
			<?php endif;?>

		<?php endif; ?>

	<?php endif; ?>
	
<?php else: ?>

	<!-- Bilhete desativado -->
	<div class="alert alert-danger"><?= __('Este bilhete está desativado, nenhuma aposta registrada nele é válida!') ?></div>

<?php endif; ?>