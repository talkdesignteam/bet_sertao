<?php 
	$opt_dinheiro = [
		'places' => 2,
		'before' => 'R$ '
	];
?>

<?php if( $this->request->is('mobile') ): ?>
<div class="table-responsive">
<?php endif;?>
	<table id="tbList" class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th><?= __('Revendedor') ?></th>
				<th><?= __('Cliente') ?></th>
				<th><?= __('Valor') ?></th>
				<th><?= __('Prêmio') ?></th>
				<th><?= __('Lucro') ?></th>
				<th><?= __('Data ') ?></th>
				<th><?= __('Premiado?') ?></th>
				<th><?= __('Ações') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($bilhetes as $k=>$b): ?>
			<?php 
				// line style
				$status_tr = '';
				if($b->won===true) $status_tr = ' text-success text-bold';
				if($b->won===false) $status_tr = ' text-danger';
				if($b->is_active!==true) $status_tr = ' text-muted';
			?>
			<tr class="<?= $status_tr ?>">
				<td>
				<?php
					echo $this->Html->link($b->id,
						['controller'=>'Bilhetes', 'action'=>'view', '?'=>['id'=>$b->id]]
					); 
				?>
				</td>
				<td><?= h($b->user->username) ?></td>
				<td><?= h($b->apostador) ?></td>
				<td><?= $this->Number->format($b->valor_apostado, $opt_dinheiro) ?></td>
				<td title="Prêmio atual (inicial)">
				<?php 
					echo $this->Number->format($b->valor_premio_real, $opt_dinheiro);
					echo ' <small class="text-muted">('. $this->Number->format($b->valor_premio, $opt_dinheiro).')</small>';
				?>	
				</td>
				<td title="Lucro atual (inicial)">
				<?php 
					echo $this->Number->format($b->valor_revendedor_real, $opt_dinheiro);
					echo ' <small class="text-muted">('. $this->Number->format($b->valor_revendedor, $opt_dinheiro).')</small>';
				?>	
				</td>
				<td><?= h($b->created) ?></td>
				<td>
				<?php
					if ($b->won!==null and $b->is_approved) {

						echo ($b->won)?
							'<span class="badge bg-green">Sim</span>'
							: '<span class="badge bg-red">Não</span>';
					}
					else{

						echo '-';
					}
				?>
				</td>
				<td class="actions" style="white-space:nowrap">
					<?= $this->element('Layout/bilhetes-index-actions', ['bilhete'=>$b]) ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php if( $this->request->is('mobile') ): ?>
</div>
<?php endif;?>