<?php if( $this->request->is('mobile') ): ?>
<div class="table-responsive">
<?php endif;?>
	<table id="tbList" class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th><?= __('Nome completo') ?></th>
				<th><?= __('Login') ?></th>
				<th><?= __('E-mail') ?></th>
				<th><?= __('Saldo') ?></th>
				<th><?= __('Criado em') ?></th>
				<th><?= __('Status') ?></th>
				<th><?= __('Ações') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($logins as $k=>$l): ?>
			<tr>
				<td><?= h($l->id) ?></td>
				<td><?= h($l->loginprofile->full_name) ?></td>
				<td><?= h($l->username) ?></td>
				<td><?= h($l->email) ?></td>
				<td>
				<?php 
					echo $this->Number->format($l->saldo_apostas, [
						'places' => 2,
						'before' => 'R$ '
					]); 
				?>
				</td>
				<td><?= h($l->created) ?></td>
				<td>
				<?php
					echo ($l->active)?
						'<span class="badge bg-green">Ativado</span>'
						: '<span class="badge bg-red">Desativado</span>';
				?>
				</td>
				<td class="actions" style="white-space:nowrap">
				<?php
					echo $this->element('Layout/logins-lista_gerente-actions',
						[
							'id'=>$l->id,
							'confirm'=>"Confirma a exclusão deste revendedor?",
							'plugin'=>false,
							'delete'=>false
						]
					);
				?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php if( $this->request->is('mobile') ): ?>
</div>
<?php endif;?>