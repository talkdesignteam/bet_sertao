<?php
	$opt_dinheiro = [
		'places' => 2,
		'before' => 'R$ '
	];

	$valor_premio_real = $this->Number->format($bilhete->valor_premio_real, $opt_dinheiro); 
	$valor_revendedor_real = $this->Number->format($bilhete->valor_revendedor_real, $opt_dinheiro); 
?>
<div class="row">
	<div class="col-md-4">
		<div class="well">
			<h4><?= __('Apostador') ?></h4>
			<?php
				echo "<strong>Prêmio: &nbsp;&nbsp;&nbsp;</strong>$valor_premio_real <br>";
				if ($bilhete->pgto_premio===true) {

					echo "<strong>Pago em: &nbsp;&nbsp;&nbsp;</strong>";
					echo date('d/m/Y \à\s H\hi', strtotime($bilhete->pgto_premio_date));
				}
				else{

					echo "<strong>Pago em: &nbsp;&nbsp;&nbsp;</strong>";
					echo '<span class="badge bg-red">não foi pago</span><br><br>';
					$confirm = __('Confirmar pagamento do prêmio ao apostador?');
					echo $this->Form->postLink(
						__('confirmar pagamento'),
						['controller'=>'Bilhetes', 'action'=>'userConfirmarPgtoPremio', $bilhete->id, 'plugin'=>false],
						['confirm'=>$confirm, 'escape'=>false, 'class'=>'btn btn-warning loading']
					);
				}
			?>
		</div>
	</div>
	<div class="col-md-4">
		<div class="well">
			<h4><?= __('Revendedor') ?></h4>
			<?php
				echo "<strong>Lucro: &nbsp;&nbsp;&nbsp;</strong>$valor_revendedor_real <br>";
				if ($bilhete->pgto_revendedor===true) {

					echo "<strong>Pago em: &nbsp;&nbsp;&nbsp;</strong>";
					echo date('d/m/Y \à\s H\hi', strtotime($bilhete->pgto_revendedor_date));
				}
				else{

					echo "<strong>Pago em: &nbsp;&nbsp;&nbsp;</strong>";
					echo '<span class="badge bg-red">não foi pago</span><br><br>';
					$confirm = __('Confirmar recebimento do lucro deste bilhete?');
					echo $this->Form->postLink(
						__('confirmar recebimento'),
						['controller'=>'Bilhetes', 'action'=>'userConfirmarReciboRevendedor', $bilhete->id, 'plugin'=>false],
						['confirm'=>$confirm, 'escape'=>false, 'class'=>'btn btn-success loading']
					);
				}
			?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<br>
		<!-- bilhete -->
		<h4><?= __('Bilhete').'&nbsp;#'.$bilhete->id ?></h4>
		<?php if( $this->request->is('mobile') ): ?>
		<div class="table-responsive">
		<?php endif;?>
			<table class="table table-bordered text-center">
				<thead>
					<th><?= __('Apostador') ?></th>
					<th><?= __('Revendedor') ?></th>
					<th><?= __('Valor apostado') ?></th>
					<th><?= __('Prêmio') ?></th>
					<th><?= __('Lucro Revendedor') ?></th>
					<th><?= __('Total') ?></th>
					<th></th>
				</thead>
				<tbody>
					<tr>
						<td><?= h($bilhete->apostador) ?></td>
						<td><?= h($bilhete->revendedor) ?></td>
						<td><?= $this->Number->format($bilhete->valor_apostado, $opt_dinheiro) ?></td>
						<td><?= $valor_premio_real ?></td>
						<td><?= $valor_revendedor_real ?></td>
						<td><?= $this->Number->format($bilhete->valor_total_real, $opt_dinheiro) ?></td>
						<td class="actions" style="white-space:nowrap">
							<?php
								// button - ver mais
								echo $this->Html->link('<i class="fa fa-search"></i>',
									['controller'=>'Bilhetes', 'action'=>'userConsultarBilhete', '?'=>['id'=>$bilhete->id]],
									['escape'=>false, 'class'=>'btn btn-default', 'title'=>__('Ver mais detalhes do bilhete')]
								);
							?>						
						</td>
					</tr>
				</tbody>
			</table>
		<?php if( $this->request->is('mobile') ): ?>
		</div>
		<?php endif;?>
	</div>
</div>
