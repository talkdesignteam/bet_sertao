<?php 
	$opt_dinheiro = [
		'places' => 2,
		'before' => 'R$ '
	];
?>
<?php if( $this->request->is('mobile') ): ?>
<div class="table-responsive">
<?php endif;?>
	<table id="tbList" class="table table-bordered">
		<thead>
			<tr>
				<th class="text-center"><?= __('#') ?></th>
				<th class="text-center"><?= __('Cliente') ?></th>
				<th class="text-center"><?= __('Valor') ?></th>
				<th class="text-center"><?= __('Cotação') ?></th>
				<th class="text-center"><?= __('Prêmio') ?></th>
				<th class="text-center"><?= __('Lucro') ?></th>
				<th class="text-center"><?= __('Total') ?></th>
				<th class="text-center"><?= __('Data') ?></th>
				<th class="text-center"><?= __('Situação') ?></th>
			</tr>
		</thead>
		<tbody class="text-center">
			<?php foreach($bilhetes as $k=>$b): ?>
			<?php
				// Situação do bilhete
				$status_label = '';
				$status_label_class = 'default';
				if (!$b->is_active) {

					$status_label_class = 'danger';
					$status_label = '<span class="label label-'.$status_label_class.'">desativado</span>';
				}
				elseif(!$b->is_approved){

					$status_label_class = 'warning';
					$status_label =  '<span class="label label-'.$status_label_class.'">aguardando aprovação</span>';
				}
				else{

					// line style
					$status_tr = '';
					if($b->won===true) $status_tr = ' text-success text-bold';
					if($b->won===false) $status_tr = ' text-danger';
					if($b->is_active!==true) $status_tr = ' text-muted';

					$status_label_class = ($b->won)? 'success' : 'danger';
					if ($b->won === null) $status_label_class = 'default';
					$status_label = "<span class='label label-$status_label_class'>{$this->Bilhete->bilheteStatus($b->won)}</span>";
				}
			?>
			<tr class="text-<?= $status_label_class.$status_tr; ?>">
				<td>
					<?php
						echo $this->Html->link($b->id,
							['controller'=>'Bilhetes', 'action'=>'view', '?'=>['id'=>$b->id]]
						); 
					?>
				</td>
				<td><?= h($b->apostador) ?></td>
				<td><?= $this->Number->format($b->valor_apostado, $opt_dinheiro) ?></td>
				<td><?= $this->Number->format(round($b->soma_odds, 2), ['places' => 2, 'before' => false]) ?></td>
				<td title="Prêmio atual"><?= $this->Number->format($b->valor_premio_real, $opt_dinheiro) ?></td>
				<td title="Lucro atual"><?= $this->Number->format($b->valor_revendedor_real, $opt_dinheiro) ?></td>
				<td title="Total atual">
				<?php
					$valor_total_real = $b->valor_premio_real+$b->valor_revendedor_real;
					echo $this->Number->format($valor_total_real, $opt_dinheiro);
				?>
				</td>
				<td><?= h($b->created) ?></td>
				<td><?= $status_label ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php if( $this->request->is('mobile') ): ?>
</div>
<?php endif;?>