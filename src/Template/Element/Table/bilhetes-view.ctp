<?php 
	$opt_dinheiro = [
		'places' => 2,
		'before' => 'R$ '
	];
?>

<?php if (!$bilhete->is_active): ?>
	<div class="alert alert-danger"><?= __('Este bilhete está desativado, nenhuma aposta registrada nele é válida!') ?></div>
<?php endif; ?> 

<?php if($bilhete->is_active and !$bilhete->is_approved and $bilhete->expiration_datetime->isPast()): ?>
	<div class="alert alert-warning"><?= __('Bilhete expirado e não pode mais ser validado. Nenhuma aposta registrada nele é válida!') ?></div>
<?php endif; ?>

<?php if($bilhete->is_active and !$bilhete->is_approved and !$bilhete->expiration_datetime->isPast()): ?>
	<div class="alert alert-warning"><?= __('Bilhete aguardando validação!') ?></div>
<?php endif; ?> 

<?php if($bilhete->is_active and $bilhete->is_approved and is_null($bilhete->won)): ?>
	<div class="alert alert-info"><?= __('Bilhete aguardando resultado dos jogos!') ?></div>
<?php endif; ?> 

<?php if ($bilhete->won and ($bilhete->pgto_premio===true or $bilhete->pgto_premio===true)): ?>
	<div class="well">
		<?php if($bilhete->pgto_premio===true): ?>
			<!-- prêmio pago -->
			Apostador recebeu o prêmio em: <strong><?= h($bilhete->pgto_premio_date) ?></strong>
		<?php endif; ?>
		<?php if($bilhete->pgto_premio===true): ?>
			<!-- lucro pago -->
			<br>
			Revendedor recebeu o prêmio em: <strong><?= h($bilhete->pgto_revendedor_date) ?></strong>
		<?php endif; ?>
	</div>
<?php endif; ?>

<?php if ($bilhete->is_approved): ?>
	<!-- Approval data -->
	<div class="well">
		Bilhete validado em: <strong><?= h($bilhete->approval_datetime) ?></strong>
	</div>
<?php endif; ?>

<!-- bilhete -->
<?php if( $this->request->is('mobile') ): ?>
<div class="table-responsive">
<?php endif;?>
	<table class="table table-bordered text-center">
		<thead>
			<th>#</th>
			<th><?= __('Apostador') ?></th>
			<th><?= __('Revendedor') ?></th>
			<th><?= __('Valor apostado') ?></th>
			<th><?= __('Prêmio') ?></th>
			<th><?= __('Lucro revendedor') ?></th>
			<th><?= __('Total') ?></th>
			<th><?= __('Premiado?') ?></th>
		</thead>
		<tbody>
			<tr>
				<td><?= h($bilhete->id) ?></td>
				<td><?= h($bilhete->apostador) ?></td>
				<td><?= h($bilhete->revendedor) ?></td>
				<td><?= $this->Number->format($bilhete->valor_apostado, $opt_dinheiro) ?></td>
				<td title="Prêmio atual (inicial)">
				<?php 
					echo $this->Number->format($bilhete->valor_premio_real, $opt_dinheiro);
					echo ' <small class="text-muted">('. $this->Number->format($bilhete->valor_premio, $opt_dinheiro).')</small>';
				?>	
				</td>
				<td title="Lucro atual (inicial)">
				<?php 
					echo $this->Number->format($bilhete->valor_revendedor_real, $opt_dinheiro);
					echo ' <small class="text-muted">('. $this->Number->format($bilhete->valor_revendedor, $opt_dinheiro).')</small>';
				?>	
				</td>
				<td title="Total atual (inicial)">
				<?php 
					echo $this->Number->format($bilhete->valor_total_real, $opt_dinheiro);
					echo ' <small class="text-muted">('. $this->Number->format($bilhete->valor_total, $opt_dinheiro).')</small>';
				?>	
				</td>
				<td>
				<?php
					if ($bilhete->won!==null and $bilhete->is_approved) {

						echo ($bilhete->won)?
							'<span class="badge bg-green">Sim</span>'
							: '<span class="badge bg-red">Não</span>';
					}
					else{

						echo '-';
					}
				?>
				</td>
			</tr>
		</tbody>
	</table>
<?php if( $this->request->is('mobile') ): ?>
</div>
<?php endif;?>

<?php if (isset($bilhete->apostas)): ?>
	
	<!-- apostas -->
	<h4><small>Palpites:</small></h4>
	<?php if( $this->request->is('mobile') ): ?>
	<div class="table-responsive">
	<?php endif;?>
		<table class="table table-bordered table-striped">
			<tbody>
			<?php
				foreach ($bilhete->apostas as $k=>$p) {

					$partida_date = (!empty($p->partida_time))? date('d/m/Y H:i:s', $p->partida_time) : '-';
					$palpite = $this->Bilhete->palpiteAposta($p->palpite_id);
					$palpite_odd = $this->Bilhete->formatPalpiteOdd($p->palpite_odd);
					$status = '-';
					if ($p->won!==null) {

						$status = ($p->won)?
							'<span class="badge bg-green">Acertou</span>'
							: '<span class="badge bg-red">Errou</span>';
					}

					echo "<tr>";
					echo "<td><strong>$p->league_name</strong><br>$p->home_name x $p->away_name<br><small>$partida_date</small></td>";
					echo "<td class='text-center'><strong>$palpite</strong><br>$palpite_odd</td>";
					echo "<td class='text-center'><strong>$status</strong><br>$p->score</td>";
					echo "</tr>";
				}
			?>	
			</tbody>
		</table>
	<?php if( $this->request->is('mobile') ): ?>
	</div>
	<?php endif;?>

<?php endif; ?>