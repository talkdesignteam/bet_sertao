<?php 
	$meta_title = "Bet Sertão 365";
	$meta_name = "BetSertao";
	$meta_description = "";
	$meta_keywords = "";
	$meta_version = "1.0";
	$meta_url = "http://betsertao365.com";
?>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="alternate" hreflang="pt-br" href="http://betsertao365.com/" />
	<meta name="format-detection" content="telephone=no">
	
	<!-- META TAGS -->
	<meta name='robots' content='noindex, nofollow'>
	<meta name='googlebot' content='noindex, nofollow'>
	<meta name="language" content="pt-br">
	<meta http-equiv="Content-Language" content="pt-BR">
		
	<!-- CHROME -->
	<meta name="theme-color" content="#006608">

	<!-- APPLE -->
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
	<meta name="apple-mobile-web-app-title" content="<?= $meta_name; ?>"/>