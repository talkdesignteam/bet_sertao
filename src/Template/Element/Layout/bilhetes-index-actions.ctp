<?php

	// button - ver mais
	echo $this->Html->link('<i class="fa fa-search"></i>',
		['controller'=>'Bilhetes', 'action'=>'view', '?'=>['id'=>$bilhete->id]],
		['escape'=>false, 'class'=>'btn btn-default btn-xs', 'title'=>__('Ver mais detalhes')]
	);
	
	// validar bilhete
	if ($bilhete->is_active and !$bilhete->is_approved and !$bilhete->expiration_datetime->isPast()) {

		echo '&nbsp;';
		$confirmMessage = __("Confirma a validação do bilhete $bilhete->id?");
		echo $this->Form->postLink(
			'<i class="fa fa-check"></i>&nbsp;'.__('validar'),
			['action'=>'validarBilhete', $bilhete->id, false],
			['confirm'=>__("$confirmMessage"), 'escape'=>false, 'class'=>'btn btn-primary btn-xs']
		);
	}

	// button - pagar bilhete
	if($bilhete->won===true and (!$bilhete->pgto_premio or !$bilhete->pgto_revendedor)) {

		echo '&nbsp;';
		echo $this->Html->link('<i class="fa fa-money"></i>&nbsp'.__('pagar bilhete'),
			['controller'=>'Bilhetes', 'action'=>'userPagarBilhete', '?'=>['id'=>$bilhete->id]],
			['escape'=>false, 'class'=>'btn btn-success btn-xs', 'title'=>'Pagar bilhete']
		);
	}

	// button - imprimir
	if($bilhete->is_active and $bilhete->is_approved) {
		
		echo '&nbsp;';
		echo $this->Html->link('<i class="fa fa-print"></i>&nbsp'.__('imprimir'),
			['controller'=>'Bilhetes', 'action'=>'imprimir', $bilhete->id],
			['escape'=>false, 'class'=>'btn btn-default btn-xs', 'title'=>'Imprimir', 'target'=>'_blank']
		);
	}
?>