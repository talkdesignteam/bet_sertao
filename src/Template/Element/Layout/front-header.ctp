		<div class="container">
			<div class="logo">
				<?= $this->Html->link($this->Html->image('layout/logo-default.png'), '/', ['escape'=>false]) ?>
			</div>
			<div class="login">

				<?php if ($this->request->session()->read('Auth.User')): ?>
					
					<br><br><br><br>
					<?php
						echo $this->Html->link('Painel administrativo',
							['controller'=>'Dashboards', 'action'=>'index', 'admin'=>false, 'plugin'=>false],
							['class'=>'botao']
						);
					?>

				<?php else: ?>

					<?= $this->element('Layout/front-form_login') ?>

				<?php endif; ?>

			</div>
		</div>
		<nav class="navbar navbar-site">
			<div class="container-fluid">
				<!-- toggle para mobile -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#NavbarCollapse" aria-expanded="false">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>    
				<div class="collapse navbar-collapse" id="NavbarCollapse">
					<div class="container">
						<ul class="menu nav navbar-nav">
							<li><?= $this->Html->link(__('Início'), '/') ?></li>   
							<li>
							<?php 
								echo $this->Html->link(__('Consultar bilhete'), 
									['controller'=>'Bilhetes', 'action'=>'consultarBilhete', 'plugin'=>false, 'admin'=>false]
								); 
							?>
							</li>
							<li>
							<?php 
								echo $this->Html->link(__('Regulamento'), 
									['controller'=>'Pages', 'action'=>'display', 'regulamento', 'plugin'=>false, 'admin'=>false]
								); 
							?>
							</li>
							<li>
							<?php 
								echo $this->Html->link(__('Fale conosco'), 
									['controller'=>'Pages', 'action'=>'display', 'contato', 'plugin'=>false, 'admin'=>false]
								); 
							?>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		