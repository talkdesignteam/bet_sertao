<?php
	echo $this->Form->create(null, [
			'url'=>['controller'=>'Users', 'action'=>'login', 'plugin'=>'AccessManager', 'admin'=>false],
			'class'=>'formlogin'
		]
	);
?>

	<div class="usuario has-feedback">
	<?php 
		echo $this->Form->input('username', 
			['class'=>'', 'placeholder'=>__('Login'), 'required'=>'required', 'type'=>'text', 'label'=>false]
		);
	?>
	</div>
	<div class="senha has-feedback">
	<?php 
		echo $this->Form->password('password', 
			['class'=>'', 'placeholder'=>__('Senha'), 'required'=>'required']
		);
	?>
	</div>
	<div class="acao">
	<?php 
		echo $this->Form->button(__('Entrar no sistema'), 
			['class'=>'botao -login', 
			'id'=>'loadButton', 'data-loading-text'=>__('Aguarde...'), 'submitContainer'=>null]
		);
	?>
	</div>

<?= $this->Form->end() ?>