<?php
	if (!isset($view)) $view = true;
	if (!isset($edit)) $edit = true;
	if (!isset($delete)) $delete = true;

	if (!isset($plugin)) $plugin = false;

	$iconView = '<i class="fa fa-search"></i>';
	$iconEdit = '<i class="fa fa-pencil"></i>';
	$iconDel = '<i class="fa fa-trash"></i>';

	if ($view) {

		echo $this->Html->link(
			$iconView,
			['action'=>'view', $id, 'plugin'=>$plugin],
			['escape'=>false, 'class'=>'btn btn-default btn-xs', 'title'=>__('Ver mais detalhes')]
		);
		echo '&nbsp;';
	}

	if ($edit) {

		echo $this->Html->link(
			$iconEdit,
			['action'=>'edit', $id, 'plugin'=>$plugin],
			['escape'=>false, 'class'=>'btn btn-default btn-xs', 'title'=>__('Editar')]
		);
		echo '&nbsp;';
	}

	if ($delete) {
		
		echo $this->Form->postLink(
			$iconDel,
			['action'=>'delete', $id, 'plugin'=>$plugin],
			['confirm'=> $confirm, 'escape'=>false, 'class'=>'btn btn-default btn-xs', 'title'=>__('Excluir')]
		);
	}
?>
