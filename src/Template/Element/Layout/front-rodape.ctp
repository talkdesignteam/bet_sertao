<div class="container">
	<div class="content-footer">
		<div class="item">
			<?= $this->Html->link($this->Html->image('layout/logo-default.png'), '/', ['class'=>'img-responsive', 'escape'=>false]) ?>
		</div>
		<div class="item">
			<h3>Navegue no site</h3>
			<ul class="menu-rodape">
				<li>
				<?php 
					echo $this->Html->link(__('Consultar bilhete'), 
						['controller'=>'Bilhetes', 'action'=>'consultarBilhete', 'plugin'=>false, 'admin'=>false]
					); 
				?>
				</li>
				<li>
				<?php 
					echo $this->Html->link(__('Regulamento'), 
						['controller'=>'Pages', 'action'=>'display', 'regulamento', 'plugin'=>false, 'admin'=>false]
					); 
				?>
				</li>
				<li>
				<?php 
					echo $this->Html->link(__('Fale conosco'), 
						['controller'=>'Pages', 'action'=>'display', 'contato', 'plugin'=>false, 'admin'=>false]
					); 
				?>
				</li>
			</ul>
		</div>
		<div class="item">
			<h3>Fale conosco</h3>
			<div class="telefone">
				(99) 91234 - 5678
			</div>
			<div class="telefone -whatsapp">
				(99) 91234 - 5678
			</div>
		</div>
		<div class="item">
			<h3>Siga-nos</h3>
			<div class="facebook">
				<a href="" target="_blank"><i class="fa fa-facebook"></i></a>
			</div>
			<div class="instagram">
				<a href="" target="_blank"><i class="fa fa-instagram"></i></a>
			</div>
		</div>
		<div class="item">
			<h3>Aceitamos os cartões</h3>
			<div class="cartoes">
				<i class="fa fa-cc-visa" aria-hidden="true"></i>
				<i class="fa fa-cc-mastercard" aria-hidden="true"></i>
			</div>
		</div>
	</div>
</div>
<!-- <a href="javascript: void(0);" class="botao -scroll"><i class="fa fa-angle-up" aria-hidden="true"></i></a> -->
