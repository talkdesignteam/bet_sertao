<?php

	// button - ver mais
	// echo $this->Html->link('<i class="fa fa-search"></i>',
	// 	['controller'=>'Bilhetes', 'action'=>'view', '?'=>['id'=>$bilhete->id]],
	// 	['escape'=>false, 'class'=>'btn btn-default btn-xs', 'title'=>__('Ver mais detalhes')]
	// );
	
	// button - editar odds
	echo '&nbsp;';
	echo $this->Html->link('<i class="fa fa-line-chart"></i>&nbsp'.__('odds'),
		['controller'=>'Partidas', 'action'=>'editarOdds', $partida->id],
		['escape'=>false, 'class'=>'btn btn-default btn-xs', 'title'=>'Editar ODDS']
	);
?>