<?php
	if (!isset($edit)) $edit = true;
	if (!isset($editPass)) $editPass = true;
	if (!isset($delete)) $delete = true;

	if (!isset($plugin)) $plugin = false;

	$iconView = '<i class="fa fa-search"></i>';
	$iconEdit = '<i class="fa fa-pencil"></i>';
	$iconEditPass = '<i class="fa fa-key"></i>';
	$iconDel = '<i class="fa fa-trash"></i>';

	if ($edit) {

		echo $this->Html->link(
			$iconEdit,
			['action'=>'editGerente', $id, 'plugin'=>$plugin],
			['escape'=>false, 'class'=>'btn btn-default btn-xs', 'title'=>__('Editar')]
		);
		echo '&nbsp;';
	}

	if ($editPass) {

		echo $this->Html->link(
			$iconEditPass,
			['action'=>'editPassword', $id, 'plugin'=>$plugin],
			['escape'=>false, 'class'=>'btn btn-default btn-xs', 'title'=>__('Alterar senha')]
		);
		echo '&nbsp;';
	}

	if ($delete) {
		
		// echo $this->Form->postLink(
		// 	$iconDel,
		// 	['action'=>'delete', $id, 'plugin'=>$plugin],
		// 	['confirm'=> $confirm, 'escape'=>false, 'class'=>'btn btn-default btn-xs', 'title'=>__('Excluir')]
		// );
	}
?>
