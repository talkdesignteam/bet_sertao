<?= $this->Form->create('Filtro', ['autocomplete'=>'off']) ?>


<div class="col-xs-12 col-md-3">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-user"></i></div>
			<?php
				echo $this->Form->select('approval_user',
					$users,
					['class'=>'form-control input-lg', 
						'multiple'=>false, 'empty'=>'-- revendedor', 'escape'=>false, 'required'=>true]
				);
			?>
		</div>
	</div>
</div>
<div class="col-xs-12 col-md-3">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
			<?php
				echo $this->Form->input('data_inicio', [
						'label'=>false, 'class'=>'form-control datepicker pull-right input-lg', 'placeholder'=>'Data início', 'required'=>true
					]
				);
			?>
		</div>
	</div>
</div>
<div class="col-xs-12 col-md-3">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
			<?php
				echo $this->Form->input('data_fim', [
						'label'=>false, 'class'=>'form-control datepicker pull-right input-lg', 'placeholder'=>'Data final', 'required'=>true
					]
				);
			?>
		</div>
	</div>             
</div>
<div class="col-md-2">
	<div class="form-group">                
		<?php
			if ($this->request->is('mobile')) {

				echo $this->Form->button(__('Visualizar'),
					['class'=>'btn btn-success btn-lg btn-block', 'value'=>'filterSubmit',
					'id'=>'loadButton', 'data-loading-text'=>'Aguarde...']
				);
			}
			else {
				
				echo $this->Form->button(__('Visualizar'),
					['class'=>'btn btn-success btn-lg', 'value'=>'filterSubmit',
					'id'=>'loadButton', 'data-loading-text'=>'Aguarde...']
				);
			}
		?>
	</div>
</div>

<?php $this->Form->end(); ?>