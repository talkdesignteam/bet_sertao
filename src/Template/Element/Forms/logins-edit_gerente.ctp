<?= $this->Form->create($login, array('role'=>'form', 'class'=>'form-horizontal')) ?>
	<div class="box-body">
		<br>
		<!-- Nome Completo -->
		<div class="form-group">
			<label for="first-name" class="col-sm-2 control-label">Nome</label>
			<div class="col-sm-2">
				<?php 
					echo $this->Form->input('loginprofile.first_name', 
						['placeholder'=>'nome', 'label'=>false, 'class'=>'form-control']
					);
				?>
			</div>
			<div class="col-sm-4">
				<?php 
					echo $this->Form->input('loginprofile.last_name', 
						['placeholder'=>'sobrenome (opcional)', 'label'=>false, 'class'=>'form-control']
					);
				?>
			</div>
		</div>
		<!-- CPF -->
		<div class="form-group">
			<label for="cpf" class="col-sm-2 control-label">CPF</label>
			<div class="col-sm-3">
				<?php 
					echo $this->Form->input('loginprofile.cpf', [
							'label'=>false, 'class'=>'form-control', 'maxlength'=>'14',
							'data-inputmask'=>'\'mask\': \'999.999.999-99\'', 'data-mask'
						]
					);
				?>
			</div>
		</div>
		<!-- Telefone -->
		<div class="form-group">
			<label for="phone01" class="col-sm-2 control-label">Telefone</label>
			<div class="col-sm-3">
				<div class="input-group">
					<div class="input-group-addon">
						<i class="fa fa-phone"></i>
					</div>
					<?php 
						echo $this->Form->input('loginprofile.phone01', [
								'label'=>false, 'class'=>'form-control', 'maxlength'=>'16',
								'data-inputmask'=>'\'mask\': \'(99) [9] 9999-9999\'', 'data-mask'
							]
						);
					?>
				</div>
			</div>
		</div>
		<!-- Login -->
		<div class="form-group">
			<label for="username" class="col-sm-2 control-label">Login</label>
			<div class="col-sm-4">
				<?php 
					echo $this->Form->input('username', 
						['type'=>'text', 'placeholder'=>'Login', 'label'=>false, 'class'=>'form-control', 'required'=>true]
					);
				?>
			</div>
		</div>
		<!-- E-mail -->
		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">E-mail</label>
			<div class="col-sm-4">
				<?php 
					echo $this->Form->input('email', 
						['type'=>'email', 'placeholder'=>'(opcional)', 'label'=>false, 'class'=>'form-control', 'required'=>false]
					);
				?>
			</div>
		</div>
		<!-- Saldo -->
		<div class="form-group">
			<label for="username" class="col-sm-2 control-label">Saldo</label>
			<div class="col-sm-2">
				<div class="input-group">
					<span class="input-group-addon">R$</span>
					<?php 
						echo $this->Form->control('saldo_apostas', 
							['label'=>false, 'class'=>'form-control', 'required'=>true]
						);
					?>
				</div>
			</div>
		</div>


		<!-- Status -->
		<div class="form-group">
			<label for="status" class="col-sm-2 control-label">&nbsp;</label>
			<div class="col-sm-10">
				<?= $this->Form->control('active', ['label'=>'Ativado', 'default'=>true]) ?>
			</div>
		</div>
		<br>
		<div class="form-group">
			<label  class="col-sm-2 control-label">&nbsp;</label>
			<div class="col-sm-10">
				<?php 
					echo $this->Form->button('Atualizar', [
						'class'=>'btn btn-success btn-lg col-md-3',
						'id'=>'loadButton', 'data-loading-text'=>'Aguarde...', 'submitContainer'=>null
					]);
				?>
			</div>
		</div>
	</div>

<?= $this->Form->end() ?>