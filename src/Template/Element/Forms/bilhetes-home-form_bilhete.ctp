<?php 
	$BilheteHelper = $this->loadHelper('Bet.Bilhete');
?>
<?= $this->Form->create($bilhete) ?>
	<div class="item-aposta loading">
		
		<!-- apostas selecionadas -->
		<div class="panel panel-default">
			<div class="panel-heading">
				<?= __('Palpites') ?>				
			</div>
			
			<?php if (!empty($bilhete->apostas)): ?>
				<?php foreach($bilhete->apostas as $k=>$a): ?>
					<div class="painel-aposta panel-body alert alert-warning" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<ul>
							<li class="aposta">
								<div class="item"><?= $a->home_name.' x '.$a->away_name ?></div> 
								<div class="item">
									Palpite: <?= $BilheteHelper->palpiteAposta($a->palpite_id) ?>
									&nbsp;&raquo&nbsp;
									Cotação: <?= $this->Number->precision($a->palpite_odd, 2) ?>
								</div>
								<div class="item">
									<i class="fa fa-calendar-o"></i> <?= date('d/m/Y', $a->partida_time) ?>
									 | <i class="fa fa-clock-o"></i> <?= date('H\hi', $a->partida_time) ?>
								</div>
							</li>
						</ul>
					</div>
				<?php endforeach; ?>
			<?php else: ?>
				<p class="alert text-muted"><?= __('nenhuma aposta selecionada') ?></p>
			<?php endif; ?>
		</div>
		
		<!-- form bilhete -->
		<div class="panel panel-default panelaposta form-horizontal">
			<div class="panel-heading"><?= __('Bilhete')?></div>
			<div class="panel-body">
				
				<!-- palpite -->
				<div class="form-group">
					<label for="" class="col-sm-4 control-label">Palpites:</label>
					<div class="col-xs-12 col-sm-8">
						<span class="form-control" disabled><?php if(isset($bilhete->apostas)) echo count($bilhete->apostas); ?></span>
					</div>
				</div>

				<!-- cotação -->
				<div class="form-group">
					<label for="" class="col-sm-4 control-label">Cotação:</label>
					<div class="col-xs-12 col-sm-8">
						<span class="form-control" disabled><?= $bilhete->soma_odds; ?></span>
					</div>
				</div>

				<!-- valor apostado -->
				<div class="form-group">
					<label for="" class="col-sm-4 control-label">Valor:</label>
					<div class="col-xs-12 col-sm-8">
						<div class="input-group">
							<span class="input-group-addon">R$</span>	
							<?php 
								echo $this->Form->input('valor_apostado', 
									['type'=>'number', 'label'=>false, 'class'=>'atualizaPainelAposta form-control', 'placeholder'=>'aposta']
								);
							?>
							<div class="input-group-addon">,00</div>
						</div>
					</div>
				</div>

				<!-- valor pago -->
				<div class="form-group">
					<label for="" class="col-sm-4 control-label">Prêmio:</label>
					<div class="col-xs-12 col-sm-8">
						<div class="input-group">
							<span class="input-group-addon">R$</span>	
							<?php
								echo $this->Form->input('valor_premio', 
									['type'=>'number', 'label'=>false, 'class'=>'form-control', 'placeholder'=>false, 'disabled']
								);
							?>
						</div>
					</div>
				</div>

				<!-- cliente -->
				<div class="form-group">
					<label for="" class="col-sm-4 control-label">Cliente:</label>
					<div class="col-xs-12 col-sm-8">
						<?php 
							echo $this->Form->input('apostador', 
								['type'=>'text', 'placeholder'=>'seu nome', 'label'=>false, 'class'=>'form-control']
							);
						?>
					</div>
				</div>

				<!-- cambista -->
				<div class="form-group">
					<label for="" class="col-sm-4 control-label">Revendedor:</label>
					<div class="col-xs-12 col-sm-8">
						<?php 
							echo $this->Form->input('revendedor', 
								['type'=>'text', 'placeholder'=>'(opcional)', 'label'=>false, 'class'=>'form-control']
							);
						?>
					</div>
				</div>

				<!-- telefone -->
				<div class="form-group">
					<label for="" class="col-sm-4 control-label">Telefone:</label>
					<div class="col-xs-12 col-sm-8">
						<?php 
							echo $this->Form->input('apostador_fone1', 
								['type'=>'tel', 'placeholder'=>'(opcional)', 'label'=>false, 'class'=>'form-control']
							);
						?>
					</div>
				</div>

			</div>
		</div>
	   
		<div class="form-group -acao text-center">
			<!-- <a href="#" class="btn btn-link">Limpar<a> -->
			<?php 
				echo $this->Form->button(__('Pré-bilhete'), [
					'class'=>'btn btn-success btn-lg btn-flat',
					'data-toggle='=>'modal', 'data-target'=>'#GerarBilhete'
				]);
			?>
		</div>

	</div>                                
<?= $this->Form->end() ?>