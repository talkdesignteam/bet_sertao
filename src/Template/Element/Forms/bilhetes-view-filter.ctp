<?= $this->Form->create(null, ['type' => 'get', 'class'=>'form-inline', 'autocomplete'=>'off', 'valueSources'=>'query']) ?>

<div class="col-xs-12 col-md-3">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-ticket"></i></div>
			<?php
				echo $this->Form->input('id', [
						'label'=>false, 'class'=>'form-control input-lg', 'placeholder'=>'número do bilhete', 'value'=>$id, 'required'=>true
					]
				);
			?>
		</div>
	</div>             
</div>
<div class="col-md-2">
	<div class="form-group">                
		<?php
			if ($this->request->is('mobile')) {

				echo $this->Form->button(__('Consultar'),
					['class'=>'btn btn-success btn-lg btn-block', 'value'=>'filterSubmit',
					'id'=>'loadButton', 'data-loading-text'=>'Aguarde...']
				);
			}
			else {
				
				echo $this->Form->button(__('Consultar'),
					['class'=>'btn btn-success btn-lg', 'value'=>'filterSubmit',
					'id'=>'loadButton', 'data-loading-text'=>'Aguarde...']
				);
			}
		?>
	</div>
</div>
<?= $this->Form->end(); ?>