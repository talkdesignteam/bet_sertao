<?= $this->Form->create(null, ['autocomplete'=>'off', 'valueSources'=>'query']) ?>

<?= $this->Form->hidden('submit', ['value'=>true]) ?>

<!-- League name -->
<div class="col-xs-12 col-md-4">
	<div class="form-group">
		<?php
			echo $this->Form->input('league_name', [
					'label'=>false, 'class'=>'form-control input-lg', 'placeholder'=>'Campeonato'
				]
			);
		?>
	</div>
</div>

<div class="col-md-3">
	<div class="form-group">                
		<?php
			if ($this->request->is('mobile')) {

				echo $this->Form->button('<i class="fa fa-search"></i> &nbsp;&nbsp;'.__('Pesquisar'),
					['class'=>'btn btn-success btn-lg btn-block', 'value'=>'filterSubmit',
					'id'=>'loadButton', 'data-loading-text'=>'Aguarde...']
				);
			}
			else {
				
				echo $this->Form->button('<i class="fa fa-search"></i> &nbsp;&nbsp;'.__('Pesquisar'),
					['class'=>'btn btn-success btn-lg', 'value'=>'filterSubmit',
					'id'=>'loadButton', 'data-loading-text'=>'Aguarde...']
				);
			}

			if (!empty($_isSearch)) {

				echo $this->Html->link('Limpar', 
					['action'=>'index'],
					['class'=>'btn btn-link', 'escape'=>false]
				);
			}
		?>
	</div>
</div>

<?= $this->Form->end() ?>