<style>
	#tbList tr{cursor: pointer;}
	.disabled{cursor: default !important;}
	.checked{background: blue;}
</style>

<?= $this->Form->create('Partidas', array('role'=>'form', 'class'=>'form-horizontal')) ?>

	<div class="box box-default">
		<div class="box-body">
			<?php if( $this->request->is('mobile') ): ?>
			<div class="table-responsive">
			<?php endif;?>
				<table id="tbList" class="table table-bordered table-hover table-striped">
					<thead>
						<tr>
							<th><input type="checkbox" id="checkAll"></th>
							<th>#ID</th>
							<th><?= __('Campeonato') ?></th>
							<th><?= __('Casa') ?></th>
							<th><?= __('Fora') ?></th>
							<th><?= __('Data e Hora') ?></th>
							<th><?= __('Odds') ?></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($partidas as $k=>$p): ?>
						<?php
							$have_odds = false;
							if ( isset($p->odds) ) {

								$have_odds = (!empty(current($p->odds)))? true:false;
							}

							$disabled = ($have_odds)? '' : 'disabled';
							$tr_class = ($have_odds)? '' : 'text-muted disabled';
						?>
						<tr class="<?= $tr_class ?>">
							<td>
							<?php
								if ($have_odds) {

									echo $this->Form->checkbox('partidas.'.$k, ['value'=>$p->id, 'hiddenField'=>false, $disabled]);
								}
							?>								
							</td>
							<td><?= h($p->id) ?></td>
							<td><?= h($p->league->name) ?></td>
							<td>
								<?= $this->Bet->image($p->home->image_id) ?>
								<?= h($p->home->name) ?>
							</td>
							<td>
								<?= $this->Bet->image($p->away->image_id) ?>
								<?= h($p->away->name) ?>
							</td>
							<td><?= date('d-m-Y H:i', $p->time) ?></td>
							<td><?= ($have_odds)? '<span class="label label-default">Sim</label>':'<span class="label label-danger">Não</label>' ?></td>
							<td class="actions" style="white-space:nowrap">
							<?php
								echo $this->Html->link('ver mais', 
									['controller'=>'Partidas', 'action'=>'view', $p->id]
								);
							?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php if( $this->request->is('mobile') ): ?>
			</div>
			<?php endif;?>
		</div>
		<div class="box-footer">
		<?php 
			echo $this->Form->button('Atualizar', [
				'class'=>'btn btn-primary btn-flat btn-lg col-md-3',
				'id'=>'loadButton', 'data-loading-text'=>'Aguarde...', 'submitContainer'=>null
			]);
		?>
		</div>
	</div>	

<?= $this->Form->end() ?>