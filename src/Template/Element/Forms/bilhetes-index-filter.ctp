<?= $this->Form->create(null, ['autocomplete'=>'off', 'valueSources'=>'query']) ?>

<?= $this->Form->hidden('submit', ['value'=>true]) ?>

<!-- revendedores -->
<div class="col-xs-12 col-md-3">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-user"></i></div>
			<?php
				echo $this->Form->select('approval_user',
					$users,
					['class'=>'form-control input-lg', 
						'multiple'=>false, 'empty'=>'-- revendedor', 'escape'=>false, 'required'=>false]
				);
			?>
		</div>
	</div>
</div>

<!-- # ID -->
<div class="col-xs-12 col-md-3">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-ticket"></i></div>
			<?php
				echo $this->Form->input('id', [
						'label'=>false, 'class'=>'form-control input-lg', 'placeholder'=>'número do bilhete'
					]
				);
			?>
		</div>
	</div>
</div>

<!-- Apostador -->
<div class="col-xs-12 col-md-3">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-user"></i></div>
			<?php
				echo $this->Form->input('apostador', [
						'label'=>false, 'class'=>'form-control input-lg', 'placeholder'=>'nome do apostador'
					]
				);
			?>
		</div>
	</div>
</div>

<div class="col-md-3">
	<div class="form-group">                
		<?php
			if ($this->request->is('mobile')) {

				echo $this->Form->button('<i class="fa fa-search"></i> &nbsp;&nbsp;'.__('Pesquisar'),
					['class'=>'btn btn-success btn-lg btn-block', 'value'=>'filterSubmit',
					'id'=>'loadButton', 'data-loading-text'=>'Aguarde...']
				);
			}
			else {
				
				echo $this->Form->button('<i class="fa fa-search"></i> &nbsp;&nbsp;'.__('Pesquisar'),
					['class'=>'btn btn-success btn-lg', 'value'=>'filterSubmit',
					'id'=>'loadButton', 'data-loading-text'=>'Aguarde...']
				);
			}

			if (!empty($_isSearch)) {

				echo $this->Html->link('Limpar', 
					['action'=>'index'],
					['class'=>'btn btn-link', 'escape'=>false]
				);
			}
		?>
	</div>
</div>

<?= $this->Form->end() ?>