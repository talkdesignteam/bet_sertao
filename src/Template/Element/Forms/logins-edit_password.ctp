<?= $this->Form->create($login, array('role'=>'form', 'class'=>'form-horizontal')) ?>
	
	<div class="box-body">
		<br>
		<!-- Login -->
		<div class="form-group">
			<label for="username" class="col-sm-2 control-label">Login</label>
			<div class="col-sm-4">
				<?php 
					echo $this->Form->input('username', 
						['type'=>'text', 'placeholder'=>'Login', 'label'=>false, 'class'=>'form-control', 'required'=>false, 'disabled']
					);
				?>
			</div>
		</div>
		<!-- E-mail -->
		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">E-mail</label>
			<div class="col-sm-4">
				<?php 
					echo $this->Form->input('email', 
						['type'=>'email', 'placeholder'=>false, 'label'=>false, 'class'=>'form-control', 'required'=>false, 'disabled']
					);
				?>
			</div>
		</div>
		<br>
		<!-- Senha -->
		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">Senha</label>
			<div class="col-sm-3">
				<?php 
					echo $this->Form->input('password', 
						['type'=>'password', 'placeholder'=>'Nova senha', 'label'=>false, 'class'=>'form-control input-lg', 'required'=>true, 'data-toggle'=>'password', 'data-message'=>'Clique aqui para mostrar/esconder a senha']
					);
				?>
			</div>
		</div>
		<div class="form-group">
			<label  class="col-sm-2 control-label">&nbsp;</label>
			<div class="col-sm-10">
				<?php 
					echo $this->Form->button('Atualizar', [
						'class'=>'btn btn-success btn-lg col-md-3',
						'id'=>'loadButton', 'data-loading-text'=>'Aguarde...', 'submitContainer'=>null
					]);
				?>
			</div>
		</div>
	</div>

<?= $this->Form->end() ?>