<?= $this->Form->create('Bilhetes', ['type' => 'post', 'autocomplete'=>'off']) ?>

<div class="col-xs-12 col-md-3">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-ticket"></i></div>
			<?php
				echo $this->Form->input('id', [
						'label'=>false, 'class'=>'form-control input-lg', 'placeholder'=>'número do bilhete', 'required'=>true
					]
				);
			?>
		</div>
	</div>             
</div>
<div class="col-md-2">
	<div class="form-group">                
		<?php
			if ($this->request->is('mobile')) {

				echo $this->Form->button(__('Consultar'),
					['class'=>'btn-lg btn-block botao', 'value'=>'filterSubmit',
					'id'=>'loadButton', 'data-loading-text'=>'Aguarde...']
				);
			}
			else {
				
				echo $this->Form->button(__('Consultar'),
					['class'=>'btn btn-lg botao', 'value'=>'filterSubmit',
					'id'=>'loadButton', 'data-loading-text'=>'Aguarde...']
				);
			}
		?>
	</div>
</div>
<?= $this->Form->end(); ?>