<?= $this->Form->create($partida, array('role'=>'form', 'class'=>'form-horizontal')) ?>
	<div class="box-body">
		<br>
		<div class="form-group">
			<label class="col-sm-2 control-label">Casa</label>
			<div class="col-sm-2">
				<?php 
					echo $this->Form->input('home_od', [
							'label'=>false, 'class'=>'form-control', 'autocomplete'=>'off',
							'odds-mask'
						]
					);
				?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Empate</label>
			<div class="col-sm-2">
				<?php 
					echo $this->Form->control('draw_od', [
							'label'=>false, 'class'=>'form-control', 'autocomplete'=>'off',
							'odds-mask'
						]
					);
				?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Fora</label>
			<div class="col-sm-2">
				<?php 
					echo $this->Form->control('away_od', [
							'label'=>false, 'class'=>'form-control', 'autocomplete'=>'off',
							'odds-mask'
						]
					);
				?>
			</div>
		</div>

		<br>
		<div class="form-group">
			<label  class="col-sm-2 control-label">&nbsp;</label>
			<div class="col-sm-10">
				<?php 
					echo $this->Form->button('Atualizar', [
						'class'=>'btn btn-success btn-lg col-md-3',
						'id'=>'loadButton', 'data-loading-text'=>'Aguarde...', 'submitContainer'=>null
					]);
				?>
			</div>
		</div>
	</div>

<?= $this->Form->end() ?>