<div class="box-body">
	<div class="row">
		<?php 
			echo $this->Html->link('Hoje', 
				['controller'=>'Bilhetes', 'action'=>'disponiveis', 'date'=>'today'],
				['class'=>'btn btn-link']
			);
			echo $this->Html->link('Amanhã', 
				['controller'=>'Bilhetes', 'action'=>'disponiveis', 'date'=>'tomorrow'],
				['class'=>'btn btn-link']
			);
		?>
	</div>
</div>

		<!-- Campeonato -->
<!-- 		<div class="form">
			<div class="form-group col-md-3">
				<label class="text-muted"><?= __('Campeonato') ?></label>
				<?php
					echo $this->Form->select('league_id',
						$leagues,
						['class'=>'form-control', 'multiple'=>false, 'empty'=>'-- todos']
					);
				?>
			</div>
		</div>
 -->
		<!-- Data -->
<!-- 		<div class="form">
			<div class="form-group col-md-3">
				<label class="text-muted"><?= __('Data') ?></label>
				<?php
					// echo $this->Form->select('time',
					// 	$times,
					// 	['class'=>'form-control', 'multiple'=>false, 'empty'=>'-- todos']
					// );
				?>
			</div>
		</div>
 -->