<?php if( $this->request->is('mobile') ): ?>
<div class="table-responsive">
<?php endif;?>
	<table id="tbList" class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th>#ID</th>
				<th>#FI</th>
				<th><?= __('Casa') ?></th>
				<th><?= __('Fora') ?></th>
				<th><?= __('Início') ?></th>
				<th><?= __('Apostas') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($partidas as $k=>$p): ?>
			<?php 
				echo $this->Form->hidden('apostas.'.$p->id.'.partida_id', ['value'=>$p->id]);

				echo $this->Form->hidden('apostas.'.$p->id.'.league_name', ['value'=>$p->league_name]);
				echo $this->Form->hidden('apostas.'.$p->id.'.home_name', ['value'=>$p->home_name]);
				echo $this->Form->hidden('apostas.'.$p->id.'.away_name', ['value'=>$p->away_name]);

				echo $this->Form->hidden('apostas.'.$p->id.'.palpite_id', ['value'=>2]);
				echo $this->Form->hidden('apostas.'.$p->id.'.palpite_odd', ['value'=>5]);
				echo $this->Form->hidden('apostas.'.$p->id.'.is_active', ['value'=>true]);
			?>
			<tr>
				<td><?= h($p->id) ?></td>
				<td><?= h($p->fi) ?></td>
				<td>
					<?= $this->Bet->image($p->home_image_id) ?>
					<?= h($p->home_name) ?>
				</td>
				<td>
					<?= $this->Bet->image($p->away_image_id) ?>
					<?= h($p->away_name) ?>
				</td>
				<td><?= date('d-m-Y H:i', $p->time) ?></td>
				<td class="actions">
				<?php
					$odds = json_decode($p->odds);

					$opt = [
						'home'=>'Casa: '.$odds->home_od,
						'draw'=>'Empate '.$odds->handicap,
						'away'=>'Fora '.$odds->away_od
					];
					$attr = [
						'class'=>'radio'
					];
					// echo $this->Form->input('apostas.'.$p->id.'.palpite_id', [
					// 	'templates' => [
					// 		'radioWrapper' => '<div class="radio">{{label}}</div>'
					// 	],
					// 	'type' => 'radio',
					// 	'options' => $opt,
					// 	'label' => false
					// ]);
				?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php if( $this->request->is('mobile') ): ?>
</div>
<?php endif;?>