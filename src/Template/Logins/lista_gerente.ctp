<section class="content-header">
	<h1>
		<?= __('Lista de gerentes') ?>&nbsp;
		<div class="pull-right"></div>
	</h1>
	<ol class="breadcrumb">
		<li>
			<?php 
				echo $this->Html->link('<i class="fa fa-angle-double-left"></i> '.__('Voltar'),
					'javascript:window.history.back()',
					['escape' => false]
				);
			?>
		</li>
	</ol>
</section>

<section class="content">
	
	<?php if(isset($logins)): ?>
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-default">
				<div class="box-body">
					<?= $this->element('Table/logins-lista_gerente') ?>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>

</section>

<?php
$this->Html->css([
	'AdminLTE./plugins/datatables/dataTables.bootstrap',
],
['block' => 'css']);

$this->Html->script([
	'AdminLTE./plugins/datatables/jquery.dataTables.min',
	'AdminLTE./plugins/datatables/dataTables.bootstrap.min'
],
['block' => 'script']);

	// URL for dataTables translate file
	$url_dataTable_translate_file = $this->Url->assetUrl('/datatables/Portuguese-Brasil.json', ['fullBase'=>true]);
?>
<?php $this->start('scriptBottom'); ?>
<script>
$(function () {

	$('#tbList').DataTable({
		"stateSave": true, 		// Salva o estado do filtro mesmo após reload de página e etc.
		"paging": false,			// Exibe paginação
		"lengthChange": true, 	// 
		"pageLength": 25,		// Num de registros por página
		"searching": true,		// Exibir filtro de pesquisa
		"ordering": false,		// Ordenar colunas
		"info": true,			// Informações da tabela
		"autoWidth": true,		// Auto redimensionamento da tabela
		"language": {
			"url": "<?= $url_dataTable_translate_file ?>" // "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
		},
	});

});
</script>
<?php $this->end(); ?>