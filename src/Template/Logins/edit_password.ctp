<section class="content-header">
	<h1>
		<?= __('Alterar senha') ?>&nbsp;
		<div class="pull-right"></div>
	</h1>
	<ol class="breadcrumb">
		<li>
			<?php 
				echo $this->Html->link('<i class="fa fa-angle-double-left"></i> '.__('Voltar'),
					'javascript:window.history.back()',
					['escape' => false]
				);
			?>
		</li>
	</ol>
</section>

<section class="content">
	
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-default">
				<div class="box-body">
					<?= $this->element('Forms/logins-edit_password') ?>
				</div>
			</div>
		</div>
	</div>

</section>

<?php
$this->Html->css([
	// 'AdminLTE./plugins/datatables/dataTables.bootstrap',
],
['block' => 'css']);

$this->Html->script([
	'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.0.3/bootstrap-show-password.min.js'
],
['block' => 'script']);

?>
<?php $this->start('scriptBottom'); ?>
<script>
	$(function () {

		//Password hide/show
		$("#password").password('hide');
		
	});
</script>
<?php $this->end(); ?>