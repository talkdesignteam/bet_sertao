<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Painel do revendedor
	</h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="alert alert-info">
		Olá, <strong>seja bem vindo</strong> ao painel do revendor do betsertão.
		<br>
		Em breve colocaremos aqui algumas funções e gráficos para facilitar a sua vida
	</div>
	<p>
		<strong>Saldo de apostas: </strong>
		<?php 
			echo $this->Number->format($saldo_apostas, [
				'places' => 2,
				'before' => 'R$ '
			]); 
		?>
	</p>
</section>