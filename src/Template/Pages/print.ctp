<?php
	// Definindo o layout da página
	$this->layout = 'AdminLTE.print';
	
	// Incluindo arquivos css no bloco para o layout
	$this->Html->css([
		// 'print',
	],
	['block' => 'css']);
?>
<div class="container">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-stripe text-center">
					<thead>
						<tr>
							<th class="text-center">Número</th>
							<th class="text-center">Cambista</th>
							<th class="text-center">Cliente</th>
							<th class="text-center">Aposta</th>
							<th class="text-center">Prêmio</th>
							<th class="text-center">Situação</th>
							<th class="text-center">Cadastro</th>
							<th class="text-center">Modificado</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>2018000</td>
							<td>Anderson</td>
							<td>Anderson</td>
							<td>R$10,00</td>
							<td>R$107,80</td>
							<td>aguardando validação</td>
							<td>14/09/2018 08:51:28</td>
							<td>14/09/2018 08:51:28</td>
						</tr>
					</tbody>
				
				</table>
				<table class="table table-stripe">
					<thead>
						<tr>
							<th>ID</th>
							<th>Partida</th>
							<th>Palpite</th>
							<th>Status</th>							
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>#3</td>	
							<td>Brasil Série A | Cruzeiro x Atlético Mineiro (16/09/2018 16:00:00)</td>
							<td>Casa | 2,80</td>
							<td>Aguardando resultado</td>
						</tr>
						<tr>
							<td>#4</td>	
							<td>Brasil Série A | Grêmio x Paraná (15/09/2018 16:00:00)</td>
							<td>Casa | 1,40</td>
							<td>Aguardando resultado</td>
						</tr>	
						<tr>
							<td>#5</td>	
							<td>Brasil Série A | Santos XI x São Caetano (15/09/2018 11:00:00)</td>
							<td>Casa | 2,75</td>
							<td>Aguardando resultado</td>
						</tr>						
						
					</tbody>
				
				</table>

			</div>
		</div>
		
	</div>