<!-- Boxes no topo -->
<section class="content-header">
<!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
            <div class="inner">
                <h3>150</h3>

                <p>Bilhetes cadastrados</p>
            </div>
            <div class="icon">
                <i class="fa fa-ticket"></i>
            </div>            
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
            <div class="inner">
                <h3>30</h3>

                <p>Cambistas registrados</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>            
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
            <div class="inner">
                <h3>R$2.500,00</h3>

                <p>Em saldo no sistema</p>
            </div>
            <div class="icon">
                <i class="fa fa-money"></i>
            </div>            
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
            <div class="inner">
                <h3>20</h3>

                <p>Apostas canceladas</p>
            </div>
            <div class="icon">
                <i class="fa fa-ban"></i>
            </div>            
            </div>
        </div>
        <!-- ./col -->       
    </div>
      <!-- /.row -->
</section>
<!-- Extrato do sistema-->
<section class="content-header">
      <h1>
        Extrato do sistema
        <small>Informações atualizadas</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Administrador</a></li>
        <li class="active">Painel</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Apostas realizadas hoje</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Partidas</th>
                  <th>Cambista</th>
                  <th>Cliente</th>
                  <th style="width: 40px">Situação</th>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>Flamengo x São Paulo</td>
                  <td>David</td>
                  <td>Anderson</td>
                  <td><span class="badge bg-red">Errou</span></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>Flamengo x São Paulo</td>
                  <td>David</td>
                  <td>Anderson</td>
                  <td><span class="label label-primary">Acertou</span></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>Flamengo x São Paulo</td>
                  <td>David</td>
                  <td>Anderson</td>
                  <td><span class="label label-warning">Aguardando aprovação</span></td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>Flamengo x São Paulo</td>
                  <td>David</td>
                  <td>Anderson</td>
                  <td><span class="label label-default">Cancelado</span></td>
                </tr>
                
                
                
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>
          </div>
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Bilhetes a serem validados</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Partidas</th>
                  <th>Cliente</th>
                  <th>Cambista</th>
                  <th style="width: 40px">Prêmio</th>
                  <th>Ações</th>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>Flamengo x Cruzeiro</td>
                  <td>
                   Anderson
                  </td>
                  <td>David</td>
                  <td>R$124,00</td>
                  <td>
                    <a href="#" class="btn btn-primary btn-xs" title="Validar a aposta"><i class="fa fa-check"></i>Validar</a>
                    <a href="#" class="btn btn-default btn-xs" title="Imprimir bilhete"><i class="fa fa-print"></i>Imprimir</a>
                    <a href="#" class="btn btn-default btn-xs" title="Ver mais detalhes"><i class="fa fa-search"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>Flamengo x Cruzeiro</td>
                  <td>
                   Anderson
                  </td>
                  <td>David</td>
                  <td>R$124,00</td>
                  <td>
                    <a href="#" class="btn btn-primary btn-xs" title="Validar a aposta"><i class="fa fa-check"></i>Validar</a>
                    <a href="#" class="btn btn-default btn-xs" title="Imprimir bilhete"><i class="fa fa-print"></i>Imprimir</a>
                    <a href="#" class="btn btn-default btn-xs" title="Ver mais detalhes"><i class="fa fa-search"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>Flamengo x Cruzeiro</td>
                  <td>
                   Anderson
                  </td>
                  <td>David</td>
                  <td>R$124,00</td>
                  <td>
                    <a href="#" class="btn btn-primary btn-xs" title="Validar a aposta"><i class="fa fa-check"></i>Validar</a>
                    <a href="#" class="btn btn-default btn-xs" title="Imprimir bilhete"><i class="fa fa-print"></i>Imprimir</a>
                    <a href="#" class="btn btn-default btn-xs" title="Ver mais detalhes"><i class="fa fa-search"></i></a>
                  </td>
                </tr>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Total de apostas</h3>               
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                            <i class="fa fa-bar-chart-o"></i>
                            <h3 class="box-title">Gráfico</h3>
                            <div class="box-tools pull-right">
                                Tempo real
                                <div class="btn-group" id="realtime" data-toggle="btn-toggle">
                                <button type="button" class="btn btn-default btn-xs active" data-toggle="on">On</button>
                                <button type="button" class="btn btn-default btn-xs" data-toggle="off">Off</button>
                                </div>
                            </div>
                            </div>
                            <div class="box-body">
                            <div id="interactive" style="height: 300px;"></div>
                            </div>
                            <!-- /.box-body-->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->     
    </section>
    <!-- /.content -->

<?php
$this->Html->script([
  'AdminLTE./plugins/flot/jquery.flot.min',
  'AdminLTE./plugins/flot/jquery.flot.resize.min',
  'AdminLTE./plugins/flot/jquery.flot.pie.min',
  'AdminLTE./plugins/flot/jquery.flot.categories.min',
],
['block' => 'script']);
?>
<script>
$(function () {
    /*
     * Flot Interactive Chart
     * -----------------------
     */
    // We use an inline data source in the example, usually data would
    // be fetched from a server
    var data = [], totalPoints = 100;

    function getRandomData() {

      if (data.length > 0)
        data = data.slice(1);

      // Do a random walk
      while (data.length < totalPoints) {

        var prev = data.length > 0 ? data[data.length - 1] : 50,
            y = prev + Math.random() * 10 - 5;

        if (y < 0) {
          y = 0;
        } else if (y > 100) {
          y = 100;
        }

        data.push(y);
      }

      // Zip the generated y values with the x values
      var res = [];
      for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]]);
      }

      return res;
    }

    var interactive_plot = $.plot("#interactive", [getRandomData()], {
      grid: {
        borderColor: "#f3f3f3",
        borderWidth: 1,
        tickColor: "#f3f3f3"
      },
      series: {
        shadowSize: 0, // Drawing is faster without shadows
        color: "#3c8dbc"
      },
      lines: {
        fill: true, //Converts the line chart to area chart
        color: "#3c8dbc"
      },
      yaxis: {
        min: 0,
        max: 100,
        show: true
      },
      xaxis: {
        show: true
      }
    });

    var updateInterval = 500; //Fetch data ever x milliseconds
    var realtime = "on"; //If == to on then fetch data every x seconds. else stop fetching
    function update() {

      interactive_plot.setData([getRandomData()]);

      // Since the axes don't change, we don't need to call plot.setupGrid()
      interactive_plot.draw();
      if (realtime === "on")
        setTimeout(update, updateInterval);
    }

    //INITIALIZE REALTIME DATA FETCHING
    if (realtime === "on") {
      update();
    }
    //REALTIME TOGGLE
    $("#realtime .btn").click(function () {
      if ($(this).data("toggle") === "on") {
        realtime = "on";
      }
      else {
        realtime = "off";
      }
      update();
    });
</script>