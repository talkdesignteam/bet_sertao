<?php
	// Definindo o layout da página
	$this->layout = 'AdminLTE.front';
	
	// Incluindo arquivos css no bloco para o layout
	$this->Html->css([
		'regulamento'
	],
	['block' => 'css']);

	// Incluindo arquivos de javascript no bloco para o layout
	$this->Html->script([
		// 'home'
	],
	['block' => 'scriptBottom']);
?>

<div class="container">	
	<div class="publicidade">
		<p>Publicidade</p>
	</div>
	<div class="page-header">
		<h1 class="titulo-pagina">Regulamento</h1>	
	</div>
	
	<div class="conteudo">
		<div class="textos">
			<p>I- Jogos com horário errado, que seja por causa de mudança por parte da organização, falha no sistema ou erro de digitação, 
			ficarão valendo apenas as apostas feitas até 1 minuto antes do horário oficial do jogo.</p>
			<p>II- Jogos com os times invertidos por exemplo Palmeiras x Curitiba, o palmeiras joga em casa (São Paulo), 
			caso o jogo esteja Curitiba x Palmeiras o mesmo será cancelado.</p>
			<p>III- Apostas feitas em jogos já iniciados serão cancelados, pois não trabalhamos com apostas em jogos ao vivo.</p>
			<p>IV- Apostas feitas em jogos adiados, com data errada ou em jogo interrompidos serão cancelados.</p>
			<p>V- Em casadinhas, caso seu jogo esteja em algum dos casos das regras I, II, III e IV, 
			ficarão valendo apenas os jogos restantes e o prêmio será recalculado.</p>

			<p>VI- Jogos em que haja mudança de data que seja pra mais de 72h, serão cancelados. Porém, 
			jogos com mudança de horário ficam valendo as apostas feitas até 1 minuto antes do horário oficial.</p>
			<p>VII- Em jogos com cotações acima do normal a Betsertão fica com direito de cancelar o bilhete a 
			qualquer momento, por exemplo: o time da casa paga 1,80 e na média das casas de apostas internacionais 
			esteja 1,40, nesse caso a cotação está errada e o bilhete pode ser cancelado pela Betsertão a qualquer momento.</p>
			<strong>Essas regras são válidas para todos os jogos feitos no site ou por meio de representantes.</strong>
		</div>
	</div>
</div>


<?php $this->start('scriptBottom'); ?>
<script>
$(document).ready(function(){

});	
</script>
<?php $this->end(); ?>