<?php
	// Definindo o layout da página
	$this->layout = 'AdminLTE.front';
	
	// Incluindo arquivos css no bloco para o layout
	$this->Html->css([
		'contato',

	],
	['block' => 'css']);

	// Incluindo arquivos de javascript no bloco para o layout
	$this->Html->script([
		// 'home'
	],
	['block' => 'scriptBottom']);
?>

<div class="container">	
	<div class="page-header centraliza">
		<h1 class="titulo-pagina">Fale conosco</h1>
		<p>Utilize o formulário de contato para falar conosco.</p>	
	</div>
	<div class="formulario">
		<div class="cols">
			<div class="col -dozecolunas">
				<div class="form-group">
					<input type="text" placeholder="Seu nome"/>
				</div>
			</div>
			<div class="col -seiscolunas">
				<div class="form-group">
					<input type="email" placeholder="Informe seu e-mail"/>
				</div>
			</div>
			<div class="col -seiscolunas">
				<div class="form-group">
					<input type="tel" placeholder="Informe seu telefone"/>
				</div>
			</div>
			<div class="col -dozecolunas">
				<div class="form-group">
					<input type="text" placeholder="Assunto"/>
				</div>
			</div>
			<div class="col -dozecolunas">
				<div class="form-group">
					<textarea rows="5" placeholder="Sua mensagem"></textarea>
				</div>
			</div>
			<div class="col -dozecolunas acao">
				<button type="submit" class="botao -medio">Enviar mensagem</button>
			</div>
		</div>
	</div>
	<div class="publicidade">
		<p>Publicidade</p>
	</div>
</div>


<?php $this->start('scriptBottom'); ?>
<script>
$(document).ready(function(){

});	
</script>
<?php $this->end(); ?>