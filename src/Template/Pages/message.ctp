<?php $this->layout = 'public'; ?>

<?php if( !$this->request->is('mobile') ): ?>
<div class="container">
<?php endif; ?>
	
	<?= $this->Flash->render() ?>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php 
					echo $this->Html->link('<i class="fa fa-arrow-left"></i> '.__('voltar'),
						'javascript:window.history.back()',
						['escape' => false, 'class'=>'btn btn-default']
					);
					echo '&nbsp;';
					echo $this->Html->link('<i class="fa fa-home"></i> '.__('Início'),
						['controller'=>'Pages', 'action'=>'display', 'home', 'plugin'=>false, 'prefix'=>false, '_full'=>true],
						['escape' => false, 'class'=>'btn btn-default']
					);
				?>
			</div>
		</div>
	</section>

<?php if( !$this->request->is('mobile') ): ?>
</div>
<?php endif; ?>