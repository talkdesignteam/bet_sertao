<section class="content-header">
	<h1>
		<?= __('Editar Odds') ?>&nbsp;
		<div class="pull-right"></div>
	</h1>
	<ol class="breadcrumb">
		<li>
			<?php 
				echo $this->Html->link('<i class="fa fa-angle-double-left"></i> '.__('Voltar'),
					'javascript:window.history.back()',
					['escape' => false]
				);
			?>
		</li>
	</ol>
</section>

<section class="content">
	
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<strong><?= $partida->league_name ?></strong><br>
					<?= $partida->home_name.' x '.$partida->away_name ?><br>
					<em><?= date('d/m/Y \a\s h:i', $partida->time) ?></em>
				</div>
				<div class="box-body">
					<?= $this->element('Forms/partidas-editar_odds') ?>
				</div>
			</div>
		</div>
	</div>

</section>

<?php
$this->Html->css([
	// '',
],
['block' => 'css']);

$this->Html->script([
	'AdminLTE./plugins/input-mask/jquery.inputmask',
	'AdminLTE./plugins/input-mask/jquery.inputmask.extensions',
	'AdminLTE./plugins/input-mask/jquery.inputmask.numeric.extensions',
],
['block' => 'script']);

?>
<?php $this->start('scriptBottom'); ?>
<script>
	$(function () {

		// ODDS Mask
		$("[odds-mask]").inputmask("[9]9.99", {
			positionCaretOnClick: "radixFocus",
			radixPoint: ".",
			_radixDance: true,
			numericInput: true,
			placeholder: "",
			definitions: {
				"0": {
					validator: "[0-9\uFF11-\uFF19]"
				}
			}
		});

	});
</script>
<?php $this->end(); ?>