<?php $this->layout = 'login'; ?>

<!-- page header --> 
<section class="content-header">
	<h1>
		<?= __('Auto update table') ?>&nbsp;
	</h1>
</section>

<!-- page content -->
<section class="content">
	<div class="row">

		<div class="col-xs-12">
		<?php
			if(isset($errors)) {

				foreach ($errors as $k=>$e) {
					
					echo "$e <br>";
				}
			}

			if(isset($count_saved))
				echo "Partidas adicionadas ou atualizadas: $count_saved <br>";
			
			if(isset($count_not_saved))
				echo "Partidas não salvas: $count_not_saved <br>";

			if(isset($count_not_odds))
				echo "Partidas sem ODDS: $count_not_odds <br>";
		?>
		</div>

	</div>
</section>