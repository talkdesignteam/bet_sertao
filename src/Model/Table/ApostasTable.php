<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Apostas Model
 *
 */
class ApostasTable extends Table
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->setTable('apostas');
		$this->setDisplayField('id');
		$this->setPrimaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('Bilhetes', [
			'foreignKey' => 'bilhete_id',
			'joinType' => 'INNER'
		]);
		$this->belongsTo('Partidas', [
			'foreignKey' => 'partida_id',
			'joinType' => 'INNER'
		]);
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->integer('id')
			->allowEmpty('id', 'create');

		$validator
			->integer('partida_id')
			->requirePresence('partida_id', 'create')
			->notEmpty('partida_id');

		$validator
			->integer('bilhete_id')
			// ->requirePresence('bilhete_id', 'create')
			->allowEmpty('bilhete_id');

		$validator
			->maxLength('league_name', 150)
			->requirePresence('league_name', 'create')
			->notEmpty('league_name');
		
		$validator
			->maxLength('home_name', 150)
			->requirePresence('home_name', 'create')
			->notEmpty('home_name');
		
		$validator
			->maxLength('away_name', 150)
			->requirePresence('away_name', 'create')
			->notEmpty('away_name');

		$validator
			->maxLength('palpite_id', 50)
			->requirePresence('palpite_id', 'create')
			->notEmpty('palpite_id');

		$validator
			->maxLength('palpite_odd', 10)
			->requirePresence('palpite_odd', 'create')
			->notEmpty('palpite_odd');

		$validator
			->boolean('palpite_win')
			->allowEmpty('palpite_win');

		$validator
			->boolean('is_active')
			->allowEmpty('is_active');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{
		$rules->add($rules->existsIn(['bilhete_id'], 'Bilhetes'));
		$rules->add($rules->existsIn(['partida_id'], 'Partidas'));

		return $rules;
	}
}
