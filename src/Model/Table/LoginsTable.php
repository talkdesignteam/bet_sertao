<?php
namespace App\Model\Table;

// for beforeMarshal callback
use Cake\Event\Event;
use ArrayObject;

use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Network\Session;
use Cake\Core\Configure;


/**
 * Logins Model
 *
 */
class LoginsTable extends Table {

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config) {

		parent::initialize($config);

		$this->setTable('users');
		$this->setDisplayField('username');
		$this->setPrimaryKey('id');

		// $this->addBehavior('Acl.Acl', ['type' => 'requester']);
		$this->addBehavior('Timestamp');

		$this->belongsTo('AccessManager.Groups', [
			'foreignKey' => 'group_id',
			'joinType' => 'INNER',
			'className' => 'AccessManager.Groups'
		]);
		$this->belongsTo('AccessManager.Roles', [
			'foreignKey' => 'role_id',
			'joinType' => 'INNER',
			'className' => 'AccessManager.Roles'
		]);
		$this->hasOne('Loginprofiles', [
			'foreignKey' => 'user_id',
			'joinType' => 'INNER',
			'className' => 'Loginprofiles'
		]);

		// Dados do usuário logado
		$session = new Session();
		$User = $session->read('Auth.User');
		$this->user_id = $User['id'];
		$this->user_role_id = $User['role_id'];
		$this->user_group_id = $User['group_id'];

		// variáveis do app
		$this->admin_role_id = Configure::read('Default.Vars.admin_role_id');
		$this->cambista_role_id = Configure::read('Default.Vars.cambista_role_id');
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)	{
		
		$validator
			->integer('id')
			->allowEmpty('id', 'create');

		$validator
			->scalar('password')
			->requirePresence('password', 'create')
			->notEmpty('password', 'Insira uma senha válida.');

		$validator
				->requirePresence('username', 'create')
				->notEmpty('username', 'Insira um nome de usuário válido.')
				->add('username', 'unique', [
						'rule'=>'validateUnique',
						'provider'=>'table',
						'message'=>'Este nome de usuário já está em uso por outro usuário.'
					]
				);

		$validator
				->email('email')
				->allowEmpty('email')
				->add('email', 'unique', [
						'rule'=>'validateUnique',
						'provider'=>'table',
						'message'=>'Este e-mail já está em uso por outro usuário.'
					]
				);

		$validator
			->boolean('active')
			->allowEmpty('active');

		return $validator;
	}

	public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {   
		
		if (isset($data['email']) and empty($data['email'])) {

			unset($data['email']);
		}
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules) {

		$rules->add($rules->isUnique(['email']));
		$rules->add($rules->isUnique(['username']));
		$rules->add($rules->existsIn(['group_id'], 'Groups'));
		$rules->add($rules->existsIn(['role_id'], 'Roles'));

		return $rules;
	}

 	/**
	 * beforeFind
	 *
	 */
	public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary) {
		
		// Se não for usuário ADMIN então exibe apenas os usuários que ele criou
		if (!isset($options['role_filter']) and $this->user_role_id==$this->admin_role_id) $options['role_filter'] = false;
		if (!isset($options['role_filter'])) $options['role_filter'] = true;
		
		if ( $options['role_filter']==true ) {

			$query
				->where([
					'AND' => [
						'Logins.user_creator_id'=>$this->user_id
					]
				]);
		}

		return $query;
	}
	
	/**
	 * Retorna lista de ADMINS para selections/options e etc.
	 */
	public function getListAdmins() {

		$query = $this->find('list', [
			'keyField' => 'id',
			'valueField' => 'username'
		]);		
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('role_id'), $query->newExpr(':role_id'))
					->eq(new IdentifierExpression('active'), true);
			})
			->order([
				'Logins.username ASC'
			])
			->bind(':role_id', $this->admin_role_id, 'integer');

		return $query->toArray();
	}

	/**
	 * Retorna lista de GERENTES para selections/options e etc.
	 */
	public function getListGerentes() {

		$query = $this->find('list', [
			'keyField' => 'id',
			'valueField' => 'username'
		]);		
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('role_id'), $query->newExpr(':role_id'))
					->eq(new IdentifierExpression('active'), true);
			})
			->order([
				'Logins.username ASC'
			])
			->bind(':role_id', $this->gerente_role_id, 'integer');

		return $query->toArray();
	}

	/**
	 * Retorna lista de REVENDEDORES para selections/options e etc.
	 */
	public function getListRevendedores() {

		$query = $this->find('list', [
			'keyField' => 'id',
			'valueField' => 'username'
		]);		
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('role_id'), $query->newExpr(':role_id'))
					->eq(new IdentifierExpression('active'), true);
			})
			->order([
				'Logins.username ASC'
			])
			->bind(':role_id', $this->cambista_role_id, 'integer');

		return $query->toArray();
	}
}
