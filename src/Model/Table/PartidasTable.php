<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Partidas Model
 */
class PartidasTable extends Table
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->setTable('partidas');
		$this->setDisplayField('id');
		$this->setPrimaryKey('id');

		$this->addBehavior('Timestamp');

		$this->hasMany('Apostas', [
			'foreignKey' => 'partida_id'
		]);

		// Search behavior
		$this->addBehavior('Search.Search');
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->integer('id')
			->allowEmpty('id', 'create');

		$validator
			->scalar('time')
			->maxLength('time', 255)
			->requirePresence('time', 'create')
			->notEmpty('time');

		$validator
			->integer('time_status')
			->requirePresence('time_status', 'create')
			->notEmpty('time_status');

		$validator
			->integer('league_id')
			->requirePresence('league_id', 'create')
			->notEmpty('league_id');

		$validator
			->scalar('league_name')
			->maxLength('league_name', 150)
			->allowEmpty('league_name');

		$validator
			->scalar('league_cc')
			->maxLength('league_cc', 25)
			->allowEmpty('league_cc');

		$validator
			->integer('home_id')
			->requirePresence('home_id', 'create')
			->notEmpty('home_id');

		$validator
			->scalar('home_name')
			->maxLength('home_name', 150)
			->allowEmpty('home_name');

		$validator
			->scalar('home_cc')
			->maxLength('home_cc', 25)
			->allowEmpty('home_cc');

		$validator
			->integer('away_id')
			->requirePresence('away_id', 'create')
			->notEmpty('away_id');

		$validator
			->scalar('away_name')
			->maxLength('away_name', 150)
			->allowEmpty('away_name');

		$validator
			->scalar('away_cc')
			->maxLength('away_cc', 25)
			->allowEmpty('away_cc');

		$validator
			->requirePresence('odds', 'create')
			->maxLength('odds', 10000)
			->notEmpty('odds');

		$validator
			->boolean('is_active')
			->requirePresence('is_active', 'create')
			->notEmpty('is_active');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
	public function buildRules(RulesChecker $rules)
	{

		return $rules;
	}

	/**
	 * @return \Search\Manager
	 */
	public function searchManager() {

		$searchManager = $this->behaviors()->Search->searchManager();
		$searchManager
			->value('id')
			->add('time', 'Search.Callback', [
				'callback' => function ($query, $args, $filter) {
					
					if(isset($args['time'])){

						switch ($args['time']) {
							
							case 'today':
								$query
									->where([
										'AND' => array(
											'time >=' => strtotime('now'),
											'time <' => strtotime('tomorrow')
										),
									]);
								break;

							case 'tomorrow':
								$query
									->where([
										'AND' => array(
											'time >=' => strtotime('tomorrow'),
											'time <' => strtotime('tomorrow +2 day')
										),
									]);
								break;
							
							default:
								$query
									->where([
										'AND' => array(
											'time >=' => strtotime('now')
										),
									]);
								break;
						}
					}
					return $query;
				}
			])
			->value('league_id')
			->add('league_name', 'Search.Like', [
				'before' => '%',
				'after' => '%',
				'comparison' => 'LIKE'
			])
			// league_cc
			// home_id
			// home_name
			// home_cc
			// away_id
			// away_name
			// away_cc
			// odds
			->add('is_active', 'Search.Boolean');

		return $searchManager;
	}
}
