<?php
namespace App\Model\Table;

// for beforeMarshal callback
use Cake\Event\Event;
use ArrayObject;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Network\Session;
use Cake\Core\Configure;


/**
 * Bilhetes Model
 *
 */
class BilhetesTable extends Table
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->setTable('bilhetes');
		$this->setDisplayField('cliente');
		$this->setPrimaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('AccessManager.Users', [
			'foreignKey' => 'approval_user',
			'joinType' => 'INNER',
			'className' => 'AccessManager.Users'
		]);

		$this->hasMany('Apostas', [
			'foreignKey' => 'bilhete_id',
			'dependent' => true,
			'cascadeCallbacks' => true
		]);

		// Search Behavior
		$this->addBehavior('Search.Search');

		// Dados do usuário logado
		$session = new Session();
		$User = $session->read('Auth.User');
		$this->user_id = $User['id'];
		$this->user_role_id = $User['role_id'];
		$this->user_group_id = $User['group_id'];

		// variáveis do app
		$this->admin_role_id = Configure::read('Default.Vars.admin_role_id');
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->integer('id')
			->allowEmpty('id', 'create');

		$validator
			->scalar('apostador')
			->maxLength('apostador', 150, 'limite de 150 caracteres')
			->requirePresence('apostador', 'create', 'campo obrigatório*')
			->notEmpty('apostador', 'campo obrigatório*');

		$validator
			->scalar('revendedor')
			->maxLength('revendedor', 150, 'limite de 150 caracteres')
			->allowEmpty('revendedor', 'campo obrigatório*');

		$validator
			->numeric('valor_apostado', ' obrigatório*')
			->requirePresence('valor_apostado', 'create', 'campo obrigatório*')
			->notEmpty('valor_apostado', 'campo obrigatório*')
			->add('valor_apostado', 'minValue', [
				'rule' => function ($value, $provider) {
					if ($value >= 2) {
						return true;
					}
					return 'aposta mínina: R$ 2,00';
				}
			])
			->add('valor_apostado', 'maxValue', [
				'rule' => function ($value, $provider) {
					if ($value <= 1000) {
						return true;
					}
					return 'aposta máxima: R$ 1000,00';
				}
			]);

		$validator
			->boolean('is_approved')
			->allowEmpty('is_approved');

		$validator
			->dateTime('date_approved')
			->allowEmpty('date_approved');
		
		$validator
			->integer('user_approved')
			->allowEmpty('user_approved', 'create');

		$validator
			->boolean('is_active')
			->allowEmpty('is_active');

		return $validator;
	}

	/**
	 * @return \Search\Manager
	 */
	public function searchManager() {

		$searchManager = $this->behaviors()->Search->searchManager();
		$searchManager
			->value('id')
			->value('approval_user')
			->add('apostador', 'Search.Like', [
				'before' => '%',
				'after' => '%',
				'comparison' => 'LIKE'
			])
			->add('revendedor', 'Search.Like', [
				'before' => '%',
				'after' => '%',
				'comparison' => 'LIKE'
			])
			->add('is_approved', 'Search.Callback', [
				'callback' => function ($query, $args, $filter) {
					
					if(isset($args['is_approved'])){
						
						if (is_numeric($args['is_approved'])) {

							$query
								->where([
									'OR' => array(
										'is_approved'=>$args['is_approved']
									),
								]);
						}
					}
					return $query;
				}
			])
			->add('is_active', 'Search.Callback', [
				'callback' => function ($query, $args, $filter) {
					
					if(isset($args['is_active'])){

						if (is_numeric($args['is_active'])) {

							$query
								->where([
									'OR' => array(
										'is_active'=>$args['is_active']
									),
								]);
						}
					}
					return $query;
				}
			]);
			// ->add('is_active', 'Search.Boolean');

		return $searchManager;
	}

	public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {   
		
		// Unset Apostas->odds and palpite_odd
		if (isset($data['apostas'])) {

			foreach ($data['apostas'] as $k=>$a) {
				
				if (isset($a['odds'])) {

					$odds = trim($a['odds'], '"');
					$odds = stripcslashes($odds);
					$odds = json_decode($odds);

					unset($a['odds']); // tira do array Entity (não existe esse campo na tabela);

					if ( !empty($a['palpite_id']) and isset($odds->{$a['palpite_id']}) ) {

						$a['palpite_odd'] = $odds->{$a['palpite_id']};
						$a['is_active'] = (!empty($odds->{$a['palpite_id']}))? true : false;

						$data['apostas'][$k] = $a;
					}
					else{

						unset($data['apostas'][$k]);
					}
				}
			}
		}

		// soma_odds
		if (!isset($data['soma_odds']) and isset($data['apostas'])) {
			
			$palpites = array_column($data['apostas'], 'palpite_odd');
			if (!empty($palpites)) {

				$soma_odds = round(array_product($palpites), 2);
				$data['soma_odds'] = (float) $soma_odds;
			}
		}

		// valor_premio, valor_revendedor e valor_total
		if(isset($data['valor_apostado']) and !empty($data['soma_odds'])) {

			if (is_numeric($data['valor_apostado']) and is_numeric($data['soma_odds'])) {

				$valor_total = round($data['soma_odds']*$data['valor_apostado'], 2);
				$valor_premio = round($valor_total-((10/100)*$valor_total), 2); // site paga - 10% do revendedor
				$valor_revendedor = round((10/100)*$valor_total, 2); // 10% do valor total
				
				// valor_premio
				if (!isset($data['valor_premio'])) 
					$data['valor_premio'] = (float) $valor_premio;

				// valor_revendedor
				if (!isset($data['valor_revendedor'])) 
					$data['valor_revendedor'] = (float) $valor_revendedor;
				
				// valor_total
				if (!isset($data['valor_total'])) 
					$data['valor_total'] = (float) $valor_total;
			}
		}

		// expiration_datetime
		if(!isset($data['expiration_datetime']) and isset($data['apostas'])) {

			$partidas_time = array_column($data['apostas'], 'partida_time');
			$data['expiration_datetime'] = !empty($partidas_time)? min($partidas_time) : null;
		}

		// is_active
		if (!isset($data['is_active'])) {

			if (!empty($data['valor_apostado']) and !empty($data['valor_premio']) and !empty($data['valor_revendedor']) and !empty($data['valor_total'])) {

				$data['is_active'] = true;
			}
			else{

				$data['is_active'] = false;
			}
		}
	}

	public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary) {
		
		// Se não for usuário ADMIN então exibe apenas os bilhetes que ele aprovou
		if (!isset($options['role_filter']) and $this->user_role_id==$this->admin_role_id) $options['role_filter'] = false;
		if (!isset($options['role_filter'])) $options['role_filter'] = true;
		
		if ( $options['role_filter']==true ) {

			if ($primary) {

				$query
					->contain(['Users'])
					->where([
						'AND' => [
							'OR' => [
								'Bilhetes.approval_user'=>$this->user_id,
								'Users.user_creator_id'=>$this->user_id
							]
						]
					]);
			}
			else{

				$query->where([
					'AND' => [
						'Bilhetes.approval_user'=>$this->user_id
					]
				]);
			}
		}

		return $query;
	}
}