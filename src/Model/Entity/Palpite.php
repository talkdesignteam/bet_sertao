<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Palpite extends Entity
{

	protected $_accessible = [
		'nome' => true,
		'descricao' => true,
		'calculo' => true,
		'created' => true,
		'modified' => true,
		'apostas' => true
	];
}
