<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bilhete Entity
 *
 */
class Bilhete extends Entity
{

	/**
	 * Fields that can be mass assigned using newEntity() or patchEntity().
	 *
	 * Note that when '*' is set to true, this allows all unspecified fields to
	 * be mass assigned. For security purposes, it is advised to set '*' to false
	 * (or remove it), and explicitly make individual fields accessible as needed.
	 *
	 * @var array
	 */
	protected $_accessible = [
		'apostador' => true,
		'apostador_fone1' => true,
		'revendedor' => true,
		'valor_apostado' => true,
		'soma_odds' => true,
		'valor_premio' => true,
		'valor_revendedor' => true,
		'valor_total' => true,
		'expiration_datetime' => true,
		'is_approved' => true,
		'approval_datetime' => true,
		'approval_user' => true,
		'won' => true,
		'is_active' => true,
		'created' => true,
		'modified' => true,
		'apostas' => true,
		'users' => false
	];

	// protected $_virtual = ['valor_premio_pago'];

	/**
	 * Valor do bilhete de acordo com a situação atual dele.
	 * Obs: só será positivo caso o bilhete esteja ativado, aprovado e prêmiado.
	 */
	protected function _getValorPremioReal($value) {
		
		if ($this->_properties['valor_premio']) {

			return ($this->__validaPremio($this->_properties))? 
				$this->_properties['valor_premio'] : 0;
		}

		return null;
	}

	/**
	 * Valor de premiação já pago ao apostador.
	 * Obs: caso o bilhete não tenha sido premiado mas não pago ao cliente, o valor é 0.
	 */
	protected function _getValorPremioPago() {
		
		if ($this->_properties['valor_premio']) {

			return ($this->__validaPremio($this->_properties) and $this->_properties['pgto_premio']===true)? 
				$this->_properties['valor_premio'] : 0;
		}

		return null;
	}

	/**
	 * Valor do bilhete de acordo com a situação atual dele.
	 * Obs: só será positivo caso o bilhete esteja ativado, aprovado e prêmiado.
	 */
	protected function _getValorRevendedorReal($value) {
		
		if ($this->_properties['valor_revendedor']) {

			return ($this->__validaLucroRevendedor($this->_properties))? 
				$this->_properties['valor_revendedor'] : 0;
		}

		return null;
	}

	/**
	 * Valor de premiação já pago ao apostador.
	 * Obs: caso o bilhete não tenha sido premiado mas não pago ao cliente, o valor é 0.
	 */
	protected function _getValorRevendedorPago() {
		
		if ($this->_properties['valor_revendedor']) {

			return ($this->__validaLucroRevendedor($this->_properties) and $this->_properties['pgto_revendedor']===true)? 
				$this->_properties['valor_revendedor'] : 0;
		}

		return null;
	}

	/**
	 * Valor total pago pelo site do bilhete.
	 * Obs: só será positivo caso o bilhete esteja ativado, aprovado e prêmiado.
	 */
	protected function _getValorTotalReal($value) {
		
		if ($this->_properties['valor_total']) {

			return ($this->__validaLucroRevendedor($this->_properties) and $this->__validaPremio($this->_properties))? 
				$this->_properties['valor_total'] : 0;
		}

		return null;
	}


	#######################
	# Funções da entidate #
	#######################

	/**
	 * Verifica se o prêmio é válido e retorna true or false, baseando-se nas seguintes validações:
	 * 1) is_active = true;
	 * 2) is_approved = true;
	 * 3) won = true;
	 * -> se alguma dessas condições acima não for desobedecida, então retorna false
	 */
	protected function __validaPremio($Data) {

		if ( isset($Data['valor_premio']) ) {

			// se está desativado, não validado ou não ganhou, então retorna false
			return (!$Data['is_active'] or !$Data['is_approved'] or $Data['won']!==true)? false : true;
		} 

		return false;
	}

	/**
	 * Verifica se o lucro do revendedor é válido e retorna true or false, baseando-se nas seguintes validações:
	 * 1) is_active = true;
	 * 2) is_approved = true;
	 * 3) won = true;
	 * -> se alguma dessas condições acima não for desobedecida, então retorna false
	 */
	protected function __validaLucroRevendedor($Data) {

		if ( isset($Data['valor_revendedor']) ) {

			// se está desativado, não validado ou não ganhou, então retorna false
			return (!$Data['is_active'] or !$Data['is_approved'] or $Data['won']!==true)? false : true;
		} 

		return false;
	}
}
