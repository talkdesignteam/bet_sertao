<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Aposta Entity
 *
 */
class Aposta extends Entity
{

	/**
	 * Fields that can be mass assigned using newEntity() or patchEntity().
	 *
	 * Note that when '*' is set to true, this allows all unspecified fields to
	 * be mass assigned. For security purposes, it is advised to set '*' to false
	 * (or remove it), and explicitly make individual fields accessible as needed.
	 *
	 * @var array
	 */
	protected $_accessible = [
		'partida_id' => true,
		'partida_time' => true,
		'bilhete_id' => true,
		'league_name' => true,
		'home_name' => true,
		'away_name' => true,
		'palpite_id' => true,
		'palpite_odd' => true,
		'won' => true,
		'score' => true,		
		'is_active' => true,
		'created' => true,
		'modified' => true,
		'bilhetes' => true,
		'partidas' => true
	];
}
