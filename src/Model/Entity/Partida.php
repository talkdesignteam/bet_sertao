<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Partida Entity
 *
 */
class Partida extends Entity
{

	/**
	 * Fields that can be mass assigned using newEntity() or patchEntity().
	 *
	 * Note that when '*' is set to true, this allows all unspecified fields to
	 * be mass assigned. For security purposes, it is advised to set '*' to false
	 * (or remove it), and explicitly make individual fields accessible as needed.
	 *
	 * @var array
	 */
	protected $_accessible = [
		'time' => true,
		'league_id' => true,
		'league_name' => true,
		'league_cc' => true,
		'home_id' => true,
		'home_name' => true,
		'home_image_id' => true,
		'home_cc' => true,
		'away_id' => true,
		'away_name' => true,
		'away_image_id' => true,
		'away_cc' => true,
		'odds' => true,
		'is_active' => true,
		'created' => true,
		'modified' => true,
		'apostas' => true
	];
}
