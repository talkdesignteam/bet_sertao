<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;

use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

class PartidasController extends AppController
{

	public function initialize() {
		parent::initialize();
		
		$this->Auth->allow(['autoUpdateTable']);

		$this->loadComponent('Paginator');

		# Bet.Bet component
		$this->loadComponent('Bet.Bet', [
			'api_token'=>Configure::read('Bet.api_token')
		]);

		# Search plugin - component
		$this->loadComponent('Search.Prg', [
			'actions' => ['index']
		]);
	}


	public function index() {

		if ($this->request->query('submit')==true) {

			$query = $this->Partidas->find('search', ['search'=>$this->request->query]);
			$query
				->order(['Partidas.created DESC']);
		}
		else{

			$query = $this->Partidas->find('all');
			$query
				->where(function(QueryExpression $exp, Query $query) {
					return $exp
						->gt(new IdentifierExpression('Partidas.time'), $query->newExpr(':time_now'));
				})
				->bind(':time_now', strtotime('now'), 'string')
				->order(['Partidas.created DESC']);
		}

		$query->formatResults(function (\Cake\Collection\CollectionInterface $results) {
			
			return $results->map(function ($row) {

				$odds = json_decode($row['odds']);
				$row['home_od'] = Number::precision($odds->home_od, 2);
				$row['draw_od'] = Number::precision($odds->draw_od, 2);
				$row['away_od'] = Number::precision($odds->away_od, 2);

				$row['inicio'] = date('d/m/Y h:i', $row['time']);
				return $row;
			});
		});

		$partidas = (!$query->isEmpty())? $query->toArray() : array();
		$this->set(compact('partidas'));
	}

	public function editarOdds($id=null) {
		
		if( !$this->Partidas->exists(['Partidas.id'=>$id]) ) {

			$this->Flash->error(__('Nenhum registro encontrado.'));
			return $this->redirect($this->referer());
		}

		// busca a partida no banco de dados
		$query = $this->Partidas->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Partidas.id'), $query->newExpr(':id'));
			})
			->bind(':id', $id, 'integer')
			->formatResults(function (\Cake\Collection\CollectionInterface $results) {
				
				return $results->map(function ($row) {

					$odds = json_decode($row['odds']);
					$row['home_od'] = Number::precision($odds->home_od, 2);
					$row['draw_od'] = Number::precision($odds->draw_od, 2);
					$row['away_od'] = Number::precision($odds->away_od, 2);
					return $row;
				});
			});

		if (!$query->isEmpty()) {

			$partida = $query->first();
			
			if ($this->request->is(['patch', 'post', 'put'])) {

				$Data = $this->request->getData();

				// beforeSaveOdds
				$odds = json_decode($partida->odds);
				$odds->home_od = $Data['home_od'];
				$odds->draw_od = $Data['draw_od'];
				$odds->away_od = $Data['away_od'];
				$Data = $partida;
				$Data->odds = json_encode($odds);

				$partida = $this->Partidas->patchEntity($partida, $Data->toArray());
				if ($this->Partidas->save($partida)) {

					$this->Flash->success(__('Os dados foram atualizados.'));
					return $this->redirect($this->referer());
				}

				$this->Flash->error(__('Erro ao salvar os dados. Por favor, verifique e tente novamente ou entre em contato.'));
			}

			$this->set(compact('partida'));
		}		
	}

	#########################################
	#	FUNÇÕES DE ATUALIZAÇÃO DE DADOS 	#
	#########################################

	/**
	 * Atualiza a tabela de partidas do banco de dados com dados vindos da API.
	 *
	 */
	public function autoUpdateTable() {

		// variáveis iniciais
		$count_saved = 0;
		$count_not_saved = 0;
		$count_not_odds = 0;
		$params = [
			'sport_id'=>1,			// apenas eventos de futebol
			// 'cc'=>'br',			// apenas eventos do Brasil
			'day'=>'TODAY',			// apenas próximos eventos
			'LNG_ID'=>'22',			// Language pt_PT
		];

		// consulta partidas disponíveis
		if ( $partidas=$this->Bet->getUpcomingEvents($params) ) {

			foreach ($partidas as $k=>$p) {

				// consulta odds de cada partida
				$params_odds = [
					'event_id'=>$p->id, // Event ID you get from events/* (required)
					'source'=>'bet365', // bet365, 10bet...defaults to bet365.
					'since_time'=>strtotime('today'), // integer. add_time will be >= $since_time in results. Faster to get only updates.
					'odds_market'=>'1' // String. &odds_market=1 or &odds_market=2,3 etc.
				];
				if ($odds = $this->Bet->getEventOdds($params_odds)) {
					
					// converte dados da API para entidade da tabela
					if ( isset(current($odds)[0]) ) {

						$query = $this->Partidas->findById($p->id);
						if ( $query->isEmpty() ) {
							
							// create a new entity
							$partida = $this->Partidas->newEntity();

							$partida->id = $partidas[$k]->id;
							$partida->time = $partidas[$k]->time;

							$partida->league_id = $partidas[$k]->league->id;
							$partida->league_name = $partidas[$k]->league->name;
							$partida->league_cc = $partidas[$k]->league->cc;
							
							$partida->home_id = $partidas[$k]->home->id;
							$partida->home_name = $partidas[$k]->home->name;
							$partida->home_image_id = $partidas[$k]->home->image_id;
							$partida->home_cc = $partidas[$k]->home->cc;
							
							$partida->away_id = $partidas[$k]->away->id;
							$partida->away_name = $partidas[$k]->away->name;
							$partida->away_image_id = $partidas[$k]->away->image_id;
							$partida->away_cc = $partidas[$k]->away->cc;
							
							$partida->odds = json_encode(get_object_vars(current($odds)[0]));

							$partida->is_active = true;
						}
						else{

							$partida = $query->first();

							// updates only odds and time_status;
							$partida->odds = json_encode(get_object_vars(current($odds)[0]));
						}

						if(!$partida->errors()){

							if ($this->Partidas->save($partida)) {

								$count_saved++;
							}
							else{
														
								$count_not_saved++;
							}
						}
						else{

							$this->set('errors', $partida->errors());
						}
					}
					else{

						$count_not_odds++;
					}
				}
				else{

					$count_not_odds++;
				}
			}
		}
		else{

			$this->Flash->error($this->Bet->getLastError());
		}

		$this->set(compact('count_saved', 'count_not_saved', 'count_not_odds'));
	}



	#########################
	#	FUNÇÕES INTERNAS 	#
	#########################
	
	protected function _getListLeagues() {

		$query = $this->Partidas->find('list', [
			'keyField'=>'league_id',
    		'valueField' => 'league_name'
    	]);
		$query
			->where([
				'time >='=>strtotime('now'),
				'is_active'=>true
			])
			->order([
				'time ASC'
			]);

		$list = (!$query->isEmpty())? $query->toArray() : array();

		return $list;
	}




	#################################
	#	FUNÇÕES DE DESENVOLVIMENTO 	#
	#################################

	public function reset() {
		
		$this->Partidas->updateAll(
			[	// fields
				'time' => strtotime('tomorrow')
			],
			[	// conditions
				// 'status' => true
			]
		);

		$this->Flash->success('As partidas foram atualizadas');
		$this->render(false);
	}
}
