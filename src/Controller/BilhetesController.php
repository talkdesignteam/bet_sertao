<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;

use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

class BilhetesController extends AppController
{

	public function initialize() {
		parent::initialize();
		
		$this->Auth->allow(['home', 'atualizaPainelAposta', 'consultar', 'imprimir']);

		# Bet.Bet component
		$this->loadComponent('Bet.Bet', [
			'api_token'=>Configure::read('Bet.api_token')
		]);

		# Search Plugin
		$this->loadComponent('Search.Prg', [
			'actions' => ['index']
		]);

		if (in_array($this->request->action, ['home', 'atualizaPainelAposta'])) {
			
			$this->components['Ajax.Ajax'] = [];
		}
	}



	#################
	#	FRONT END 	#
	#################

	/**
	 * Exibe as partidas disponíveis para o usuário fazer suas apostas
	 *
	 */
	public function home() {

		// variáveis para a função
		$success = false;
		$message = '';
		$partidas = array();
		$apostas = array();

		// cria a entidade para o form
		$bilhete = $this->Bilhetes->newEntity(null, ['associated' => ['Apostas']]);

		// se o formulário de aposta foi submetido
		if( $this->request->is('post') ) {

			$Data = $this->request->getData();
			
			if (isset($Data['apostas'])) {

				$bilhete = $this->Bilhetes->patchEntity($bilhete, $Data, [
					'associated' => ['Apostas']
				]);

				if($this->Bilhetes->save($bilhete)) {

					$success = true;
					$message = __('Sua aposta foi cadastrada! Procure um revendedor para valida-la!');
				}
				else{

					$message = __('Falha ao salvar a aposta!');
				}
			}
			else{

				$message = __('Selecione pelo menos uma aposta para o bilhete!');
			}
		}
		// se não, então consulta partidas disponíveis no BD
		else{

			// Separa partidas por campeonato
			$mapper = function ($partida, $key, $mapReduce) {
				
				$league_name = (!empty($partida->league_name))? $partida->league_name : __('desconhecida');
				$mapReduce->emitIntermediate($partida, $league_name);
			};
			$reducer = function ($partidas, $league_name, $mapReduce) {
				$mapReduce->emit($partidas, $league_name);
			};

			// consulta partidas disponíveis
			$Partidas = TableRegistry::get('Partidas');
			$query = $Partidas->find();
			$query
				->where(function(QueryExpression $exp, Query $query) {
					return $exp
						->gt(new IdentifierExpression('Partidas.time'), $query->newExpr(':time_now'))
						->eq(new IdentifierExpression('Partidas.is_active'), true);
				})
				->mapReduce($mapper, $reducer)
				->order([
					'Partidas.time ASC'
				])
				->bind(':time_now', strtotime('now'), 'string');
			
			$partidas = (!$query->isEmpty())? $query->toArray() : array();
		}

		$this->set(compact('bilhete', 'partidas', 'success', 'message'));
	}

	/**
	 * Atualiza o painel de apostas a cada opção ou palpite escolhido
	 * (only AJAX)
	 *
	 * @return json $entity
	 */
	public function atualizaPainelAposta() {

		$this->request->allowMethod(['ajax']);
		
		$bilhete = $this->Bilhetes->newEntity(null, ['associated' => ['Apostas']]);
		$bilhete = $this->Bilhetes->patchEntity($bilhete, $this->request->getData(), [
			'associated' => ['Apostas'],
			'validate' => false
		]);
		
		$this->set(compact('bilhete'));
	}

	/**
	 * Consulta e exibe a situação de um bilhete e suas apostas.
	 *
	 */
	public function consultarBilhete() {
		
		if ($this->request->is('post')) {
			
			// ID do bilhete
			$id = $this->request->getData('id');
			$bilhete = array();

			// busca os bilhetes no banco de dados
			$query = $this->Bilhetes->find();
			$query
				->where(function(QueryExpression $exp, Query $query) {
					return $exp
						->eq(new IdentifierExpression('Bilhetes.id'), $query->newExpr(':id'));
				})
				->contain(['Apostas'])
				->bind(':id', $id, 'integer');

			if (!$query->isEmpty()) {

				$bilhete = $query->first();
			}
		}

		$this->set(compact('bilhete', 'id'));
	}

	/**
	 * Exibe o bilhete no layout para impressão
	 *
	 */
	public function imprimir($id=null) {

		// busca o registro no banco de dados
		$query = $this->Bilhetes->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Bilhetes.is_active'), true)
					->eq(new IdentifierExpression('Bilhetes.is_approved'), true)
					->eq(new IdentifierExpression('Bilhetes.id'), $query->newExpr(':id'));
			})
			->contain(['Apostas'])
			->bind(':id', $id, 'integer');

		if (!$query->isEmpty()) {

			$bilhete = $query->first();
			$this->set(compact('bilhete'));
		}
		else{

			$this->Flash->default(__('Falha na impressão, bilhete não encontrado!'));
			return $this->redirect(['controller'=>'Pages', 'action'=>'display', 'message']);
		}

		// renderiza no layout print 
		$this->render('imprimir', 'print'); 
	}



	#################################
	#	FUNÇÕES ADMINISTRATIVAS 	#
	#################################

	public function index() {

		if ($this->request->query('submit')==true) {

			// busca os bilhetes no banco de dados
			$query = $this->Bilhetes->find('search', ['search'=>$this->request->query]);
			$query
				->contain(['Users'])
				->order(['Bilhetes.created DESC']);
				
			$bilhetes = (!$query->isEmpty())? $query->toArray() : array();
		}

		// gera lista de revendedores (cambistas)
		$Logins = TableRegistry::get('Logins');
		$users = $Logins->getListRevendedores();

		$this->set(compact('bilhetes', 'users'));
	}

	public function view($id=null) {

		if ($id = $this->request->query('id')) {
			
			$bilhete = array();

			// busca os bilhetes no banco de dados
			$query = $this->Bilhetes->find();
			$query
				->where(function(QueryExpression $exp, Query $query) {
					return $exp
						->eq(new IdentifierExpression('Bilhetes.id'), $query->newExpr(':id'));
				})
				->contain(['Apostas'])
				->bind(':id', $id, 'integer');

			if (!$query->isEmpty()) {

				$bilhete = $query->first();
			}
		}

		$this->set(compact('bilhete', 'id'));
	}

	public function delete($id = null) {

		$this->request->allowMethod(['post', 'delete']);

		$bilhete = $this->Bilhetes->get($id);
		if ($this->Bilhetes->delete($bilhete)) {

			$this->Flash->success(__('O bilhete foi excluído!'));
			return $this->redirect(['controller'=>'Pages', 'action'=>'display', 'message', 'plugin'=>false]);
		}
		else {

			$this->Flash->error(__('Erro ao excluir o bilhete. Por favor, tente novamente ou entre em contato.'));
		}

		return false;
	}

	/**
	 * Ativa o bilhete e as suas apostas
	 *
	 * @param int $id
	 * @return void
	 */
	public function ativar($id = null) {

		$this->request->allowMethod(['post']);

		// busca o registro no banco de dados
		$query = $this->Bilhetes->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Bilhetes.id'), $query->newExpr(':id'));
			})
			->contain(['Apostas'])
			->bind(':id', $id, 'integer');

		// se existe, então tenta ativar
		if (!$query->isEmpty()) {

			$bilhete = $query->first();
			
			// ativa o bilhete e suas apostas
			$bilhete->is_active = 1;
			foreach ($bilhete->apostas as $k=>$aposta) {
				
				$aposta->is_active = 1;
			}

			$bilhete = $this->Bilhetes->patchEntity($bilhete, $bilhete->toArray(), [
				'associated' => ['Apostas']
			]);

			if ($this->Bilhetes->save($bilhete)) {

				$this->Flash->success(__('O bilhete foi ativado!'));
				return $this->redirect($this->referer());
			}
			else {

				$this->Flash->error(__('Falha ao ativar, tente novamente ou entre em contato.'));
			}
		}
		else{

			$this->Flash->default(__('Falha ao ativar, bilhete não encontrado.'));
			return $this->redirect(['controller'=>'Pages', 'action'=>'display', 'message']);
		}

		return false;
	}

	/**
	 * Cancela o bilhete e suas apostas
	 *
	 * @param int $id
	 * @return void
	 */
	public function desativar($id = null) {

		$this->request->allowMethod(['post']);

		// busca o registro no banco de dados
		$query = $this->Bilhetes->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Bilhetes.id'), $query->newExpr(':id'));
			})
			->contain(['Apostas'])
			->bind(':id', $id, 'integer');

		if (!$query->isEmpty()) {

			$bilhete = $query->first();

			// desativa o bilhete e suas apostas
			$bilhete->is_active = 0;
			foreach ($bilhete->apostas as $k=>$aposta) {
				
				$aposta->is_active = 0;
			}

			$bilhete = $this->Bilhetes->patchEntity($bilhete, $bilhete->toArray(), [
				'associated' => ['Apostas']
			]);

			if ($this->Bilhetes->save($bilhete)) {

				$this->Flash->success(__('O bilhete foi cancelado!'));
				return $this->redirect($this->referer());
			}
			else {

				$this->Flash->error(__('Falha ao cancelar, tente novamente ou entre em contato.'));
			}
		}
		else{

			$this->Flash->default(__('Falha ao cancelar, bilhete não encontrado.'));
			return $this->redirect(['controller'=>'Pages', 'action'=>'display', 'message']);
		}

		return false;
	}

	/**
	 * Valida o bilhete informado caso não tenha expirado e atualiza seu status no BD
	 *
	 * @param int $id
	 * @return void
	 */
	public function validar($id=null) {
		
		// busca os bilhete no banco de dados
		$query = $this->Bilhetes->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Bilhetes.is_active'), true)
					->notEq(new IdentifierExpression('Bilhetes.is_approved'), true)
					->eq(new IdentifierExpression('Bilhetes.id'), $query->newExpr(':id'));
			})
			->bind(':id', $id, 'integer');

		if (!$query->isEmpty()) {

			$bilhete = $query->first();

			// bilhete expirado
			if ($bilhete->expiration_datetime->isPast()) {
				
				$this->Flash->default(__('Bilhete expirado e não pode mais ser validado!'));
				return $this->redirect(['controller'=>'Pages', 'action'=>'display', 'message']);
			}

			// default settings
			$bilhete->is_approved = true;
			$bilhete->approval_user = $this->user_id; // usuário logado
			$bilhete->approval_datetime = date('Y-m-d H:i:s');

			// atualiza o bilhete no BD
			if ($this->Bilhetes->save($bilhete)) {

				$this->_atualizaSaldo($this->user_id, $bilhete->valor_apostado);	// usuário logado

				$this->Flash->success(__('O bilhete #'.$bilhete->id.' foi validado!'));
				return $this->redirect($this->referer());
			}
			else{

				$this->Flash->error(__('Falha ao validar o bilhete, verifique e tente novamente ou entre em contato!'));
				return $this->redirect($this->referer());
			}
		}
		else{

			$this->Flash->default(__('Falha ao validar, bilhete não encontrado.'));
			return $this->redirect(['controller'=>'Pages', 'action'=>'display', 'message']);
		}
	}


	/**
	 * Exibe o saldo de apostas, prêmios e lucro do cambista selecionado acordo com a data especificada
	 *
	 * @param date $data_inicio | 01-10-2018
	 * @param date $data_fim 	| 30-10-2018
	 * @return array $bilhetes 	| bilhetes do cambista logado
	 * @return array $resumo 	| saldo de apostas, prêmio e lucro
	 */
	##### TAREFAS:
	########### aprimorar validação do form (https://book.cakephp.org/3.0/en/core-libraries/form.html)
	public function caixa() {

		if ($this->request->is('post')) {

			$bilhetes = array();
			$resumo = array();

			// variáveis da função
			$user_id = $this->request->getData('approval_user');
			$data_inicio = date('Y-m-d', strtotime($this->request->getData('data_inicio')));
			$data_fim = date('Y-m-d', strtotime($this->request->getData('data_fim')));

			// busca os bilhetes no banco de dados
			$query = $this->Bilhetes->find();
			$query
				->where(function(QueryExpression $exp, Query $query) {
					return $exp
						->between(
							new IdentifierExpression('Bilhetes.created'),
							$query->newExpr(':data_inicio'),
							$query->newExpr(':data_fim')
						)
						->eq(new IdentifierExpression('approval_user'), $query->newExpr(':user_id'));
				})
				->order([
					'Bilhetes.created ASC'
				])
				->bind(':user_id', $user_id, 'integer')
				->bind(':data_inicio', $data_inicio, 'date')
				->bind(':data_fim', $data_fim, 'date');
			
			if (!$query->isEmpty()) {

				$bilhetes = $query->toArray();

				// total apostas
				$resumo['total_apostas'] = array_sum(array_column($bilhetes, 'valor_apostado'));

				// total prêmios
				$resumo['total_premios'] = array_sum(array_column($bilhetes, 'valor_premio'));
				$resumo['total_premios_real'] = array_sum(array_column($bilhetes, 'valor_premio_real'));
				$resumo['total_premios_pago'] = array_sum(array_column($bilhetes, 'valor_premio_pago'));
				$resumo['total_premios_receber'] = $resumo['total_premios_real'] - $resumo['total_premios_pago'];

				// total lucro
				$resumo['total_lucro'] = array_sum(array_column($bilhetes, 'valor_revendedor'));
				$resumo['total_lucro_real'] = array_sum(array_column($bilhetes, 'valor_revendedor_real'));
				$resumo['total_lucro_pago'] = array_sum(array_column($bilhetes, 'valor_revendedor_pago'));
				$resumo['total_lucro_receber'] = $resumo['total_lucro_real'] - $resumo['total_lucro_pago'];
				
				// total pago pelo site
				$resumo['total_valor_pago_real'] = $resumo['total_lucro_real']+$resumo['total_premios_real'];
				
			}
		}

		// gera lista de revendedores (cambistas)
		$Logins = TableRegistry::get('Logins');
		$users = $Logins->getListRevendedores();

		$filter = $this->request->getData();

		$this->set(compact('bilhetes', 'resumo', 'users', 'filter'));
	}

	/**
	 * Exibe o saldo de apostas, prêmios e lucro do cambista logado de acordo com a data especificada
	 *
	 * @param date $data_inicio | 01-10-2018
	 * @param date $data_fim 	| 30-10-2018
	 * @return array $bilhetes 	| bilhetes do cambista logado
	 * @return array $resumo 	| saldo de apostas, prêmio e lucro
	 */
	##### TAREFAS:
	########### aprimorar validação do form (https://book.cakephp.org/3.0/en/core-libraries/form.html)
	public function userCaixa() {

		if ($this->request->is('post')) {

			$bilhetes = array();
			$resumo = array();

			// variáveis da função
			$user_id = $this->user_id;
			$data_inicio = date('Y-m-d', strtotime($this->request->getData('data_inicio')));
			$data_fim = date('Y-m-d', strtotime($this->request->getData('data_fim')));

			// busca os bilhetes no banco de dados
			$query = $this->Bilhetes->find();
			$query
				->where(function(QueryExpression $exp, Query $query) {
					return $exp
						->between(
							new IdentifierExpression('Bilhetes.created'),
							$query->newExpr(':data_inicio'),
							$query->newExpr(':data_fim')
						)
						->eq(new IdentifierExpression('approval_user'), $query->newExpr(':user_id'));
				})
				->order([
					'Bilhetes.created ASC'
				])
				->bind(':user_id', $user_id, 'integer')
				->bind(':data_inicio', $data_inicio, 'date')
				->bind(':data_fim', $data_fim, 'date');
			
			if (!$query->isEmpty()) {

				$bilhetes = $query->toArray();

				// total apostas
				$resumo['total_apostas'] = array_sum(array_column($bilhetes, 'valor_apostado'));

				// total prêmios
				$resumo['total_premios'] = array_sum(array_column($bilhetes, 'valor_premio'));
				$resumo['total_premios_real'] = array_sum(array_column($bilhetes, 'valor_premio_real'));
				$resumo['total_premios_pago'] = array_sum(array_column($bilhetes, 'valor_premio_pago'));
				$resumo['total_premios_receber'] = $resumo['total_premios_real'] - $resumo['total_premios_pago'];

				// total lucro
				$resumo['total_lucro'] = array_sum(array_column($bilhetes, 'valor_revendedor'));
				$resumo['total_lucro_real'] = array_sum(array_column($bilhetes, 'valor_revendedor_real'));
				$resumo['total_lucro_pago'] = array_sum(array_column($bilhetes, 'valor_revendedor_pago'));
				$resumo['total_lucro_receber'] = $resumo['total_lucro_real'] - $resumo['total_lucro_pago'];
				
				// total pago pelo site
				$resumo['total_valor_pago_real'] = $resumo['total_lucro_real']+$resumo['total_premios_real'];
				
			}
		}

		$this->set(compact('bilhetes', 'resumo'));
	}

	/**
	 * Consulta um bilhete pelo $id e exibe todas as informações para pagamento
	 *
	 * @param int $id | 00001
	 * @return array $bilhete | Entity
	 */
	public function userPagarBilhete() {

		if ($id = $this->request->query('id')) {
			
			$bilhete = array();

			// busca os bilhetes no banco de dados
			$query = $this->Bilhetes->find();
			$query
				->where(function(QueryExpression $exp, Query $query) {
					return $exp
						->eq(new IdentifierExpression('Bilhetes.won'), true)
						->eq(new IdentifierExpression('Bilhetes.is_active'), true)
						->eq(new IdentifierExpression('Bilhetes.is_approved'), true)
						->eq(new IdentifierExpression('Bilhetes.id'), $query->newExpr(':id'));
				})
				->contain(['Apostas'])
				->bind(':id', $id, 'integer');

			if (!$query->isEmpty()) {

				$bilhete = $query->first();

				if ($bilhete->approval_user!==$this->user_id) {

					$this->Flash->error(__('Você não tem permissão para alterar este bilhete!'));
					return $this->redirect($this->action);
				}
			}
			else{

				$this->Flash->default(__('Bilhete inválido para pagamento!'));
				return $this->redirect($this->action);
			}
		}

		$is_revendedor = ($this->user_role_id===$this->cambistaRoleId)? true : false;

		$this->set(compact('bilhete', 'id', 'is_revendedor'));
	}

	public function userConfirmarPgtoPremio($id=null) {
		
		$this->request->allowMethod(['post']);

		// busca o registro no banco de dados
		$query = $this->Bilhetes->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Bilhetes.id'), $query->newExpr(':id'));
			})
			->bind(':id', $id, 'integer');

		if(!$query->isEmpty()){
			
			// default params
			$bilhete = $query->first();

			if ($bilhete->approval_user!==$this->user_id) {

				$this->Flash->error(__('Você não tem permissão para alterar este bilhete!'));
				return $this->redirect($this->referer());
			}

			$bilhete->pgto_premio = true;
			$bilhete->pgto_premio_date = strtotime('now');
			$bilhete->pgto_premio_user = $this->user_id;

			// tenta salvar o pagamento
			if ($this->Bilhetes->save($bilhete)) {

				$this->Flash->success(__('O pagamento foi confirmado!'));
				return $this->redirect($this->referer());
			}
			else {

				$this->Flash->error(__('Falha ao confirmar o pagamento. Por favor, tente novamente ou entre em contato.'));
			}
		}
		else{

			$this->Flash->error(__('Bilhete não encontrado!'));
			return $this->redirect($this->referer());
		}

		return false;
	}

	public function userConfirmarReciboRevendedor($id=null) {
		
		$this->request->allowMethod(['post']);

		// busca o registro no banco de dados
		$query = $this->Bilhetes->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Bilhetes.id'), $query->newExpr(':id'));
			})
			->bind(':id', $id, 'integer');

		if(!$query->isEmpty()){
			
			// default params
			$bilhete = $query->first();

			if ($bilhete->approval_user!==$this->user_id) {

				$this->Flash->error(__('Você não tem permissão para alterar este bilhete!'));
				return $this->redirect($this->referer());
			}

			$bilhete->pgto_revendedor = true;
			$bilhete->pgto_revendedor_date = strtotime('now');
			$bilhete->pgto_revendedor_user = $this->user_id;

			// tenta salvar o pagamento
			if ($this->Bilhetes->save($bilhete)) {

				$this->Flash->success(__('O pagamento foi confirmado!'));
				return $this->redirect($this->referer());
			}
			else {

				$this->Flash->error(__('Falha ao confirmar o pagamento. Por favor, tente novamente ou entre em contato.'));
			}
		}
		else{

			$this->Flash->error(__('Bilhete não encontrado!'));
			return $this->redirect($this->referer());
		}

		return false;
	}



	#########################
	#	FUNÇÕES INTERNAS 	#
	#########################

	/**
	 * OK - Retorna o score da partida buscando na API
	 *
	 * @param int $partida_id
	 * @return array $score[
	 *		2 => [home=>1, away=>0],
	 *		1 => [home=>2, away=>1]
	 *	]
	 */
	protected function _partidaScore($partida_id=null) {

		if (!is_null($partida_id)) {

			// busca partida na API
			$params = ['event_id'=>$partida_id];
			if ( $partida = $this->Bet->getEventView($params) ) {

				// se a partida estiver encerrada (time_status=3)
				if ($partida->time_status == 3) {
					
					return $partida->scores;
				}
			}
		}

		return false;
	}

	/**
	 * OK - Retorna o resultado da aposta de acordo com o palpite escolhido e o score vindo da API
	 *
	 * @param array entity $aposta
	 * @return bool true, false or null
	 */
	protected function _palpiteApostaStatus($aposta=null, $scores=null) {

		if (!is_null($aposta)) {
			
			if (is_null($scores)) {

				// busca scores da partida na API
				$scores = $this->_partidaScore($aposta->partida_id);
			}

			// Consulta palpite
			$Palpites = TableRegistry::get('Palpites');
			$query = $Palpites->find('all');
			$query
				->where([
					'nome LIKE' => $aposta->palpite_id
				]);

			// se existe o palpite selecionado, então verifica se ganhou ou perdeu
			if (!$query->isEmpty()) {
				
				$palpite = $query->First();

				// se a partida estiver encerrada (time_status=3)
				if (!empty($scores)) {
					
					// variáveis para o cálculo
					$home1 = $scores->{1}->{'home'};
					$home2 = $scores->{2}->{'home'};
					$away1 = $scores->{1}->{'away'};
					$away2 = $scores->{2}->{'away'};

					return (eval($palpite->calculo))? true : false;
				}
			}
		}

		return null;
	}

	protected function _atualizaSaldo($user_id=null, $value=null) {

		$Users = TableRegistry::get('AccessManager.Users');
		
		if( !$Users->exists(['Users.id'=>$user_id]) ) {

			$this->Flash->error(__('Falha ao atualizar o saldo. Nenhum registro encontrado.'));
			return false;
		}

		$user = $Users->get($user_id);

		// atualiza o saldo
		$user->saldo_apostas = $user->saldo_apostas-$value;

		if ($Users->save($user)) {

			return true;
		}
		
		return false;
	}

	#########################################
	#	FUNÇÕES DE ATUALIZAÇÃO DE DADOS 	#
	#########################################

	/**
	 * Atualiza as tabelas de bilhetes e apostas do banco de dados com dados vindos da API.
	 *
	 */
	public function autoUpdateApostas() {

		// default vars
		$count_total = 0;
		$count_saved = 0;
		$count_not_saved = 0;

		// consulta os bilhetes ativos e que ainda tenham apostas aguardando resultado 
		$query = $this->Bilhetes->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Bilhetes.is_active'), true)
					// ->eq(new IdentifierExpression('Bilhetes.is_approved'), true)
					->isNull(new IdentifierExpression('Bilhetes.won'));
			})
			->contain([
				'Apostas' => function ($q) {
					return $q
					->where(function(QueryExpression $exp, Query $query) {
						return $exp
							->eq(new IdentifierExpression('Apostas.is_active'), true);
							// ->lte(new IdentifierExpression('Apostas.partida_time'), strtotime('now'))
							// ->isNull(new IdentifierExpression('Apostas.won'));
					});
				}
			]);

		if (!$query->isEmpty()) {
			
			// busca os resultados e atualiza cada bilhete no banco de dados
			$bilhetes = $query->toArray();
			foreach ($bilhetes as $k=>$b) {
				
				$bilhete = null;
				$palpites = [null];

				foreach ($b->apostas as $j=>$a) {
					
					// score da partda
					$scores = $this->_partidaScore($a->partida_id);
					$a->score = null;
					if (!empty($scores)) {

						$home = $scores->{2}->{'home'};
						$away = $scores->{2}->{'away'};
						$a->score = $home.'x'.$away;
					}

					// retorna o status do palpite de acordo com o scores da partida
					$a->won = $this->_palpiteApostaStatus($a, $scores);
					$palpites[$j] = $a->won;
				}

				// atualiza o status do bilhete
				switch ($palpites) {
					
					// ganhou
					case (in_array(true, $palpites, true) and !in_array(false, $palpites, true) and !in_array(null, $palpites, true)):
						$b->won = true;
						break;

					// perdeu
					case (in_array(false, $palpites, true) and !in_array(null, $palpites, true)):
						$b->won = false;
						break;

					// em andamento
					case (in_array(null, $palpites, true)):
						$b->won = null;
						break;
					
					default:
						$b->won = null;
						break;
				}

				$bilhete = $this->Bilhetes->patchEntity($bilhetes[$k], $b->toArray(), [
					'associated' => ['Apostas']
				]);

				if ($this->Bilhetes->save($bilhete) ) {

					$count_saved++;
				}
				else{

					$count_not_saved++;
				}
			}
		}

		$this->set(compact('bilhetes', 'count_total', 'count_saved', 'count_not_saved'));
	}

	#################################
	#	FUNÇÕES DE DESENVOLVIMENTO 	#
	#################################

	public function insert() {

		$bilhetes = [
			// bilhete cadastrado
			[ 
				'apostador'=>'Anderson',
				'apostador_fone1'=>NULL,
				'revendedor'=>'David',
				'valor_apostado'=>'10',
				'soma_odds'=>'5.1',
				'valor_premio'=>'45.9',
				'valor_revendedor'=>'5.1',
				'valor_total'=>'51',
				'premiado'=>NULL,
				'pgto_premio'=>'0',
				'pgto_premio_date'=>NULL,
				'pgto_premio_user'=>NULL,
				'pgto_revendedor'=>'0',
				'pgto_revendedor_date'=>NULL,
				'pgto_revendedor_user'=>NULL,
				'won'=>NULL,
				'expiration_datetime'=>date('Y-m-d h:i:s', strtotime('tomorrow')),
				'is_approved'=>NULL,
				'approval_datetime'=>NULL,
				'approval_user'=>NULL,
				'is_active'=>'1',
				'created'=>NULL,
				'modified'=>NULL
			],
			// bilhete expirado
			[ 
				'apostador'=>'Anderson',
				'apostador_fone1'=>NULL,
				'revendedor'=>'David',
				'valor_apostado'=>'10',
				'soma_odds'=>'5.1',
				'valor_premio'=>'45.9',
				'valor_revendedor'=>'5.1',
				'valor_total'=>'51',
				'premiado'=>NULL,
				'pgto_premio'=>'0',
				'pgto_premio_date'=>NULL,
				'pgto_premio_user'=>NULL,
				'pgto_revendedor'=>'0',
				'pgto_revendedor_date'=>NULL,
				'pgto_revendedor_user'=>NULL,
				'won'=>NULL,
				'expiration_datetime'=>date('Y-m-d h:i:s', strtotime('yesterday')),
				'is_approved'=>NULL,
				'approval_datetime'=>NULL,
				'approval_user'=>NULL,
				'is_active'=>'1',
				'created'=>NULL,
				'modified'=>NULL
			],
			// bilhete aprovado
			[ 
				'apostador'=>'Anderson',
				'apostador_fone1'=>NULL,
				'revendedor'=>'David',
				'valor_apostado'=>'10',
				'soma_odds'=>'5.1',
				'valor_premio'=>'45.9',
				'valor_revendedor'=>'5.1',
				'valor_total'=>'51',
				'premiado'=>NULL,
				'pgto_premio'=>'0',
				'pgto_premio_date'=>NULL,
				'pgto_premio_user'=>NULL,
				'pgto_revendedor'=>'0',
				'pgto_revendedor_date'=>NULL,
				'pgto_revendedor_user'=>NULL,
				'won'=>NULL,
				'expiration_datetime'=>date('Y-m-d h:i:s', strtotime('yesterday')),
				'is_approved'=>'1',
				'approval_datetime'=>date('Y-m-d h:i:s', strtotime('now')),
				'approval_user'=>'3',
				'is_active'=>'1',
				'created'=>NULL,
				'modified'=>NULL
			],
			// bilhete premiado não pago
			[ 
				'apostador'=>'Anderson',
				'apostador_fone1'=>NULL,
				'revendedor'=>'David',
				'valor_apostado'=>'10',
				'soma_odds'=>'5.1',
				'valor_premio'=>'45.9',
				'valor_revendedor'=>'5.1',
				'valor_total'=>'51',
				'premiado'=>'1',
				'pgto_premio'=>'0',
				'pgto_premio_date'=>NULL,
				'pgto_premio_user'=>NULL,
				'pgto_revendedor'=>'0',
				'pgto_revendedor_date'=>NULL,
				'pgto_revendedor_user'=>NULL,
				'won'=>'1',
				'expiration_datetime'=>date('Y-m-d h:i:s', strtotime('yesterday')),
				'is_approved'=>'1',
				'approval_datetime'=>date('Y-m-d h:i:s', strtotime('now')),
				'approval_user'=>'3',
				'is_active'=>'1',
				'created'=>NULL,
				'modified'=>NULL
			],
			// bilhete premiado pago
			[ 
				'apostador'=>'Anderson',
				'apostador_fone1'=>NULL,
				'revendedor'=>'David',
				'valor_apostado'=>'10',
				'soma_odds'=>'5.1',
				'valor_premio'=>'45.9',
				'valor_revendedor'=>'5.1',
				'valor_total'=>'51',
				'premiado'=>'1',
				'pgto_premio'=>'1',
				'pgto_premio_date'=>date('Y-m-d h:i:s', strtotime('now')),
				'pgto_premio_user'=>3,
				'pgto_revendedor'=>'1',
				'pgto_revendedor_date'=>date('Y-m-d h:i:s', strtotime('now')),
				'pgto_revendedor_user'=>4,
				'won'=>'1',
				'expiration_datetime'=>date('Y-m-d h:i:s', strtotime('yesterday')),
				'is_approved'=>'1',
				'approval_datetime'=>date('Y-m-d h:i:s', strtotime('now')),
				'approval_user'=>'3',
				'is_active'=>'1',
				'created'=>NULL,
				'modified'=>NULL
			],
			// bilhete desativado
			[ 
				'apostador'=>'Anderson',
				'apostador_fone1'=>NULL,
				'revendedor'=>'David',
				'valor_apostado'=>'10',
				'soma_odds'=>'5.1',
				'valor_premio'=>'45.9',
				'valor_revendedor'=>'5.1',
				'valor_total'=>'51',
				'premiado'=>'1',
				'pgto_premio'=>'1',
				'pgto_premio_date'=>date('Y-m-d h:i:s', strtotime('now')),
				'pgto_premio_user'=>3,
				'pgto_revendedor'=>'1',
				'pgto_revendedor_date'=>date('Y-m-d h:i:s', strtotime('now')),
				'pgto_revendedor_user'=>4,
				'won'=>'1',
				'expiration_datetime'=>date('Y-m-d h:i:s', strtotime('yesterday')),
				'is_approved'=>'1',
				'approval_datetime'=>date('Y-m-d h:i:s', strtotime('now')),
				'approval_user'=>'3',
				'is_active'=>'0',
				'created'=>NULL,
				'modified'=>NULL
			],
		];

		if($this->Bilhetes->deleteAll(['is_active'=>1]) and $this->Bilhetes->deleteAll(['is_active'=>0])) {

			$entities = $this->Bilhetes->newEntities($bilhetes);
			if ($result = $this->Bilhetes->saveMany($entities)) {

				$this->Flash->success('Os dados foram inseridos!');
			}
			else{

				$this->Flash->error('Falha ao inserir os dados!');
			}
		}
		else{
			
			$this->Flash->error('Falha ao excluir a tabela!');
		}

		$this->render('/Pages/message');
	}

	public function reset() {
		
		$this->Bilhetes->updateAll(
			[	// fields
				'is_active' => true,
				'won' => false,
				'premiado' => null,
				'pgto_premio' => null,
				'pgto_premio_date' => null,
				'pgto_premio_user' => null,
				'pgto_revendedor' => null,
				'pgto_revendedor_date' => null,
				'pgto_revendedor_user' => null,
				'won' => false,
				'expiration_datetime' => date('Y-m-d h:i:s', strtotime('tomorrow')),
				'is_approved' => null,
				'approval_user' => null,
				'approval_datetime' => null,
			],
			[	// conditions
				// 'status' => true
			]
		);

		$this->Flash->success('As partidas foram atualizadas');
		$this->render(false);
	}
}
