<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class DashboardsController extends AppController
{

	public function initialize() {
		parent::initialize();
	}

	public function index(){
		
		// Role ID do usuário logado
		$role_id = $this->Auth->user('role_id');

		switch ($role_id) {
			
			case $this->cambistaRoleId:
				$this->redirect(['action'=>'index_cambista']);
				break;
			
			case $this->gerenteRoleId:
				$this->redirect(['action'=>'index_gerente']);
				break;
			
			case $this->adminRoleId:
				$this->redirect(['action'=>'index_admin']);
				break;
			
			default:
				# code...
				break;
		}
	}

	public function indexCambista() {
		
		$saldo_apostas = $this->Auth->user('saldo_apostas');

		$this->set(compact('saldo_apostas'));
	}

	public function indexGerente() {
		
		$saldo_apostas = $this->Auth->user('saldo_apostas');
		
		$this->set(compact('saldo_apostas'));
	}

	public function indexAdmin() {
		
		// code here
	}
}
