<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

class LoginsController extends AppController {

	public function initialize() {
		parent::initialize();
	}

	public function addGerente() {

		// cria a entidade do login
		$login = $this->Logins->newEntity();

		if ($this->request->is('post')) {

			// default settings 
			$Data = $this->request->getData();
			$Data['group_id'] = $this->gerenteGroupId;
			$Data['role_id'] = $this->gerenteRoleId;
			$Data['user_creator_id'] = $this->user_id;

			$login = $this->Logins->patchEntity($login, $Data);
			if ($this->Logins->save($login)) {

				$this->Flash->success(__('O login de gerente foi cadrastrado.'));
				return $this->setAction('listaGerente');
			}
			$this->Flash->error(__('Erro ao cadastrar o gerente. Por favor, verifique e tente novamente ou entre em contato.'));
		}

		$this->set(compact('login'));
	}

	public function addRevendedor() {

		// cria a entidade do login
		$login = $this->Logins->newEntity();

		if ($this->request->is('post')) {

			// default settings 
			$Data = $this->request->getData();
			$Data['group_id'] = $this->cambistaGroupId;
			$Data['role_id'] = $this->cambistaRoleId;
			$Data['user_creator_id'] = $this->user_id;

			$login = $this->Logins->patchEntity($login, $Data);
			if ($this->Logins->save($login)) {

				$this->Flash->success(__('O login de cambista foi cadrastrado.'));
				return $this->setAction('listaRevendedor');
			}
			$this->Flash->error(__('Erro ao cadastrar o cambista. Por favor, verifique e tente novamente ou entre em contato.'));
		}

		$this->set(compact('login'));
	}

	public function editGerente($id=null) {

		if( !$this->Logins->exists(['Logins.id'=>$id]) ) {

			$this->Flash->error(__('Nenhum registro encontrado.'));
			return $this->redirect($this->referer());
		}

		// busca o login no banco de dados
		$query = $this->Logins->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Logins.id'), $query->newExpr(':id'));
			})
			->contain(['Loginprofiles'])
			->bind(':id', $id, 'integer');

		if (!$query->isEmpty()) {

			$login = $query->first();
		}
		
		if ($this->request->is(['patch', 'post', 'put'])) {

			$Data = $this->request->getData();

			$login = $this->Logins->patchEntity($login, $Data);
			if ($this->Logins->save($login)) {

				$this->Flash->success(__('O login foi atualizado.'));
				return $this->setAction('listaGerente');
			}
			$this->Flash->error(__('Erro ao salvar os dados. Por favor, verifique e tente novamente ou entre em contato.'));
		}

		$this->set(compact('login'));
	}

	public function editRevendedor($id=null) {

		if( !$this->Logins->exists(['Logins.id'=>$id]) ) {

			$this->Flash->error(__('Nenhum registro encontrado.'));
			return $this->redirect($this->referer());
		}

		// busca o login no banco de dados
		$query = $this->Logins->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Logins.id'), $query->newExpr(':id'));
			})
			->contain(['Loginprofiles'])
			->bind(':id', $id, 'integer');

		if (!$query->isEmpty()) {

			$login = $query->first();
		}
		
		if ($this->request->is(['patch', 'post', 'put'])) {

			$Data = $this->request->getData();

			$login = $this->Logins->patchEntity($login, $Data);
			if ($this->Logins->save($login)) {

				$this->Flash->success(__('O login foi atualizado.'));
				return $this->setAction('listaRevendedor');
			}
			$this->Flash->error(__('Erro ao salvar os dados. Por favor, verifique e tente novamente ou entre em contato.'));
		}

		$this->set(compact('login'));
	}

	public function editPassword($id=null) {

		if( !$this->Logins->exists(['Logins.id'=>$id]) ) {

			$this->Flash->error(__('Nenhum registro encontrado.'));
			return $this->redirect($this->referer());
		}

		// busca o login no banco de dados
		$query = $this->Logins->find();
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Logins.id'), $query->newExpr(':id'));
			})
			->select(['id', 'username', 'email'])
			->bind(':id', $id, 'integer');

		if (!$query->isEmpty()) {

			$login = $query->first();
		}

		if ($this->request->is(['patch', 'post', 'put'])) {

			$login = $this->Logins->patchEntity($login, $this->request->getData());
			if ($this->Logins->save($login)) {

				$this->Flash->success(__('Senha atualizada.'));
				return $this->redirect($this->referer());
			}
			$this->Flash->error(__('Erro ao salvar os dados. Por favor, verifique e tente novamente ou entre em contato.'));
		}

		$this->set(compact('login'));
	}

	public function listaGerente() {

		// consulta usuários com função de gerente
		$query = $this->Logins->find('all');
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Logins.role_id'), $query->newExpr(':role_id'));
			})
			->order([
				'Logins.created ASC'
			])
			->contain(['Loginprofiles'])
			->bind(':role_id', $this->gerenteRoleId, 'integer');

		$logins = (!$query->isEmpty())? $query->toArray() : array();

		$this->set(compact('logins'));		
	}

	public function listaRevendedor() {

		// consulta usuários com função de revendedor
		$query = $this->Logins->find('all', ['role_filter'=>false]);
		$query
			->where(function(QueryExpression $exp, Query $query) {
				return $exp
					->eq(new IdentifierExpression('Logins.role_id'), $query->newExpr(':role_id'));
			})
			->order([
				'Logins.created ASC'
			])
			->contain(['Loginprofiles'])
			->bind(':role_id', $this->cambistaRoleId, 'integer');

		$logins = (!$query->isEmpty())? $query->toArray() : array();

		$this->set(compact('logins'));		
	}
}
